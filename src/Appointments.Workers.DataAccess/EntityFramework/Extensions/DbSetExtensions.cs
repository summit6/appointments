﻿namespace Appointments.Workers.DataAccess.EntityFramework.Extensions
{
    using Appointments.Workers.Domain;
    using Microsoft.EntityFrameworkCore;

    public static class DbSetExtensions
    {
        public static Speciality GetRandom(this DbSet<Speciality> setOfSpecialities)
        {
            var rand = new Random();

            var count = setOfSpecialities.Count();

            var toSkip = rand.Next(0, count);

            var spec = setOfSpecialities.ToList().OrderBy(r => Guid.NewGuid()).Skip(toSkip).Take(1).First();

            return spec;
        }
    }
}
