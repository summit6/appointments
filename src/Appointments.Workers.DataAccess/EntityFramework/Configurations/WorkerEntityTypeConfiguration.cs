﻿namespace Appointments.Workers.DataAccess.EntityFramework.Configurations
{
    using Appointments.Workers.Domain;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class WorkerEntityTypeConfiguration : IEntityTypeConfiguration<Worker>
    {
        public void Configure(EntityTypeBuilder<Worker> builder)
        {
            builder.Property(p => p.Phone).HasMaxLength(12);
            builder.Property(p => p.FirstName).HasMaxLength(100);
            builder.Property(p => p.SecondName).HasMaxLength(100);
            builder.Property(p => p.LastName).HasMaxLength(100);
        }
    }
}
