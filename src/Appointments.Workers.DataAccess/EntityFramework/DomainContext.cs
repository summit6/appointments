﻿using Microsoft.EntityFrameworkCore;

namespace Appointments.Workers.DataAccess.EntityFramework
{
    using Appointments.Workers.DataAccess.Data;
    using Appointments.Workers.Domain;

    /// <summary>
    /// Контекст
    /// </summary>
    public class DomainContext : DbContext
    {
        public DomainContext(DbContextOptions<DomainContext> options) : base(options)
        {
        }

        /// <summary>
        /// Специалисты
        /// </summary>
        public DbSet<Worker> Workers { get; set; }

        /// <summary>
        /// Специальности
        /// </summary>
        public DbSet<Speciality> Specialities { get; set; }

        public DbSet<WorkerSpeciality> WorkerSpecialities { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Worker>()
                .HasMany(e => e.Specialities)
                .WithMany(e => e.Workers)
                .UsingEntity<WorkerSpeciality>();

            modelBuilder.Entity<WorkerSpeciality>().HasKey(e => e.Id);

            modelBuilder.Entity<WorkerSpeciality>().HasOne(e => e.Worker).WithMany(e => e.WorkerSpecialities)
                .HasForeignKey(e => e.WorkerId);
            
            modelBuilder.Entity<WorkerSpeciality>().HasOne(e => e.Speciality).WithMany(e => e.WorkerSpecialities)
                .HasForeignKey(e => e.SpecialityId);

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DomainContext).Assembly);
        }
    }
}
