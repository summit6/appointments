﻿namespace Appointments.Workers.DataAccess.EntityFramework
{
    using Appointments.Workers.DataAccess.Abstractions;
    using Appointments.Workers.DataAccess.Data;
    using Appointments.Workers.DataAccess.EntityFramework.Extensions;
    using Appointments.Workers.Domain;
    using Microsoft.EntityFrameworkCore;

    public class DbInitializer : IDbInitializer
    {
        private readonly DomainContext domainContext;

        public DbInitializer(DomainContext domainContext)
        {
            this.domainContext = domainContext;
        }

        public void InitializeDb()
        {
            if (this.domainContext.Specialities.Any()) return;

            this.domainContext.Specialities.AddRange(FakeDataFactory.Specialities);

            this.domainContext.Workers.AddRange(FakeDataFactory.Workers);

            this.domainContext.SaveChanges();

            foreach (var worker in this.domainContext.Workers
                         .Include(w => w.WorkerSpecialities).ToList())
            {
                var random = new Random();

                var count = random.Next(1, 3);

                for (var i = 0; i < count; i++)
                {
                    var randomSpeciality = this.domainContext.Specialities.GetRandom();

                    this.domainContext.WorkerSpecialities.Add(

                        new WorkerSpeciality
                        {
                            SpecialityId = randomSpeciality.Id,
                            Speciality = randomSpeciality,
                            WorkerId = worker.Id,
                            Worker = worker
                        }
                    );
                }
            }

            this.domainContext.SaveChanges();
        }
    }
}
