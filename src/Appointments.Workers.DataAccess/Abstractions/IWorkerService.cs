﻿namespace Appointments.Workers.DataAccess.Abstractions
{
    using Appointments.Workers.Domain;

    public interface IWorkerService
    {
        /// <summary>
        /// Получить список.
        /// </summary>
        /// <returns> Список врачей. </returns>
        Task<IEnumerable<Worker>> GetAll();

        /// <summary>
        /// Получить полный список специалистов со специальностями
        /// </summary>
        /// <returns> Полный список специалистов </returns>
        Task<IEnumerable<Worker>> GetAllWithSpecialities();

        /// <summary>
        /// Получить список специальностей врача.
        /// </summary>
        /// <returns> Список специальностей. </returns>
        Task<IEnumerable<Speciality>?> GetAllSpecialities(Guid id);

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>клиент</returns>
        Task<Worker?> GetByIdAsync(Guid id);

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="worker">врач</para
        Task<Guid> Create(Worker worker);

        /// <summary>
        /// Изменить
        /// </summary>
        Task Update();

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        Task<bool> Delete(Guid id);
    }
}
