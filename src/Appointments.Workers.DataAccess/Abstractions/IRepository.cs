﻿namespace Appointments.Workers.DataAccess.Abstractions
{
    public interface IRepository<T>
    {
        /// <summary>
        /// Представить выборку, как запрос
        /// </summary>
        /// <returns></returns>
        IQueryable<T> AsQueryable();

        /// <summary>
        /// Запросить все сущности в базе
        /// </summary>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns>сущность</returns>
        Task<T?> GetByIdAsync(Guid id);

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns>сущность</returns>
        Task<T?> GetById(Guid id);

        /// <summary>
        /// Добавить в базу одну сущность
        /// </summary>
        /// <param name="newItem">сущность для добавления</param>
        /// <returns>добавленная сущность</returns>
        Task<T> AddAsync(T newItem);

        /// <summary>
        /// Удалить сущность
        /// </summary>
        /// <param name="id">ID удалённой сущности</param>
        /// <returns>была ли сущность удалена</returns>
        Task<bool> Delete(Guid id);

        /// <summary>
        /// Сохранить изменения
        /// </summary>
        Task SaveChangesAsync();
    }
}
