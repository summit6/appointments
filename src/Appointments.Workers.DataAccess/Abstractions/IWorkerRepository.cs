﻿namespace Appointments.Workers.DataAccess.Abstractions
{
    using Appointments.Workers.Domain;

    /// <summary>
    /// Репозиторий работы с клиентами
    /// </summary>
    public interface IWorkerRepository : IRepository<Worker>
    {
        Task<IEnumerable<Speciality>?> GetSpecialities(Guid id);

        Task<IEnumerable<Worker>> GetAllWithSpecialities();
    }
}
