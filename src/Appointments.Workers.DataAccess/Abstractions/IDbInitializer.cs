﻿namespace Appointments.Workers.DataAccess.Abstractions;

public interface IDbInitializer
{
    void InitializeDb();
}