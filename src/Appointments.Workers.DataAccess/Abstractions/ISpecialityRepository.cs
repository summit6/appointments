﻿namespace Appointments.Workers.DataAccess.Abstractions;

using Appointments.Workers.Domain;

public interface ISpecialityRepository : IRepository<Speciality>
{
}