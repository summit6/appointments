﻿namespace Appointments.Workers.DataAccess.Repositories
{
    using Appointments.Workers.DataAccess.Abstractions;
    using Appointments.Workers.Domain.Core;
    using Microsoft.EntityFrameworkCore;

    public abstract class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly DbContext Context;

        protected readonly DbSet<T> _entitySet;

        public EfRepository(DbContext context)
        {
            Context = context;

            _entitySet = Context.Set<T>();
        }

        public IQueryable<T> AsQueryable()
        {
            return this._entitySet.AsQueryable();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _entitySet.ToListAsync();
        }

        public virtual async Task<T?> GetByIdAsync(Guid id)
        {
            return await _entitySet.FindAsync(id);
        }

        public virtual async Task<T?> GetById(Guid id)
        {
            return await _entitySet.FindAsync(id);
        }

        public virtual Task<bool> Delete(Guid id)
        {
            return Task.Run(() =>
            {
                var obj = _entitySet.Find(id);
                if (obj == null)
                {
                    return false;
                }
                _entitySet.Remove(obj);
                return true;
            });
        }

        public virtual async Task<T> AddAsync(T entity)
        {
            return (await _entitySet.AddAsync(entity)).Entity;
        }

        #region SaveChanges

        /// <summary>
        /// Сохранить изменения
        /// </summary>
        public virtual async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
        }

        #endregion
    }
}
