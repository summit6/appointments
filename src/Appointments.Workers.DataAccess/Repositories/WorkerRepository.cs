﻿namespace Appointments.Workers.DataAccess.Repositories
{
    using Appointments.Workers.DataAccess.Abstractions;
    using Appointments.Workers.DataAccess.EntityFramework;
    using Appointments.Workers.Domain;
    using Microsoft.EntityFrameworkCore;

    public class WorkerRepository : EfRepository<Worker>, IWorkerRepository
    {
        public WorkerRepository(DomainContext context) : base(context)
        {
        }

        /// <summary>
        /// Получить специальности врача
        /// </summary>
        /// <returns>специальности</returns>
        public async Task<IEnumerable<Speciality>?> GetSpecialities(Guid id)
        {
            return (await Context.Set<Worker>()
                .AsQueryable()
                .Include(w => w.Specialities)
                .FirstAsync(c => c.Id == id)
                )?.Specialities;
        }

        public async Task<IEnumerable<Worker>> GetAllWithSpecialities()
        {
            return await this._entitySet.AsQueryable().Include(s => s.Specialities).ToListAsync();
        }
    }
}
