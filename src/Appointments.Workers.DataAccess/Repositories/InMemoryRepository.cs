﻿namespace Appointments.Workers.DataAccess.Repositories
{
    using Appointments.Workers.DataAccess.Abstractions;
    using Appointments.Workers.Domain.Core;

    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            this.Data = data.ToList();
        }

        public IQueryable<T> AsQueryable()
        {
            return this.Data.AsQueryable();
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)this.Data);
        }

        public Task<T?> GetByIdAsync(Guid id)
        {
            return Task.FromResult(this.Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task<T?> GetById(Guid id)
        {
            return await Task.FromResult(this.Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> AddAsync(T newItem)
        {
            return Task.Run(() =>
            {
                newItem.Id = Guid.NewGuid();
                this.Data.Add(newItem);
                return newItem;
            });
        }

        public Task<bool> ChangeAsync(T newItem)
        {
            return Task.Run(() =>
            {
                var item = this.Data.FirstOrDefault(x => x.Id == newItem.Id);
                if (item == null)
                    return false;
                var result = this.Data.Remove(item);
                if (result)
                    this.Data.Add(newItem);
                return result;
            });
        }

        public Task<bool> Delete(Guid id)
        {
            return Task.Run(() =>
            {
                var item = this.Data.FirstOrDefault(x => x.Id == id);
                if (item == null)
                    return false;
                return this.Data.Remove(item);
            });
        }

        /// <summary>
        /// Сохранить изменения
        /// </summary>
        public virtual Task SaveChangesAsync()
        {
            return Task.FromResult(true);
        }
    }
}
