﻿namespace Appointments.Workers.DataAccess.Repositories
{
    using Appointments.Workers.DataAccess.Abstractions;
    using Appointments.Workers.DataAccess.EntityFramework;
    using Appointments.Workers.Domain;

    public class SpecialityRepository: EfRepository<Speciality>, ISpecialityRepository
    {
        public SpecialityRepository(DomainContext context) : base(context)
        {
        }
    }
}
