﻿using Appointments.Workers.DataAccess.Abstractions;

namespace Appointments.Workers.DataAccess.Repositories
{
    using Appointments.Workers.Domain;
    using Microsoft.EntityFrameworkCore;

    public class WorkerSpecialityRepository : EfRepository<WorkerSpeciality>, IWorkerSpecialityRepository
    {
        public WorkerSpecialityRepository(DbContext context) : base(context)
        {
        }
    }
}
