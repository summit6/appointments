﻿namespace Appointments.Workers.DataAccess.Services
{
    using Appointments.Workers.DataAccess.Abstractions;
    using Appointments.Workers.Domain;
    using Microsoft.EntityFrameworkCore;

    public class WorkerSevice : IWorkerService
    {
        private readonly IWorkerRepository _repository;

        public WorkerSevice(IWorkerRepository repository)
        {
            this._repository = repository;
        }
        /// <summary>
        /// Получить весь список.
        /// </summary>
        /// <returns> Список врачей. </returns>
        public async Task<IEnumerable<Worker>> GetAll()
        {
            return await this._repository.GetAllAsync();
        }

        public async Task<IEnumerable<Worker>> GetAllWithSpecialities()
        {
            return await this._repository.GetAllWithSpecialities();
        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>врач</returns>
        public async Task<Worker?> GetByIdAsync(Guid id)
        {
            return await this._repository.AsQueryable()
                .Include(w => w.WorkerSpecialities)
                .Include(w => w.Specialities)
                .FirstOrDefaultAsync(w => w.Id == id);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="worker">врач</para
        public async Task<Guid> Create(Worker worker)
        {
            var res = await this._repository.AddAsync(worker);
            await this._repository.SaveChangesAsync();
            return res.Id;
        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="worker">врач</param>
        public async Task Update()
        {
            await this._repository.SaveChangesAsync();
        }

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        public async Task<bool> Delete(Guid id)
        {
            var res = await this._repository.Delete(id);
            if (res)
                await this._repository.SaveChangesAsync();
            return res;
        }

        /// <summary>
        /// Получить список специальностей
        /// </summary>
        /// <param name="id">идентификатор врача</param>
        /// <returns> Список специальностей. </returns>
        public async Task<IEnumerable<Speciality>?> GetAllSpecialities(Guid id)
        {
            return (await this._repository.GetSpecialities(id))?.ToList();
        }
    }
}
