using Microsoft.AspNetCore.Mvc;

namespace DocAppointment.Gateway.Controllers
{
    /// <summary>
    /// ���������� �����������
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class AuthorizationController : ControllerBase
    {
        private readonly ILogger _logger;


        public AuthorizationController(ILogger<AuthorizationController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// ����� �����������
        /// </summary>
        [HttpGet]
        public Task<IActionResult> Get()
        {
            _logger.LogDebug("{UserName}", User.Identity.Name);
            return Task.FromResult<IActionResult>(Ok("User"));
        }
    }
}