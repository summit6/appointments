using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace DocAppointment.Gateway.Example;

using Appointments.CrossSystemContracts.Registration;

internal class ExampleSchemaFilter : ISchemaFilter
{
    private readonly JsonSerializerSettings _settings = new()
    {
        Formatting = Formatting.Indented,
        NullValueHandling = NullValueHandling.Ignore,
        Converters = new List<JsonConverter> { new Newtonsoft.Json.Converters.StringEnumConverter() }
    };

    private static readonly ProblemDetails ProblemDetailsObject = new()
    {
        Type = "Microsoft.AspNetCore.Http.BadHttpRequestException",
        Title = "One or more validation errors occurred",
        Status = 400
    };

    private static readonly RegistrationRequest RegistrationRequestObject = new()
    {
        PartnerId = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
        WorkerId = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
        Information="�������� ������",
        ScheduledOnTime = DateTime.UtcNow,
    };


    public void Apply(OpenApiSchema schema, SchemaFilterContext context)
    {
        object? example = context.Type.Name switch
        {
            nameof(ProblemDetails) => ProblemDetailsObject,
            nameof(RegistrationRequest ) => RegistrationRequestObject,
            _ => null
        };

        if (example is not null)
            schema.Example = new OpenApiString(JsonConvert.SerializeObject(example, _settings));
    }
}