﻿using DocAppointment.Gateway.Example;
using Microsoft.OpenApi.Models;
using System.Text.RegularExpressions;

namespace DocAppointment.Gateway.Extensions
{
    public static class SwaggerExtensions
    {
        public static IServiceCollection AddSwaggerService(this IServiceCollection services)
        {

            services.AddGrpcSwagger();
            var scheme = "Bearer";
            return services.AddSwaggerGen(c =>
            {
                c.AddSecurityDefinition(scheme, new OpenApiSecurityScheme
                {
                    Description = $@"Введите авторизационный токен пользователя, для выполнения запроса с авторизацией под этим пользователем. Пример ввода : '{scheme} Токен'",
                    Name = "Authorization",
                    In = ParameterLocation.Cookie,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = scheme
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = scheme
                            },
                            Name = scheme,
                            Scheme = "oauth2",
                            In = ParameterLocation.Cookie,
                        },
                        new List<string>()
                    }
                });
                c.CustomOperationIds(e => $"{Regex.Replace(e.RelativePath, "{|}", "")}{e.HttpMethod}");
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Gateway", Version = "v1" });
                var xmlFiles = Directory.GetFiles(AppContext.BaseDirectory, "*.xml", SearchOption.TopDirectoryOnly).ToList();
                xmlFiles.ForEach(xmlFile =>
                {
                    c.IncludeXmlComments(xmlFile);
                    c.IncludeGrpcXmlComments(xmlFile);
                });
                c.SchemaFilter<ExampleSchemaFilter>();

                //c.AddServer(new OpenApiServer {Url = Configuration["ASPNETCORE_URLS"]});
            });

        }
    }
}