﻿using Microsoft.Extensions.Caching.Distributed;

namespace DocAppointment.Gateway;

public class CacheMiddleware
{
    private readonly RequestDelegate _next;
    private readonly IDistributedCache _cache;

    public CacheMiddleware(RequestDelegate next, IDistributedCache cache)
    {
        _next = next;
        _cache = cache;
    }

    public async Task InvokeAsync(HttpContext context)
    {

        if (context.Request.Method != "GET")
        {
            await _next(context);
        }
        
        else
        {
            string cacheKey = context.Request.QueryString.HasValue? context.Request.Path.ToString() + context.Request.QueryString.Value : context.Request.Path.ToString();

            // Проверяем, есть ли значение в кэше для данного ключа
            var cachedResponse = await _cache.GetStringAsync(cacheKey);
            if (cachedResponse != null)
            {
                // Если значение найдено в кэше, возвращаем его без выполнения запроса
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(cachedResponse.ToString());
                return;
            }

            // Если значение не найдено в кэше, выполняем следующий обработчик запроса
            using var memoryStream = new MemoryStream();
            var originalResponseStream = context.Response.Body;
            context.Response.Body = memoryStream;

            await _next(context);

            // Копируем полученный ответ в поток памяти
            memoryStream.Seek(0, SeekOrigin.Begin);
            var responseBody = await new StreamReader(memoryStream).ReadToEndAsync();

            // Сохраняем ответ в кэше
            await _cache.SetStringAsync(cacheKey, responseBody, 
                new DistributedCacheEntryOptions { AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(10) });

            // Восстанавливаем оригинальный ответ
            memoryStream.Seek(0, SeekOrigin.Begin);
            await memoryStream.CopyToAsync(originalResponseStream);
            context.Response.Body = originalResponseStream;
        }
    }
}