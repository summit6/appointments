﻿using Appointments.CrossSystemContracts.Workers;
using Refit;

namespace Serices;

public interface IWorkerRest
{
    [Get("/api/v1/Workers")]
    public Task<IApiResponse<List<WorkerContact>>> GetUser();
}
