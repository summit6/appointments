using Google.Protobuf.WellKnownTypes;
using Greet;
using Grpc.Core;

namespace Serices;
public class WorkerGrpcService : Greeter.GreeterBase
{
    private readonly ILogger _logger;
    private readonly IWorkerRest _workerRest;
    public WorkerGrpcService(ILogger<WorkerGrpcService> logger, IWorkerRest workerRest)
    {
        _logger = logger;
        _workerRest = workerRest;
    }

    public override async Task<replyWorkers> GetWorkers(Empty request, ServerCallContext context)
    {
        var response = await _workerRest.GetUser();

        var resultJson = Newtonsoft.Json.JsonConvert.SerializeObject(response.Content);

        _logger.LogDebug(resultJson);
        var workerGrpcContacts = Newtonsoft.Json.JsonConvert.DeserializeObject<List<WorkerGrpcContact>>(resultJson);
        var res = new replyWorkers();
        res.Data.AddRange(workerGrpcContacts);
        return res;

    }

}
