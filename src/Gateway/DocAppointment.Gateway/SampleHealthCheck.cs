﻿using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace DocAppointment.Gateway
{
    public class SampleHealthCheck : IHealthCheck
    {
        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            var result = HealthCheckResult.Healthy("OK"); //TODO add check
            return Task.FromResult(result);
        }
    }
}