﻿using DocAppointment.Gateway.Extensions;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.HttpLogging;
using Refit;
using Serices;
using Serilog;
using Serilog.Formatting.Json;

namespace DocAppointment.Gateway
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Host.UseSerilog((context, services, configuration) => configuration
                    .ReadFrom.Configuration(context.Configuration)
                    .ReadFrom.Services(services)
                    .Enrich.FromLogContext()
                    .WriteTo.Console()
                        .WriteTo.RabbitMQ((clientConfiguration, sinkConfiguration) =>
                        {
                            clientConfiguration.Username = "guest";
                            clientConfiguration.Password = "guest";
                            clientConfiguration.Exchange = "Appointments.Logging.Domain.Dtos:LogDto";
                            clientConfiguration.Port = 5672;
                            clientConfiguration.Hostnames.Add("localhost");
                            sinkConfiguration.TextFormatter = new JsonFormatter();
                        })
                    );

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddGrpc().AddJsonTranscoding();
            builder.Services.AddSwaggerService();

            var handler = new SocketsHttpHandler
            {
                PooledConnectionIdleTimeout = Timeout.InfiniteTimeSpan,
                KeepAlivePingDelay = TimeSpan.FromSeconds(60),
                KeepAlivePingTimeout = TimeSpan.FromSeconds(30),
                EnableMultipleHttp2Connections = true
            };

            builder.Services.AddRefitClient<IWorkerRest>()
                .ConfigureHttpClient(c => c.BaseAddress = new Uri("http://appointments.workers/")).ConfigurePrimaryHttpMessageHandler(() => handler);

            builder.Services.AddReverseProxy()
                .LoadFromConfig(builder.Configuration.GetSection("ReverseProxy"));

            builder.Services.AddHealthChecks().AddCheck<SampleHealthCheck>(nameof(SampleHealthCheck));

            builder.Services.AddStackExchangeRedisCache(options => {
                options.Configuration = builder.Configuration.GetConnectionString("Redis");
                options.InstanceName = "gateway";
            });
            
            builder.Services.AddHttpLogging(logging =>
            {
                logging.LoggingFields = HttpLoggingFields.All;
                logging.RequestBodyLogLimit = 4096;
                logging.ResponseBodyLogLimit = 4096;

            });
            var app = builder.Build();

            // Configure the HTTP request pipeline.
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();
            
            app.UseAuthorization();

            app.MapGrpcService<WorkerGrpcService>();
            app.UseWhen(
                ctx => ctx.Request.ContentType != "application/grpc",
                builder =>
                {
                    builder.UseHttpLogging();
                }
            );

            app.UseHealthChecks("/health", new HealthCheckOptions
            {
                Predicate = healthcheck => healthcheck.Name == nameof(SampleHealthCheck)
            });
            app.UseMiddleware<CacheMiddleware>();


            app.MapReverseProxy();

            app.Run();
        }
    }
}