﻿namespace Appointments.Registration.Domain
{
    using Appointments.Registration.Domain.Core;

    public class RegistrationItem: BaseEntity
    {
        /// <summary>
        /// Id клиента
        /// </summary>
        public Guid PartnerId { get; set; }

        /// <summary>
        /// Id того, к кому записан
        /// </summary>
        public Guid WorkerId { get; set; }

        /// <summary>
        /// Дата и время создания записи о регистрации в БД
        /// </summary>
        public DateTime RegistrationTime { get; set; }

        /// <summary>
        /// Дата и время, на которые клиент записан
        /// </summary>
        public DateTime ScheduledOnTime{ get; set; }

        /// <summary>
        /// Дополнительная информация о запмси
        /// </summary>
        public string? Information { get; set; }

        /// <summary>
        /// Актуальность записи
        /// </summary>
        public bool IsEnabled { get; set; }
    }
}