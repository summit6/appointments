﻿namespace Appointments.Registration.DataAccess
{
    using Appointments.Registration.Domain;
    using Microsoft.EntityFrameworkCore;

    public class DomainContext : DbContext
    {
        public DomainContext(DbContextOptions<DomainContext> options)
            : base(options)
        {

        }

        public DbSet<RegistrationItem> RegistrationItems { get; set; }
    }
}