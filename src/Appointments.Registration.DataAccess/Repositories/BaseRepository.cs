﻿namespace Appointments.Registration.DataAccess.Repositories
{
    using Appointments.Registration.DataAccess.Abstractions;
    using Appointments.Registration.Domain.Core;
    using Microsoft.EntityFrameworkCore;

    public class BaseRepository<T> : IRepository<T> where T : BaseEntity

    {
        protected readonly DomainContext DomainContext;

        public BaseRepository(DomainContext domainContext)
        {
            this.DomainContext = domainContext;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await this.DomainContext.Set<T>().ToListAsync();

            return entities;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await this.DomainContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);

            return entity;
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await this.DomainContext.Set<T>().Where(x => ids.Contains(x.Id)).ToListAsync();
            return entities;
        }

        public async Task<Guid> AddAsync(T entity)
        {
            var added = await this.DomainContext.Set<T>().AddAsync(entity);

            await this.DomainContext.SaveChangesAsync();

            return added.Entity.Id;
        }

        public async Task UpdateAsync(T entity)
        {
            await this.DomainContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            this.DomainContext.Set<T>().Remove(entity);
            await this.DomainContext.SaveChangesAsync();
        }
    }
}
