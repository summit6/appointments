﻿namespace Appointments.Registration.DataAccess.Repositories
{
    using Appointments.Registration.Domain;
    using Microsoft.EntityFrameworkCore;

    public class RegistrationItemRepository 
        : BaseRepository<RegistrationItem>, IRegistrationItemRepository
    {
        public RegistrationItemRepository(DomainContext domainContext) 
            : base(domainContext)
        {
        }

        public async Task<IEnumerable<RegistrationItem>> GetByPartnerId(Guid partnerId)
        {
            return
                await this.DomainContext.RegistrationItems.AsQueryable()
                    .Where(r => r.PartnerId == partnerId)
                    .ToListAsync();
        }

        public async Task<IEnumerable<RegistrationItem>> GetByWorkerId(Guid workerId)
        {
            return
                await this.DomainContext.RegistrationItems.AsQueryable()
                    .Where(r => r.WorkerId == workerId)
                    .ToListAsync();
        }
    }
}
