﻿namespace Appointments.Registration.DataAccess.Repositories;

using Appointments.Registration.DataAccess.Abstractions;
using Appointments.Registration.Domain;

public interface IRegistrationItemRepository : IRepository<RegistrationItem>
{
    Task<IEnumerable<RegistrationItem>> GetByPartnerId(Guid partnerId);

    Task<IEnumerable<RegistrationItem>> GetByWorkerId(Guid workerId);
}