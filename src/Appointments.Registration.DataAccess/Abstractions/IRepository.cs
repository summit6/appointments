﻿namespace Appointments.Registration.DataAccess.Abstractions
{
    using Appointments.Registration.Domain.Core;

    public interface IRepository<T>
        where T : BaseEntity
    {
        public Task<IEnumerable<T>> GetAllAsync();

        public Task<T> GetByIdAsync(Guid id);

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids);

        public Task<Guid> AddAsync(T entity);

        public Task UpdateAsync(T entity);

        public Task DeleteAsync(T entity);
    }
}
