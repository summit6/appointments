﻿namespace Appointments.Registration.Host.Extensions
{
    using Appointments.CrossSystemContracts.Registration;
    using Appointments.Registration.Domain;
    using Appointments.Registration.Services.Partners;
    using Appointments.Registration.Services.Workers;

    public static class RegistrationItemsExtensions
    {
        public static List<PartnerRegistrationModel> ConvertToPartnerRegistrationModels(
            this IEnumerable<RegistrationItem> registrationItems, IWorkersService workersService)
        {
            return (
                from registrationItem in registrationItems

                let worker = workersService.GetFullNameAndSpecialities(registrationItem.WorkerId, CancellationToken.None).Result

                select new PartnerRegistrationModel
                {
                    RegistrationItemId = registrationItem.Id,

                    WorkerId = registrationItem.WorkerId,

                    WorkerFullName = $"{worker?.FullName}",

                    SpecialitiesRecord = worker?.SpecialitiesRecord,

                    ScheduledOnTime = registrationItem.ScheduledOnTime
                }
            ).ToList();
        }

        public static async Task<List<WorkerRegistrationModel>> ConvertToWorkerRegistrationModels( 
            this List<RegistrationItem> registrationItems, IPartnersService partnersService)
        {
            var result = new List<WorkerRegistrationModel>();

            if (registrationItems == null || !registrationItems.Any())
                return result;

            var validPartnerIds = registrationItems.Select(i => i.PartnerId);

            var workerPartners =
                (await partnersService.GetAllPartners(CancellationToken.None))?
                .Where(p => validPartnerIds.Contains(p.Id)).ToList();

            if (workerPartners == null || !workerPartners.Any())
                return result;

            result.AddRange(
                from registrationItem in registrationItems
                let partner = workerPartners.FirstOrDefault(p => p.Id == registrationItem.PartnerId)
                where partner != null
                select new WorkerRegistrationModel
                {
                    RegistrationItemId = registrationItem.Id,
                    PartnerId = partner.Id,
                    ScheduledOnTime = registrationItem.ScheduledOnTime,
                    Email = partner.Email,
                    FullName = partner.FullName,
                    PhoneStr = partner.PhoneStr
                });

            return result;
        }
    }
}
