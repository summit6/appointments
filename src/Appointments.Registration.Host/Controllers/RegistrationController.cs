﻿namespace Appointments.Registration.Host.Controllers
{
    using Appointments.CrossSystemContracts.Registration;
    using Appointments.CrossSystemContracts.Workers;
    using Appointments.Registration.Domain;
    using Appointments.Registration.Host.Extensions;
    using Appointments.Registration.Services.Logger;
    using Appointments.Registration.Services.Partners;
    using Appointments.Registration.Services.RegistrationItems;
    using Appointments.Registration.Services.Validation;
    using Appointments.Registration.Services.Workers;
    using AutoMapper;
    using FluentValidation.Results;
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Route("api/v1/[controller]")]
    public class RegistrationController : Controller
    {
        private readonly IRegistrationRequestValidator registrationRequestValidator;
        private readonly IRegistrationItemsService registrationItemsService;
        private readonly IPartnersService partnersService;
        private readonly IWorkersService workersService;
        private readonly ILoggerService loggerService;
        private readonly IMapper mapper;

        public RegistrationController(
            IRegistrationRequestValidator registrationRequestValidator,
            IRegistrationItemsService registrationItemsService,
            IPartnersService partnersService,
            IWorkersService workersService,
            ILoggerService loggerService,
            IMapper mapper)
        {
            this.registrationRequestValidator = registrationRequestValidator;
            this.registrationItemsService = registrationItemsService;
            this.partnersService = partnersService;
            this.workersService = workersService;
            this.loggerService = loggerService;
            this.mapper = mapper;
        }

        /// <summary>
        /// Проверить, доступен ли сервис
        /// </summary>
        /// <returns></returns>
        [HttpGet("check")]
        public async Task<IActionResult> CheckStatus()
        {
            return this.Ok(await Task.Run(() => "Status: OK"));
        }

        /// <summary>
        /// Добавить регистрацию
        /// </summary>
        /// <param name="registrationRequest">Запрос на регистрацию</param>
        /// <returns></returns>
        [HttpPost("register")]
        public async Task<IActionResult> Register(RegistrationRequest registrationRequest)
        {
            var validationResult = 
                await this.registrationRequestValidator.ValidateAsync(registrationRequest, CancellationToken.None);

            if (!validationResult.IsValid)
                return this.AggregateBadRequestErrors(validationResult);

            var registrationItem = this.mapper.Map<RegistrationItem>(registrationRequest);

            try
            {
                var registrationItemId = await this.registrationItemsService.AddRegistration(registrationItem);

                await this.loggerService.Log($"Добавлена новая регистрация (id:{registrationItemId})"); 
                
                return this.Ok(registrationItemId);
            }
            catch (Exception e)
            {
                await this.loggerService.Log($"RegistrationController.Register: {e.Message}");

                return this.StatusCode(500);
            }
        }

        /// <summary>
        /// Закрыть регистрацию
        /// </summary>
        /// <param name="registrationCommitRequest">Запрос на закрытие регистрации</param>
        /// <returns></returns>
        [HttpPut("commitregistration")]
        public async Task<IActionResult> CommitRegistration(CommitRegistrationRequest registrationCommitRequest)
        {
            if (registrationCommitRequest == null)
                return this.BadRequest();

            var registration = 
                await this.registrationItemsService.GetRegistrationById(registrationCommitRequest.RegistrationItemId);

            if (registration == null)
                return this.NotFound($"Регистрация {registrationCommitRequest.RegistrationItemId} не найдена!");

            registration.Information = registrationCommitRequest.Information;
            registration.IsEnabled = false;

            try
            {
                await this.registrationItemsService.UpdateRegistration(registration);
            }
            catch (Exception e)
            {
                await this.loggerService.Log($"RegistrationController.CommitRegistration: {e.Message}");

                return this.StatusCode(500);
            }

            return this.Ok();
        }

        /// <summary>
        /// Получить все регистрации из базы
        /// </summary>
        /// <returns>Cписок регистраций</returns>
        [HttpGet("all")]
        public async Task<IEnumerable<RegistrationItem>> GetAllRegistrations()
        {
            return await this.registrationItemsService.GetAllRegistrations();
        }

        /// <summary>
        /// Получить список активных регистраций клиента
        /// </summary>
        /// <param name="id">id клиента</param>
        /// <returns></returns>
        [HttpGet("partnerregistrations")]
        public async Task<ActionResult<PartnerRegistrationsResponse>> GetPartnerRegistrations(Guid id)
        {
            var registrationItems =
                (await this.registrationItemsService.GetPartnerRegistrations(id))
                .Where(i => i.ScheduledOnTime > DateTime.Now && i.IsEnabled).ToList();

            if (registrationItems == null || !registrationItems.Any())
                return this.NotFound();

            var result = new PartnerRegistrationsResponse
            {
                PartnerId = registrationItems.First().PartnerId,

                PartnerRegistrations = registrationItems.ConvertToPartnerRegistrationModels(this.workersService)
            };

            return this.Ok(result);
        }
        
        /// <summary>
        /// Получить список активных регистраций специалиста
        /// </summary>
        /// <param name="id">id специалиста</param>
        /// <returns></returns>
        [HttpGet("workerregistrations")]
        public async Task<ActionResult<WorkerRegistrationsResponse>> GetWorkerRegistrations(Guid id)
        {
            var registrationItems =
                (await this.registrationItemsService.GetRegistrationsToWorker(id))
                .Where(i => i.ScheduledOnTime > DateTime.Now && i.IsEnabled).ToList();

            if (registrationItems == null || !registrationItems.Any())
                return this.NotFound();

            var result = new WorkerRegistrationsResponse()
            {
                WorkerId = registrationItems.First().WorkerId,

                WorkerRegistrations = await registrationItems.ConvertToWorkerRegistrationModels(this.partnersService)
            };

            return this.Ok(result);
        }

        /// <summary>
        /// Получить свободные даты специалиста на 31 день
        /// </summary>
        /// <param name="id">WorkerId</param>
        /// <returns></returns>
        [HttpGet("workerbusytime")]
        public async Task<ActionResult<WorkerBusyTimeResponse>> GetWorkerBusyTime(Guid id)
        {
            var items = 
                (await this.registrationItemsService.GetRegistrationsToWorker(id)).ToList();

            if (items == null || !items.Any()) return this.NotFound();

            return new WorkerBusyTimeResponse
            {
                DateTimes = items.Where(i => i.IsEnabled)
                    .Select(i => i.ScheduledOnTime)
                    .Where(d => d > DateTime.Now).OrderBy(d => d).ToList()
            };
        }

        private BadRequestObjectResult AggregateBadRequestErrors(ValidationResult validationResult)
        {
            var errorText =
                validationResult
                    .Errors
                    .Aggregate(string.Empty,
                        (current, error) => current + $"{error.ErrorMessage}\r\n");

            return new BadRequestObjectResult(errorText);
        }
    }
}
