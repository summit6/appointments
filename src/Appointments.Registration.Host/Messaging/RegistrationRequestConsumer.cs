﻿namespace Appointments.Registration.Host.Messaging
{
    using Appointments.CrossSystemContracts.Registration;
    using Appointments.Registration.DataAccess.Repositories;
    using Appointments.Registration.Domain;
    using Appointments.Registration.Services.Validation;
    using AutoMapper;
    using MassTransit;

    public class RegistrationRequestConsumer : IConsumer<RegistrationRequest>
    {
        private readonly IRegistrationRequestValidator registrationRequestValidator;
        private readonly IMapper mapper;
        private readonly IRegistrationItemRepository registrationItemRepository;

        public RegistrationRequestConsumer(
            IRegistrationRequestValidator registrationRequestValidator,
            IMapper mapper,
            IRegistrationItemRepository registrationItemRepository)
        {
            this.registrationRequestValidator = registrationRequestValidator;
            this.mapper = mapper;
            this.registrationItemRepository = registrationItemRepository;
        }

        public async Task Consume(ConsumeContext<RegistrationRequest> context)
        {
            var registrationRequest = context.Message;

            var validationResult =
                await this.registrationRequestValidator.ValidateAsync(registrationRequest, CancellationToken.None);

            if (!validationResult.IsValid)
            {
                var errorText =
                    validationResult
                        .Errors
                        .Aggregate(string.Empty, (current, error) => current + $"{error.ErrorMessage}\r\n");
                //TODO Log error
            }

            var registrationItem = this.mapper.Map<RegistrationItem>(registrationRequest);

            await this.registrationItemRepository.AddAsync(registrationItem);

            //TODO Log registered
        }
    }
}
