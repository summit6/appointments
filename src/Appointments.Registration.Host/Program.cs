using Appointments.Registration.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Appointments.Registration.Host;

public class Program
{
    private static string origin = "RegistrationOrigins";

    private static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.

        builder.Services.AddCors(options =>
        {
            options.AddPolicy(name: origin, conf =>
                {
                    conf.WithOrigins(new[] { "*" })
                        .WithHeaders(new[] { "*" })
                        .WithMethods(new[] { "*" });
                });
        });

        builder.Services.AddControllers();

        builder.Services.AddServices(builder.Configuration);

        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();

        builder.Services.AddSwaggerGen();

        var app = builder.Build();

        // Configure the HTTP request pipeline.

        if (!app.Environment.IsDevelopment())
        {
            app.UseExceptionHandler("/Error");
            app.UseHsts();
        }

        app.UseHttpsRedirection();

        app.UseRouting();
        app.UseCors(origin);

        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseAuthorization();

        app.MapControllers();

        MigrateDatabase(app);

        app.Run();
    }

    private static void MigrateDatabase(WebApplication app)
    {
        using var scope = app.Services.GetRequiredService<IServiceScopeFactory>().CreateScope();

        var dbContext = scope.ServiceProvider.GetService<DomainContext>();

        if (dbContext.Database.ProviderName != "Microsoft.EntityFrameworkCore.InMemory")
        {
            dbContext.Database.Migrate();
        }
    }
}