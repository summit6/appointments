﻿namespace Appointments.Registration.Host.Mappings
{
    using Appointments.CrossSystemContracts.Registration;
    using Appointments.Registration.Domain;
    using AutoMapper;

    public class RegistrationItemMappingProfile : Profile
    {
        public RegistrationItemMappingProfile()
        {
            this.CreateMap<RegistrationRequest, RegistrationItem>()
                .ForMember(r => r.Id, map => map.MapFrom(m => Guid.NewGuid()))
                .ForMember(r => r.PartnerId, map => map.MapFrom(m => m.PartnerId))
                .ForMember(r => r.WorkerId, map => map.MapFrom(m => m.WorkerId))
                .ForMember(r => r.Information, map => map.MapFrom(m => m.Information))
                .ForMember(r => r.ScheduledOnTime, map => map.MapFrom(m => m.ScheduledOnTime.ToUniversalTime()))
                .ForMember(r => r.IsEnabled, map => map.MapFrom(m => true))
                .ForMember(r => r.RegistrationTime, map => map.MapFrom(m => DateTime.Now.ToUniversalTime()));
        }
    }
}
