﻿namespace Appointments.Registration.Host
{
    using Appointments.Registration.DataAccess;
    using Appointments.Registration.DataAccess.Repositories;
    using Appointments.Registration.Host.Mappings;
    using Appointments.Registration.Host.Messaging;
    using Appointments.Registration.Services.Logger;
    using Appointments.Registration.Services.Partners;
    using Appointments.Registration.Services.RegistrationItems;
    using Appointments.Registration.Services.Validation;
    using Appointments.Registration.Services.Workers;
    using AutoMapper;
    using MassTransit;
    using Microsoft.EntityFrameworkCore;

    public static class Registrar
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(configuration);

            services.AddMappings();

            services.AddDbContext<DomainContext>(x =>
            {
                x.UseNpgsql(
                    configuration["PostgresConnectionString"]
                );
            });

            services.AddScoped<IRegistrationRequestValidator, RegistrationRequestValidator>();

            services.AddScoped<IRegistrationItemRepository, RegistrationItemRepository>();

            services.AddBus(configuration);

            services.AddScoped<IPartnersService, PartnersService>();

            services.AddScoped<IWorkersService, WorkersService>();

            services.AddScoped<ILoggerService, LoggerService>();

            return services.AddScoped<IRegistrationItemsService, RegistrationItemsService>();
        }

        private static IServiceCollection AddBus(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddMassTransit(x =>
            {
                x.AddConsumer<RegistrationRequestConsumer>();

                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(configuration["RabbitMq"]);

                    cfg.ConfigureEndpoints(context);
                });
            });
        }

        private static IServiceCollection AddMappings(this IServiceCollection services)
        {
            services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));

            return services;
        }

        private static IConfigurationProvider GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<RegistrationItemMappingProfile>();
            });

            configuration.AssertConfigurationIsValid();

            return configuration;
        }

    }
}
