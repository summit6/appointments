﻿namespace Appointments.Workers.Domain
{
    using System.Text;

    public static class WorkerExtensions
    {
        public static string ToSimpleRecord(this List<Speciality> specialities)
        {
            var result = new StringBuilder();

            result.AppendJoin(',', specialities);

            return result.ToString();
        }
    }
}
