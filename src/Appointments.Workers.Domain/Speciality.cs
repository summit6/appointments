﻿namespace Appointments.Workers.Domain
{
    using Appointments.Workers.Domain.Core;

    public class Speciality : BaseEntity
    {
        /// <summary>
        /// Наименование специальности
        /// </summary>
        public string Name { get; set;}

        public ICollection<Worker> Workers { get; set; } 

        public ICollection<WorkerSpeciality> WorkerSpecialities { get; set; } 

        public override string ToString()
        {
            return this.Name;
        }
    }
}