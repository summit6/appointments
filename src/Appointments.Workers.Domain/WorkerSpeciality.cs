﻿namespace Appointments.Workers.Domain
{
    using Appointments.Workers.Domain.Core;

    public class WorkerSpeciality : BaseEntity
    {
        public Guid WorkerId { get; set; }
        public Worker Worker { get; set; }

        public Guid SpecialityId { get; set; }
        public Speciality Speciality { get; set; }
    }
}
