﻿namespace Appointments.Workers.Domain
{
    using System.ComponentModel.DataAnnotations;
    using Appointments.Workers.Domain.Core;

    public class Worker : BaseEntity
    {
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string? SecondName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Код страны
        /// </summary>
        public const string PhonePrefix = "+7";

        /// <summary>
        /// Телефонный номер
        /// </summary>
        [MinLength(7)]
        public string? Phone { get; set; }

        public ICollection<Speciality> Specialities { get; set; }

        public ICollection<WorkerSpeciality> WorkerSpecialities { get; set; }
    }
}
