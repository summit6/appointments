import { useState, useEffect } from "react";
import { useNavigate, useParams } from 'react-router-dom';
import { FormProvider, useForm } from 'react-hook-form';
import { Button, Paper, Typography } from "@mui/material";
import { WorkerForm } from '../../../models/index'
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import WorkerCard from "../../../components/WorkerCard";

const WorkerDetail = () => {

    const navigate = useNavigate();
    const back = () => navigate('/workers');

    const { id } = useParams<{ id: string | undefined }>();
    const isAdd = id === "undefined";
    const [serverError, setServerError] = useState('');
    const [worker, setWorker] = useState<WorkerForm>();

    const methods = useForm<WorkerForm>({
        defaultValues: {
            firstName: worker?.firstName, secondName: worker?.secondName, lastName: worker?.lastName,
            phone: worker?.phone, specialities: worker?.specialities
        }
    })

    const { handleSubmit } = methods

    useEffect(() => {
        const findWorker = async () => {
            try {
                if (!isAdd) {
                    const response = await fetch(`${process.env.REACT_APP_API_WORKERS_URL}/${id}`);
                    const item: WorkerForm = await response.json();
                    setWorker(item);
                    console.log(`item:  ${item}`);
                    if (!item) {
                        console.log(`findWorker item ${id} is empty`);
                    }
                } else {
                    setWorker({
                        firstName: '',
                        secondName: '',
                        lastName: '',
                        phone: '',
                        specialities: []
                    });
                }
            } catch {
                console.log(`findWorker error ${id}`);
            }
        };
        findWorker();
    }, [isAdd, id]);

    const handleResponse = (response: { text: () => Promise<any>; ok: any; statusText: any; }) => {
        return response.text().then(text => {
            console.error('handleResponse!', text);
            const data = text;

            if (!response.ok) {
                const error = `${response.statusText}`;
                setServerError(error);
                return Promise.reject(error);
            } else {
                back();
            }

            return data;
        });
    }

    const save = (data: WorkerForm) => {
        setServerError("");
        console.log(data);

        if (isAdd)
            return fetch(`${process.env.REACT_APP_API_WORKERS_URL}`, {
                method: "POST",
                headers: { "Content-Type": "application/json", Accept: 'application/json', },
                body: JSON.stringify({
                    FirstName: data.firstName,
                    SecondName: data.secondName,
                    LastName: data.lastName,
                    Phone: getRawPhone(data.phone),
                    Specialities: data.specialities.map(s => s.id),
                }),
            })
                .then(handleResponse)
                .catch(error => {
                    console.error('There was an error!', error);
                });
        else {
            return fetch(`${process.env.REACT_APP_API_WORKERS_URL}/${id}`, {
                method: "PUT",
                headers: { "Content-Type": "application/json", Accept: 'application/json', },
                body: JSON.stringify({
                    Id: id,
                    FirstName: data.firstName,
                    SecondName: data.secondName,
                    LastName: data.lastName,
                    Phone: getRawPhone(data.phone),
                    Specialities: data.specialities.map(s => s.id),
                }),
            })
                .then(handleResponse)
                .catch(error => {
                    console.error('There was an error!', error);
                });
        }
    }

    const getRawPhone = (phone: string) => {
        return phone.replace(/[()-]/g, '');
    }


    return (
        <FormProvider {...methods}>
            <div className='container'>
                <Paper elevation={8} style={{ padding: 8, margin: 8 }} sx={{ inlineSize: 974 }} >
                    <div className="header-content">
                        <h1>{isAdd ? 'Введите нового сотрудника' : 'Редактировать'}</h1>
                        <div className="register">
                            <Button color="inherit" variant="outlined" startIcon={<ArrowBackIosNewIcon />} onClick={back}>Назад</Button>
                        </div>
                    </div>
                    {worker && <WorkerCard worker={worker} />}
                    <Button onClick={handleSubmit(save)}>Сохранить</Button>
                    <Typography variant="body1" component="div" style={{ color: 'red' }}>
                        {serverError && <p>{serverError || "Ошибка!"}</p>}
                    </Typography>
                </Paper>
            </div>
        </FormProvider >
    );
}

export default WorkerDetail;