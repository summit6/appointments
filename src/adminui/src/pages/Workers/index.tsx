import { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import { Chip, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Button, Box } from "@mui/material";
import { Done, Delete } from '@mui/icons-material';
import { Worker } from '../../models/index'
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';

const Workers = () => {

    const [workers, setWorkers] = useState<Worker[]>([]);

    const navigate = useNavigate();

    useEffect(() => {
        loadWorkers()
    }, [])

    const loadWorkers = async () => {
        const url: string = `${process.env.REACT_APP_API_WORKERS_URL}`;
        console.log(`url:  ${url}`);
        try {
            const response = await fetch(`${url}`);
            const items: Worker[] = await response.json()
            console.log(`items:  ${items}`);
            if (!items) {
                console.log(`items notFound`);
            }
            setWorkers(items);
        } catch {
            console.log(`items notFound, error`);
        }
    }
    const removeWorker = (id: string) => {
        fetch(`${process.env.REACT_APP_API_WORKERS_URL}/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(() => {
                console.log(`removed ${id}`);
                setWorkers(workers.filter(worker => worker.id !== id));
            })
            .catch(error => {
                console.error('There was an error while deleting!', error);
            });
    }

    function newWorker() {
        const id = undefined;
        navigate(`/workers/${id}`);
    }

    function editWorker(id: string) {
        navigate(`/workers/${id}`);
    }



    return (
        <div className='container'>
            <Paper elevation={8} style={{ padding: 8, margin: 8 }} sx={{ inlineSize: 974 }} >
                <div className="header-content">
                    <h1>Список сотрудников</h1>
                    <div className="register">
                        <Box
                            display="flex"
                            justifyContent="flex-end"
                            alignItems="flex-end"
                        >
                            <Button color="inherit" variant="outlined" startIcon={<AddCircleOutlineIcon />} style={{ paddingInlineEnd: 32 }} onClick={newWorker}>Создать нового</Button>
                        </Box>
                    </div>
                </div>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="center">ФИО</TableCell>
                                <TableCell align="center">Телефон</TableCell>
                                <TableCell align="center">Специальность</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {workers.map((worker) => (
                                <TableRow
                                    key={worker.id}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row">
                                        {worker.fullName}
                                    </TableCell>
                                    <TableCell align="right">{worker.phoneStr}</TableCell>
                                    <TableCell align="left">{worker.specialities.map(item => item.name).join(', ')}</TableCell>
                                    <TableCell align="right">
                                        <Chip
                                            label="Изменить"
                                            color="info"
                                            onClick={() => editWorker(worker.id)}
                                            onDelete={() => editWorker(worker.id)}
                                            deleteIcon={<Done />}
                                            style={{ marginRight: 10, padding: 2 }}
                                        />
                                        <Chip
                                            label="Удалить"
                                            color="error"
                                            onClick={() => removeWorker(worker.id)}
                                            deleteIcon={<Delete />}
                                        />
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        </div>
    );
}

export default Workers;