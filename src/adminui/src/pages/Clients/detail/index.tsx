import { useState, useEffect } from "react";
import { useNavigate, useParams } from 'react-router-dom';
import { FormProvider, useForm } from 'react-hook-form';
import { Button, Paper, Typography } from "@mui/material";
import { UserForm } from '../../../models/index'
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import ClientCard from "../../../components/ClientCard";

const ClientDetail = () => {

    const navigate = useNavigate();
    const back = () => navigate('/clients');

    const { id } = useParams<{ id: string | undefined }>();
    const isAdd = id === "undefined";
    const [serverError, setServerError] = useState('');
    const [client, setClient] = useState<UserForm>();

    const methods = useForm<UserForm>({
        defaultValues: {
            firstName: client?.firstName, secondName: client?.secondName, lastName: client?.lastName,
            birthDate: client?.birthDate, email: client?.email, phone: client?.phone
        }
    })

    const { handleSubmit } = methods

    useEffect(() => {
        const findClient = async () => {
            try {
                if (!isAdd) {
                    const response = await fetch(`${process.env.REACT_APP_API_CLIENTS_URL}/${id}`);
                    const item: UserForm = await response.json();
                    setClient(item);
                    console.log(`item:  ${item}`);
                    if (!item) {
                        console.log(`findClient item ${id} is empty`);
                    }
                } else {
                    setClient({
                        firstName: '',
                        secondName: '',
                        lastName: '',
                        email: '',
                        phone: '',
                        birthDate: new Date(`01.01.2001`),
                        //sexStr: '',
                    });
                }
            } catch {
                console.log(`findClient error ${id}`);
            }
        };
        findClient();
    }, [isAdd, id]);

    const handleResponse = (response: { text: () => Promise<any>; ok: any; statusText: any; }) => {
        return response.text().then(text => {
            console.error('handleResponse!', text);
            const data = text;

            if (!response.ok) {
                const error = `${response.statusText}`;
                setServerError(error);
                return Promise.reject(error);
            } else {
                back();
            }

            return data;
        });
    }

    const save = (data: UserForm) => {
        setServerError("");
        console.log(data);

        if (isAdd)
            return fetch(`${process.env.REACT_APP_API_CLIENTS_URL}`, {
                method: "POST",
                headers: { "Content-Type": "application/json", Accept: 'application/json', },
                body: JSON.stringify({
                    FirstName: data.firstName,
                    SecondName: data.secondName,
                    LastName: data.lastName,
                    Email: data.email,
                    Phone: getRawPhone(data.phone),
                    BirthDate: data.birthDate,
                    //sex: data.sexStr,
                }),
            })
            .then(handleResponse)
            .catch(error => {
                console.error('There was an error!', error);
            });
        else {
            return fetch(`${process.env.REACT_APP_API_CLIENTS_URL}/${id}`, {
                method: "PUT",
                headers: { "Content-Type": "application/json", Accept: 'application/json', },
                body: JSON.stringify({
                    Id: id,
                    FirstName: data.firstName,
                    SecondName: data.secondName,
                    LastName: data.lastName,
                    Email: data.email,
                    Phone: getRawPhone(data.phone),
                    BirthDate: data.birthDate,
                    //sex: data.sexStr,
                }),
            })
                .then(handleResponse)
                .catch(error => {
                    console.error('There was an error!', error);
                });
        }
    }

    const getRawPhone = (phone: string) => {
        return phone.substring(2).replace(/[()-]/g, '');
    }


     return (
        <FormProvider {...methods}>
            <div className='container'>
            <Paper elevation={8} style={{ padding: 8, margin: 8 }} sx={{ inlineSize: 974 }} >
                <div className="header-content">
                    <h1>{isAdd ? 'Введите нового клиента' : 'Редактировать'}</h1>
                    <div className="register">
                        <Button color="inherit" variant="outlined" startIcon={<ArrowBackIosNewIcon />} onClick={back}>Назад</Button>
                    </div>
                </div>
                     {client && <ClientCard client={client} />}
                <Button onClick={handleSubmit(save)}>Сохранить</Button>
                <Typography variant="body1" component="div"  style={{ color: 'red' }}>
                    {serverError && <p>{serverError || "Ошибка!"}</p>}
                </Typography>
            </Paper>
        </div>
        </FormProvider >
    );
}

export default ClientDetail;