import { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import { Chip, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Button, Box } from "@mui/material";
import { Done, Delete } from '@mui/icons-material';
import { User } from '../../models/index'
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';

const Clients = () => {

    const [clients, setClients] = useState<User[]>([]);

    const navigate = useNavigate();

    useEffect(() => {
        loadClients()
    }, [])

    const loadClients = async () => {
        const url: string = `${process.env.REACT_APP_API_CLIENTS_URL}`;
        console.log(`url:  ${url}`);
        try {
            const response = await fetch(`${url}`);
            const items: User[] = await response.json()
            console.log(`items:  ${items}`);
            if (!items) {
                console.log(`items notFound`);
            }
            setClients(items);
        } catch {
            console.log(`items notFound, error`);
        }
    }
    const removeClient = (id: string) => {
        fetch(`${process.env.REACT_APP_API_CLIENTS_URL}/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(() => {
                console.log(`removed ${id}`);
                setClients(clients.filter(client => client.id !== id));
            })
            .catch(error => {
                console.error('There was an error while deleting!', error);
            });
    }

    function newClient() {
        const id = undefined;
        navigate(`/clients/${id}`);
    }

    function editClient(id: string) {
        navigate(`/clients/${id}`);
    }



    return (
        <div className='container'>
            <Paper elevation={8} style={{ padding: 8, margin: 8 }} sx={{ inlineSize: 974 }} >
                <div className="header-content">
                    <h1>Список клиентов</h1>
                    <div className="register">
                        <Box
                            display="flex"
                            justifyContent="flex-end"
                            alignItems="flex-end"
                        >
                            <Button color="inherit" variant="outlined" startIcon={<AddCircleOutlineIcon />} style={{ paddingInlineEnd: 32 }} onClick={newClient}>Создать нового</Button>
                        </Box>
                    </div>
                </div>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="center">ФИО</TableCell>
                                <TableCell align="center">Телефон</TableCell>
                                <TableCell align="center">E-Mail</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {clients.map((client) => (
                                <TableRow
                                    key={client.id}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row">
                                        {client.fullName}
                                    </TableCell>
                                    <TableCell align="right">{client.phoneStr}</TableCell>
                                    <TableCell align="right">{client.email}</TableCell>
                                    <TableCell align="right">
                                        <Chip
                                            label="Изменить"
                                            color="info"
                                            onClick={() => editClient(client.id)}
                                            onDelete={() => editClient(client.id)}
                                            deleteIcon={<Done />}
                                            style={{ marginRight: 10, padding: 2 }}
                                        />
                                        <Chip
                                            label="Удалить"
                                            color="error"
                                            onClick={() => removeClient(client.id)}
                                            deleteIcon={<Delete />}
                                        />
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        </div>
    );
}

export default Clients;