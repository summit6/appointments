import { Button, IconButton, InputAdornment, Stack, TextField } from '@mui/material';
import { Login, Visibility, VisibilityOff } from '@mui/icons-material';
import { useNavigate } from 'react-router-dom';
import { useAuthContext } from '../../context';
import { useState } from 'react';

const DoLogin = () => {
    const { setAuth } = useAuthContext();

    const navigate = useNavigate();
    const [view, setView] = useState<Boolean>(false);

    const handleShowPassword = () => {
        setView(!view);
    }



    return (
        <div style={{ backgroundImage: 'url(/blue-nurse-med.jpg)', backgroundRepeat: "no-repeat", margin: "30px", paddingLeft: "400px", minHeight: "500px", minWidth: "400px" }} >
            <Stack direction="column" gap={3} padding={4} paddingTop={14}>
                <TextField itemID="login" label="login" color="primary" placeholder="Введите Ваш login" variant="standard" sx={{ width: 300 }} />
                <TextField
                    label="password"
                    color="primary"
                    type={view ? "text" : "password"}
                    placeholder="Введите пароль"
                    variant="standard"
                    sx={{ width: 300 }}
                    InputProps={{ // <-- This is where the toggle button is added.
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleShowPassword}
                                    onMouseDown={handleShowPassword}
                                >
                                    {view ? <Visibility /> : <VisibilityOff />}
                                </IconButton>
                            </InputAdornment>
                        )
                    }}                />
                <Button startIcon={<Login />} sx={{ width: 300 }}
                    onClick={() => {
                        setAuth(true); // устанавливаем флаг isAuthenticated в true
                        navigate('/home');
                    }}
                >Войти</Button>
            </Stack>
        </div >
    );
}

export default DoLogin;