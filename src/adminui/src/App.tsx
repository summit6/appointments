import { BrowserRouter } from 'react-router-dom';
import AppRoutes from './routes';
import Header from './components/Header';
import { AuthProvider } from './context';

function App() {
    return (
        <AuthProvider>
            <BrowserRouter>
                {/* ���������� �������� ��� �������� �������� isAuthenticated � ������� setAuth ���� �� �������� ����������� */}
                <Header />
                <AppRoutes />
            </BrowserRouter>
        </AuthProvider>
    );
}

export default App;
