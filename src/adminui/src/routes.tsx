import { Routes, Route, useLocation, Navigate, Outlet } from "react-router-dom";

import Home from './pages/Home';
import DoLogin from './pages/Login';
import Clients from './pages/Clients';
import ClientDetail from './pages/Clients/detail';
import Workers from './pages/Workers';
import WorkerDetail from './pages/Workers/detail';
import { useAuthContext } from "./context";

const PrivateRoute = () => {
    const { isAuthenticated } = useAuthContext(); // ���������� �������� ��� ��������� �������� isAuthenticated
    const location = useLocation(); // �������� ������� ������� � ������� ���� useLocation()

    return (
        // ���� ������������ �����������, �� �������� �������� �������� �������� ��������, ��������� ��������� Outlet
        isAuthenticated === true ?
            <Outlet />
            // ���� ������������ �� �����������, �� �������������� ��� �� ������� /login � ������� ���������� Navigate
            // �������� replace ���������, ��� ������� ������� ����� ������� �� �����, ����� ������������ �� ��� ���������
            // �������, ��������� ������ "�����" � ��������.
            :
            <Navigate to="/login" state={{ from: location }} replace />
    );
}

const AppRoutes = () => {
    return (
        <Routes>
            <Route path="/" element={<DoLogin />} />
            <Route path="/login" element={<DoLogin />} />
            {/* ���������� �������� */}
            <Route element={<PrivateRoute />}>
                <Route path="/home" element={<Home />} />
                <Route path="/clients" element={<Clients />} />
                <Route path="/clients/:id" element={<ClientDetail />} />
                <Route path="/workers" element={<Workers />}/>
                <Route path="/workers/:id" element={<WorkerDetail />}/>
            </Route>

        </Routes>
    );
}

export default AppRoutes;