import { PropsWithChildren, createContext, useState, useContext } from 'react';


// ���������� ��� ��������� � ����� ������: isAuthenticated � setAuth
export type AuthContextType = {
    isAuthenticated: boolean; // ����, ������������, ���������������� �� ������������
    setAuth: (auth: boolean) => void; // ������� ��� ��������� �������� isAuthenticated
};

// ������� �������� � ����� AuthContextType
export const AuthContext = createContext<AuthContextType | undefined>(undefined);

export const AuthProvider = ({ children }: PropsWithChildren<{}>) => {
    const [isAuthenticated, setAuth] = useState<boolean>(false); // ���������� ��������� isAuthenticated

    return (
        <AuthContext.Provider value={{ isAuthenticated, setAuth }}>
            {children}
        </AuthContext.Provider>
    );
};

export const useAuthContext = () => {
    const context = useContext(AuthContext);

    if (!context) {
        throw new Error('useAuthContext must be used inside the AuthProvider');
    }

    return context;
};

