export type User = {
    id: string
    fullName: string
    email: string
    phoneStr: string
}

export type UserForm = {
    firstName: string
    secondName: string
    lastName: string
    email: string
    phone: string
    birthDate: Date | undefined
}

export type UserResponse = {
    id: string
    fullName: string
    firstName: string
    secondName: string
    lastName: string
    email: string
    phoneStr: string
    phone: string
    birthDate: Date | undefined
    birthDateYear: number
    birthDateMonth: number
    birthDateDay: number
    age: number
    sexStr: string
}

export type CreateOrEditPartnerRequest = {
    firstName: string
    secondName: string
    lastName: string
    email: string
    phone: string
    birthDate: Date
    birthDateYear: number
    birthDateMonth: number
    birthDateDay: number
    sex: string
}

export type Worker = {
    id: string
    fullName: string
    phoneStr: string
    specialities: Speciality[]
}

export type WorkerForm = {
    firstName: string
    secondName: string
    lastName: string
    phone: string
    specialities: Speciality[]
}

export type WorkerResponse = {
    id: string
    fullName: string
    firstName: string
    secondName: string
    lastName: string
    phone: string
    phoneStr: string
   specialities: Speciality[]
}

export type Speciality = {
    id: string
    name: string
}
