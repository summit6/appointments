import { Controller, useFormContext } from "react-hook-form"
import { Card, CardContent, TextField } from "@mui/material";
import InputMask from "react-input-mask";
//import { useState } from "react";
import { UserForm } from "../../models";

const ClientCard = (props: { client: UserForm; }) => {
    const { client: { firstName, secondName, lastName, birthDate, email, phone } } = props;
    const { control } = useFormContext();
    const isEmail = (data: string) => {
        if (data.indexOf('@') < 0)
            return false;
        if (data.substring(data.indexOf('@')).indexOf('.') < 0)
            return false;
        return true;
    }

    return (
        <Card style={{ paddingLeft: 48, margin: 8, marginLeft: 120, marginRight: 100}} sx={{ minWidth: 150 }}>
            <CardContent>
                <div>
                    <Controller
                        name="lastName"
                        control={control}
                        defaultValue={lastName}
                        render={({ field: { value, onChange } }) => (
                            <TextField
                                value={value}
                                onChange={onChange}
                                id="lastName"
                                placeholder="Введите фамилию"
                                label="Фамилия"
                                variant="standard"
                                color="primary"
                                InputLabelProps={{ shrink: true }}
                                required
                                error={value.length === 0}
                            />
                        )}
                    />
                </div>
                <div>
                    <Controller
                        name="firstName"
                        control={control}
                        defaultValue={firstName}
                        render={({ field: { value, onChange } }) => (
                            <TextField
                                value={value}
                                onChange={onChange}
                                id="firstName"
                                placeholder="Введите имя"
                                label="Имя"
                                variant="standard"
                                color="primary"
                                InputLabelProps={{ shrink: true }}
                                required
                                error={value.length === 0}
                            />
                        )}
                    />
                </div>
                <div>
                    <Controller
                        name="secondName"
                        control={control}
                        defaultValue={secondName}
                        render={({ field: { value, onChange } }) => (
                            <TextField
                                value={value}
                                onChange={onChange}
                                id="secondName"
                                placeholder="Введите отчество"
                                label="Отчество"
                                variant="standard"
                                color="primary"
                                InputLabelProps={{ shrink: true }}
                            />
                        )}
                    />
                </div>
                <div>
                    <Controller
                        name="birthDate"
                        control={control}
                        defaultValue={birthDate}
                        render={({ field: { value, onChange } }) => (
                            <TextField
                                value={value}
                                onChange={onChange}
                                id="birthDate"
                                defaultValue={birthDate}
                                placeholder="Введите дату рождения"
                                label="Дата рождения"
                                variant="standard"
                                color="primary"
                                type="date"
                                InputLabelProps={{ shrink: true }}
                            />
                        )}
                    />
                </div>
                <div>
                    <Controller
                        name="phone"
                        control={control}
                        defaultValue={phone}
                        render={({ field: { value, onChange } }) => (
                            <InputMask
                                value={value}
                                onChange={onChange}
                                mask="+7(999)999-99-99"
                                alwaysShowMask
                            >
                                <TextField
                                    id="phone"
                                    label="Номер телефона"
                                    variant="standard"
                                    color="primary"
                                    InputLabelProps={{ shrink: true }}
                                />
                            </InputMask>
                        )}
                    />
                </div>
                <div>
                    <Controller
                        name="email"
                        control={control}
                        defaultValue={email}
                        render={({ field: { value, onChange } }) => (
                            <TextField
                                value={value}
                                onChange={onChange}
                                id="birthday"
                                placeholder="Введите Email"
                                label="Email"
                                variant="standard"
                                color="primary"
                                type="email"
                                //inputProps={{ inputMode: 'email' }}
                                InputLabelProps={{ shrink: true }}
                                error={!isEmail(value)}
                                helperText={!isEmail(value) ? "Введите корректный email" : ""}
                            />
                        )}
                    />
                </div>
            </CardContent>
        </Card>
    );
}

export default ClientCard;