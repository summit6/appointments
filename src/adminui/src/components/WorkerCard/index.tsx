import { useEffect, useState } from "react";
import { Controller, useFormContext } from "react-hook-form"
import { Card, CardContent, TextField, Autocomplete } from "@mui/material";
import InputMask from "react-input-mask";
import { WorkerForm, Speciality } from "../../models";

const WorkerCard = (props: { worker: WorkerForm; }) => {
    const [allSpecialities, setAllSpecialities] = useState<Speciality[]>([]);
    const { worker: { firstName, secondName, lastName, phone, specialities } } = props;
    const { control } = useFormContext();

    useEffect(() => {
        loadSpecialities()
    }, [])

    const loadSpecialities = async () => {
        const url: string = `${process.env.REACT_APP_API_SPECIALITIES_URL}`;
        console.log(`url:  ${url}`);
        try {
            const response = await fetch(`${url}`);
            const items: Speciality[] = await response.json()
            console.log(`items:  ${items}`);
            if (!items) {
                console.log(`items notFound`);
            }
            setAllSpecialities(items);
        } catch {
            console.log(`items notFound, error`);
        }
    }

    return (
        <Card style={{ paddingLeft: 48, margin: 8, marginLeft: 120, marginRight: 100 }} sx={{ minWidth: 150 }}>
            <CardContent>
                <div>
                    <Controller
                        name="lastName"
                        control={control}
                        defaultValue={lastName}
                        render={({ field: { value, onChange } }) => (
                            <TextField
                                value={value}
                                onChange={onChange}
                                id="lastName"
                                placeholder="Введите фамилию"
                                label="Фамилия"
                                variant="standard"
                                color="primary"
                                InputLabelProps={{ shrink: true }}
                                required
                                error={value.length === 0}
                            />
                        )}
                    />
                </div>
                <div>
                    <Controller
                        name="firstName"
                        control={control}
                        defaultValue={firstName}
                        render={({ field: { value, onChange } }) => (
                            <TextField
                                value={value}
                                onChange={onChange}
                                id="firstName"
                                placeholder="Введите имя"
                                label="Имя"
                                variant="standard"
                                color="primary"
                                InputLabelProps={{ shrink: true }}
                                required
                                error={value.length === 0}
                            />
                        )}
                    />
                </div>
                <div>
                    <Controller
                        name="secondName"
                        control={control}
                        defaultValue={secondName}
                        render={({ field: { value, onChange } }) => (
                            <TextField
                                value={value}
                                onChange={onChange}
                                id="secondName"
                                placeholder="Введите отчество"
                                label="Отчество"
                                variant="standard"
                                color="primary"
                                InputLabelProps={{ shrink: true }}
                            />
                        )}
                    />
                </div>
                <div>
                    <Controller
                        name="phone"
                        control={control}
                        defaultValue={phone}
                        render={({ field: { value, onChange } }) => (
                            <InputMask
                                value={value}
                                onChange={onChange}
                                mask="+7(999)999-99-99"
                                alwaysShowMask
                            >
                                <TextField
                                    id="phone"
                                    label="Номер телефона"
                                    variant="standard"
                                    color="primary"
                                    InputLabelProps={{ shrink: true }}
                                />
                            </InputMask>
                        )}
                    />
                </div>
                <div>
                    <Controller
                        name="specialities"
                        control={control}
                        defaultValue={specialities}
                        render={({ field: { value, onChange } }) => (
                            <Autocomplete
                                onChange={(e, data) => onChange(data)}
                                defaultValue={specialities}
                                isOptionEqualToValue={(option: Speciality, value: Speciality) => option.id === value.id}
                                multiple
                                options={allSpecialities}
                                getOptionLabel={(option) => option.name}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        variant="standard"
                                        label="Специальность"
                                        placeholder="Введите специальность"
                                    />
                                )}
                            />
                        )}
                    />
                </div>
            </CardContent>
        </Card>
    );
}

export default WorkerCard;