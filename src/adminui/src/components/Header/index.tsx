/* eslint-disable jsx-a11y/alt-text */
import { AppBar, Toolbar, IconButton, Typography, Stack, Button } from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import { MedicalInformation, Logout } from '@mui/icons-material';
import { useAuthContext } from '../../context';



const Header = () => {
    const { isAuthenticated, setAuth } = useAuthContext(); // используем контекст для получения значения isAuthenticated
    const navigate = useNavigate(); // используем хук useNavigate для навигации по маршрутам
    const doLogout = () => {
        setAuth(false);
        navigate("/");
    }

    return (
        <AppBar position="static" sx={{ left: 350, inlineSize: 1000 }} elevation={16} >
            <Toolbar>
                <IconButton size="large" edge="start" color="inherit" aria-label="logo">
                    <MedicalInformation />
                </IconButton>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    Appointments. Рабочее место Администратора.
                </Typography>
                {isAuthenticated &&
                    <Stack direction="row" spacing={2}>
                        <Button color="inherit"><Link to="/clients" style={{ textDecoration: "none", color: "white" }}>Клиенты</Link></Button>
                        <Button color="inherit"><Link to="/workers" style={{ textDecoration: "none", color: "white" }}>Сотрудники</Link></Button>
                        <Button color="inherit" startIcon={<Logout />} onClick={doLogout}>Выход</Button>
                    </Stack>}
            </Toolbar>
        </AppBar>
    );
}


export default Header;