﻿namespace Appointments.Registration.Services.Validation
{
    using Appointments.CrossSystemContracts.Registration;
    using Appointments.Registration.Services.Partners;
    using Appointments.Registration.Services.Workers;
    using FluentValidation;

    public class RegistrationRequestValidator :
        AbstractValidator<RegistrationRequest>, IRegistrationRequestValidator
    {
        private readonly IPartnersService partnersService;
        private readonly IWorkersService workersService;

        public RegistrationRequestValidator(IPartnersService partnersService, 
            IWorkersService workersService)
        {
            this.partnersService = partnersService;

            this.workersService = workersService;
            
            this.RuleFor(x => x.PartnerId)
                .NotNull()
                .MustAsync(this.PartnerMustExistsAsync)
                .WithMessage("Попытка записать несуществующего клиента");

            this.RuleFor(x => x.WorkerId)
                .NotNull()
                .MustAsync(this.WorkerMustExistsAsync)
                .WithMessage("Попытка записать клиента к несуществующему сотруднику");
        }

        private async Task<bool> WorkerMustExistsAsync(Guid workerId, CancellationToken cancellationToken)
        {
            return await this.workersService.EnsureWorkerExistsAsync(workerId, cancellationToken);
        }

        private async Task<bool> PartnerMustExistsAsync(Guid partnerId, CancellationToken cancellationToken)
        {
            return await this.partnersService.EnsurePartnerExistsAsync(partnerId, cancellationToken);
        }
    }
}
