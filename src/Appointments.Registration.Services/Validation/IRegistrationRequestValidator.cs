﻿namespace Appointments.Registration.Services.Validation;

using Appointments.CrossSystemContracts.Registration;
using FluentValidation.Results;

public interface IRegistrationRequestValidator
{
    public Task<ValidationResult> ValidateAsync(RegistrationRequest instance, CancellationToken cancellationToken);
}