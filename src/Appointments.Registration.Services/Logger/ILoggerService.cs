﻿namespace Appointments.Registration.Services.Logger;

public interface ILoggerService
{
    Task Log(string message);
}