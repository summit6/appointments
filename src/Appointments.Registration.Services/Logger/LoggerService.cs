﻿namespace Appointments.Registration.Services.Logger
{
    using Appointments.CrossSystemContracts.Logging;
    using MassTransit;

    public class LoggerService : ILoggerService
    {
        private readonly IBus bus;

        public LoggerService(IBus bus)
        {
            this.bus = bus;
        }

        public async Task Log(string message)
        {
            await this.bus.Publish(new LogDto
            {
                Message = message
            });
        }
    }
}
