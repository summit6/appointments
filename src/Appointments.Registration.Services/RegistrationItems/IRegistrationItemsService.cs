﻿namespace Appointments.Registration.Services.RegistrationItems;

using Appointments.Registration.Domain;

public interface IRegistrationItemsService
{
    /// <summary>
    /// Получить все записи к врачу
    /// </summary>
    /// <param name="id">Id врача</param>
    /// <returns></returns>
    Task<IEnumerable<RegistrationItem>> GetRegistrationsToWorker(Guid id);

    /// <summary>
    /// Получить все записи клиента
    /// </summary>
    /// <param name="id">Id клиента</param>
    /// <returns></returns>
    Task<IEnumerable<RegistrationItem>> GetPartnerRegistrations(Guid id);

    /// <summary>
    /// Получить все регистрации
    /// </summary>
    /// <returns></returns>
    Task<IEnumerable<RegistrationItem>> GetAllRegistrations();

    /// <summary>
    /// Получить регистрацию к врачу по id
    /// </summary>
    /// <param name="id">Id регистрации</param>
    /// <returns></returns>
    Task<RegistrationItem> GetRegistrationById(Guid id);

    /// <summary>
    /// Обновить данные регистрации
    /// </summary>
    /// <param name="registrationItem">Регистрация с новыми значениями</param>
    /// <returns></returns>
    Task UpdateRegistration(RegistrationItem registrationItem);

    /// <summary>
    /// Добавить регистрацию
    /// </summary>
    /// <param name="registrationItem">Новая регистрация</param>
    /// <returns></returns>
    Task<Guid> AddRegistration(RegistrationItem registrationItem);
}