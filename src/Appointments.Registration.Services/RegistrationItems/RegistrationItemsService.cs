﻿namespace Appointments.Registration.Services.RegistrationItems
{
    using Appointments.Registration.DataAccess.Repositories;
    using Appointments.Registration.Domain;

    public class RegistrationItemsService : IRegistrationItemsService
    {
        private readonly IRegistrationItemRepository registrationItemRepository;

        public RegistrationItemsService(IRegistrationItemRepository registrationItemRepository)
        {
            this.registrationItemRepository = registrationItemRepository;
        }

        public async Task<IEnumerable<RegistrationItem>> GetRegistrationsToWorker(Guid id)
        {
            return await this.registrationItemRepository.GetByWorkerId(id);
        }

        public async Task<IEnumerable<RegistrationItem>> GetPartnerRegistrations(Guid id)
        {
            return await this.registrationItemRepository.GetByPartnerId(id);
        }

        public async Task<IEnumerable<RegistrationItem>> GetAllRegistrations()
        {
            return await this.registrationItemRepository.GetAllAsync();
        }

        public async Task<RegistrationItem> GetRegistrationById(Guid id)
        {
            return await this.registrationItemRepository.GetByIdAsync(id);
        }

        public async Task UpdateRegistration(RegistrationItem registrationItem)
        {
            await this.registrationItemRepository.UpdateAsync(registrationItem);
        }

        public async Task<Guid> AddRegistration(RegistrationItem registrationItem)
        {
            return await this.registrationItemRepository.AddAsync(registrationItem);
        }
    }
}
