﻿namespace Appointments.Registration.Services.Partners;

using Appointments.CrossSystemContracts.Partners;

public interface IPartnersService
{
    Task<bool> EnsurePartnerExistsAsync(Guid partnerId, CancellationToken cancellationToken);

    Task<IEnumerable<PartnerContact>?> GetAllPartners(CancellationToken cancellationToken);
}