﻿namespace Appointments.Registration.Services.Partners
{
    using Appointments.CrossSystemContracts.Partners;
    using Microsoft.Extensions.Configuration;
    using RestSharp;
    using System.Threading;

    public class PartnersService : RestClient, IPartnersService
    {
        private readonly IConfiguration configuration;

        private readonly RestClientOptions options;

        private readonly RestClient client;

        public PartnersService(IConfiguration configuration)
        {
            this.configuration = configuration;

            if (string.IsNullOrEmpty(this.configuration["PartnersIdCheckApi"]))
                throw new ArgumentException("[Registration] - Error: PartnersApi base uri not provided!");

            this.options = new RestClientOptions(this.configuration["PartnersIdCheckApi"]);

            this.options.RemoteCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            this.client = new RestClient(this.options);
        }

        public async Task<bool> EnsurePartnerExistsAsync(Guid partnerId, CancellationToken cancellationToken)
        {
            var request = new RestRequest($"exists?id={partnerId}");

            return await this.client.GetAsync<bool>(request, cancellationToken);
        }   

        public async Task<IEnumerable<PartnerContact>?> GetAllPartners(CancellationToken cancellationToken)
        {
            var request = new RestRequest();

            return await this.client.GetAsync<IEnumerable<PartnerContact>>(request, cancellationToken);
        }
    }
}
