﻿namespace Appointments.Registration.Services.Workers;

using Appointments.CrossSystemContracts.Workers;

public interface IWorkersService
{
    Task<bool> EnsureWorkerExistsAsync(Guid workerId, CancellationToken cancellationToken);

    Task<WorkerFullNameAndSpecialitiesResponse?> GetFullNameAndSpecialities(Guid workerId, CancellationToken cancellationToken);
}