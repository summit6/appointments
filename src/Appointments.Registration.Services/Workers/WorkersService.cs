﻿namespace Appointments.Registration.Services.Workers
{
    using Appointments.CrossSystemContracts.Workers;
    using Microsoft.Extensions.Configuration;
    using RestSharp;

    public class WorkersService : RestClient, IWorkersService
    {
        private readonly IConfiguration configuration;

        private readonly RestClientOptions options;

        private readonly RestClient client;

        public WorkersService(IConfiguration configuration)
        {
            this.configuration = configuration;

            if (string.IsNullOrEmpty(this.configuration["WorkersIdCheckApi"]))
                throw new ArgumentException("[Registration] - Error: WorkersApi base uri not provided!");

            this.options = new RestClientOptions(this.configuration["WorkersIdCheckApi"]);

            this.options.RemoteCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            this.client = new RestClient(this.options);
        }

        public async Task<bool> EnsureWorkerExistsAsync(Guid workerId, CancellationToken cancellationToken)
        {
            var request = new RestRequest($"exists?id={workerId}");

            return await this.client.GetAsync<bool>(request, cancellationToken);
        }

        public async Task<WorkerFullNameAndSpecialitiesResponse?> GetFullNameAndSpecialities(Guid workerId, CancellationToken cancellationToken)
        {
            var request = new RestRequest($"partnerregistrations?id={workerId}");

            return await this.client.GetAsync<WorkerFullNameAndSpecialitiesResponse>(request, cancellationToken);
        }
    }
}
