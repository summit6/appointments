﻿global using Appointments.Logging.Domain.Interfaces;
global using Appointments.Logging.Domain.Interfaces.Repositories;
global using MongoDB.Driver;
global using Appointments.Logging.Domain.Entities;
global using Appointments.Logging.Domain;
global using Appointments.Logging.Domain.Configurations;
global using Microsoft.Extensions.Options;
global using MongoDB.Bson;
