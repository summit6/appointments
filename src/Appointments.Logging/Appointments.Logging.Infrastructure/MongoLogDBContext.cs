﻿namespace Appointments.Logging.Infrastructure;

public class MongoLogDBContext: IMongoLogDBContext
{
    private IMongoDatabase _db { get; set; }

    private MongoClient _mongoClient { get; set; }

    public IClientSessionHandle Session { get; set; }

    public MongoLogDBContext(IOptions<AppSettings> configuration)
    {
        _mongoClient = new MongoClient(configuration.Value.ConnectionStrings.MongoDbConnectionString);
        _db = _mongoClient.GetDatabase(configuration.Value.ConnectionStrings.MongoDbName);

        if(!IsCollectionExists("Log"))
            _db.CreateCollection("Log");
    }

    public IMongoCollection<T> GetCollection<T>(string name)
    {
        return _db.GetCollection<T>(name);
    }

    public bool IsCollectionExists(string collectionName)
    {
        IMongoCollection<BsonDocument> mongoCollection = _db.GetCollection<BsonDocument>(collectionName);

        if (mongoCollection != null)
        {
            return true;
        }

        return false;
    }
}
