﻿using Appointments.CrossSystemContracts.Logging;
using MongoDB.Bson;

namespace Appointments.Logging.Infrastructure.Repositories;

/// <summary>
/// Репозиторий для работы с бд Mongo
/// </summary>
public class LogToMongoDbRepository : ILogToDbRepository
{
    protected readonly IMongoLogDBContext _mongoContext;

    protected IMongoCollection<Log> _dbCollection;

    public LogToMongoDbRepository(IMongoLogDBContext context)
    {
        _mongoContext = context;
        _dbCollection = _mongoContext.GetCollection<Log>(typeof(Log).Name);

    }

    public async Task<IEnumerable<Log>> GetLogsAsync(int limit = 100)
        => await _dbCollection.Find(new BsonDocument()).SortByDescending(x=>x.TimeStamp).Limit(limit).ToListAsync();

    public void WriteLog(IRequestLogging requestLogging)
    {
        throw new NotImplementedException();
    }

    public async Task WriteLogAsync(LogDto log)
    {
        ArgumentNullException.ThrowIfNull(log, nameof(log));

        await _dbCollection.InsertOneAsync(new Log
        {
            Id = Guid.NewGuid(),
            Exception = log.Exception,
            Message = log.Message,
            Ip = log.Ip,
            Level = log.Level,
            LogEvent = log.LogEvent,
            MessageTemplate = log.MessageTemplate,
            Properties = log.Properties,
            TimeStamp = DateTime.UtcNow,
            UserName = log.UserName
        });
    }
}
