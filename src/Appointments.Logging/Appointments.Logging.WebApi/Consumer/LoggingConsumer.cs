﻿
namespace Appointments.Logging.WebApi.Consumer
{
    public class LoggingConsumer : IConsumer<LogDto>
    {
        private readonly ILogger _logger;
        private readonly ILogToDbRepository _loggingToDbRepository;

        public LoggingConsumer(ILogger<LoggingConsumer> logger, ILogToDbRepository loggingToDbRepository)
        {
            _logger = logger;
            _loggingToDbRepository = loggingToDbRepository;
        }

        public async Task Consume(ConsumeContext<LogDto> context)
        {
            await _loggingToDbRepository.WriteLogAsync(context.Message);
        }
    }
}
