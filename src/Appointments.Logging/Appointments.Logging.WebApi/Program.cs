
var builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<AppSettings>(builder.Configuration);

var configuration = builder.Configuration.Get<AppSettings>();

builder.Services.AddSingleton<IMongoLogDBContext, MongoLogDBContext>();
builder.Services.AddScoped<ILogToDbRepository, LogToMongoDbRepository>();

builder.Services.AddMassTransit(x=>
{
    x.AddConsumer<LoggingConsumer>();

    x.UsingRabbitMq((context, cfg) =>
    {
        cfg.Host(configuration.ConnectionStrings.RabbitConnection);
        cfg.ConfigureEndpoints(context, SnakeCaseEndpointNameFormatter.Instance);
    });
});

builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
    options.CustomOperationIds(e => $"{Regex.Replace(e.RelativePath, "{|}", "")}{e.HttpMethod}");
    options.SwaggerDoc("v1", new OpenApiInfo { Title = "Appointments.Logging", Version = "v1" });
    var xmlFiles = Directory.GetFiles(AppContext.BaseDirectory, "*.xml", SearchOption.TopDirectoryOnly).ToList();
    xmlFiles.ForEach(xmlFile => options.IncludeXmlComments(xmlFile));
    options.SchemaFilter<ExampleSchemaFilter>();
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "Appointments.Logging v1");
    });
}

app.UseRouting();

app.UseAuthorization();

app.MapControllers();

app.Run();
