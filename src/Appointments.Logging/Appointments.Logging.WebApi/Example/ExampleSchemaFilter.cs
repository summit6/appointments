﻿namespace Appointments.Logging.WebApi.Example;

public class ExampleSchemaFilter : ISchemaFilter
{
    private readonly JsonSerializerSettings _settings = new()
    {
        Formatting = Formatting.Indented,
        NullValueHandling = NullValueHandling.Ignore,
        Converters = new List<JsonConverter>
        {
            new StringEnumConverter()
        },
        ContractResolver = new DefaultContractResolver()
        {
            NamingStrategy = new CamelCaseNamingStrategy()
        },
    };

    private static readonly ProblemDetails ProblemDetailsObject = new()
    {
        Type = "Microsoft.AspNetCore.Http.BadHttpRequestException",
        Title = "One or more validation errors occurred",
        Status = 400
    };

    private static readonly RequestLoggingDto RequestLoggingDtoObject = new()
    {
        Id = 1,
        IpAddress = "192.168.123.132",
        Method = "Post",
        RequestDate = DateTime.Now,
        ResponseDate = DateTime.Now.AddSeconds(2),
        Request = "partner info for registration",
        Response = "id request",
        StatusCode = 200,
        Url = "/appointments/registration/partner"
    };

    public void Apply(OpenApiSchema schema, SchemaFilterContext context)
    {
        object? example = context.Type.Name switch
        {
            nameof(ProblemDetails) => ProblemDetailsObject,
            nameof(RequestLoggingDto) => RequestLoggingDtoObject,
            _ => null
        };

        if (example is not null)
        {
            schema.Example = new OpenApiString(JsonConvert.SerializeObject(example, _settings));
        }
    }
}
