namespace Appointments.Logging.WebApi.Controllers
{
    [ApiController]
    [Route("/api/appointments/[controller]")]
    public class LoggingController : ControllerBase
    {
        private readonly ILogger<LoggingController> _logger;
        private readonly ILogToDbRepository _loggingToDbRepository;

        public LoggingController(ILogger<LoggingController> logger, ILogToDbRepository loggingToDbRepository)
        {
            _logger = logger;
            _loggingToDbRepository = loggingToDbRepository;
        }

        /// <summary>
        /// ����� �����������
        /// </summary>
        /// <param name="log">������ ��� �����������</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType(typeof(ProblemDetails))]
        public async Task<IActionResult> SendLog(LogDto log)
        {
            await _loggingToDbRepository.WriteLogAsync(log);

            _logger.LogInformation("������ ��������� �������.");

            return Ok();
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Log>), StatusCodes.Status200OK)]
        [ProducesDefaultResponseType(typeof(ProblemDetails))]
        public async Task<IActionResult> GetLogs(int limit = 100)
        {
            var logs = await _loggingToDbRepository.GetLogsAsync(limit);

            return Ok(logs);
        }
    }
}