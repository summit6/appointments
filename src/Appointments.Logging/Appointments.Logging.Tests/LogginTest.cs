using Appointments.Logging.Domain.Configurations;
using Appointments.Logging.Domain.Entities;
using Appointments.Logging.Infrastructure.Repositories;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Moq;

namespace Appointments.Logging.Tests;

using Appointments.CrossSystemContracts.Logging;

public class LogginTest
{
    private Mock<IOptions<ConnectionStrings>> _mockOptions;

    private Mock<IMongoDatabase> _mockDB;

    private Mock<IMongoClient> _mockClient;

    private Mock<IMongoLogDBContext> _mockContext;

    public LogginTest()
    {
        _mockOptions = new Mock<IOptions<ConnectionStrings>>();
        _mockDB = new Mock<IMongoDatabase>();
        _mockClient = new Mock<IMongoClient>();
        _mockContext = new Mock<IMongoLogDBContext>();
    }

    [Fact]
    public async void LogRepository_WriteLog_Success()
    {
        //Arrange
        var log = new LogDto()
        {
            Id = 1,
            Ip = "192.168.123.132",
            Message = "partner info for registration"
        };

        var mockCollection = new Mock<IMongoCollection<Log>>();

        mockCollection.Setup(op => op.InsertOneAsync(It.IsAny<Log>(), null, default)).Returns(Task.CompletedTask);

        _mockContext.Setup(c => c.GetCollection<Log>(typeof(Log).Name)).Returns(mockCollection.Object);

        var logToMongoDbRepository = new LogToMongoDbRepository(_mockContext.Object);

        //Act
        await logToMongoDbRepository.WriteLogAsync(log);

        //Assert
        mockCollection.Verify(c => c.InsertOneAsync(It.Is<Log>(x=>x.Ip == "192.168.123.132"), null, default), Times.Once);
    }
}