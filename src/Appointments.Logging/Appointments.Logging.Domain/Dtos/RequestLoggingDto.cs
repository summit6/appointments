﻿namespace Appointments.Logging.Domain.Dtos;

public class RequestLoggingDto : IRequestLogging
{
    public int Id { get; set; }

    public DateTime RequestDate { get; set; }

    public DateTime ResponseDate { get; set; }

    public string IpAddress { get; set; } = string.Empty;

    public string Url { get; set; } = string.Empty;

    public string Request { get; set; } = string.Empty;

    public string Response { get; set; } = string.Empty;

    public string Method { get; set; } = string.Empty;

    public int StatusCode { get; set; }
}
