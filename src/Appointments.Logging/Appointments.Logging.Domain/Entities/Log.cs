﻿namespace Appointments.Logging.Domain.Entities;

public class Log: ILog
{
    public Guid Id { get; set; }

    public string Message { get; set; } = string.Empty;

    public string? MessageTemplate { get; set; }

    public string? Level { get; set; }

    public DateTime TimeStamp { get; set; }

    public string? Exception { get; set; }

    public string? Properties { get; set; }

    public string? LogEvent { get; set; }

    public string? UserName { get; set; }

    public string? Ip { get; set; }
}
