﻿global using Appointments.Logging.Domain.Interfaces;
global using MongoDB.Driver;
global using Appointments.Logging.Domain.Entities;