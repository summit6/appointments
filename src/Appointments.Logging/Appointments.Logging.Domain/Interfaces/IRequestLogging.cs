﻿namespace Appointments.Logging.Domain.Interfaces;

/// <summary>
/// Интерфейс запроса для логирования
/// </summary>
public interface IRequestLogging
{
    public int Id { get; set; }

    public DateTime RequestDate { get; set; }

    public DateTime ResponseDate { get; set; }

    public string IpAddress { get; set; }

    public string Url { get; set; }

    public string Request { get; set; }

    public string Response { get; set; }

    public string Method { get; set; }

    public int StatusCode { get; set; }
}
