﻿namespace Appointments.Logging.Domain.Interfaces.Repositories;

public interface IMongoLogDBContext
{
    IMongoCollection<Log> GetCollection<Log>(string name);
}
