﻿using Appointments.CrossSystemContracts.Logging;

namespace Appointments.Logging.Domain.Interfaces.Repositories;

/// <summary>
/// Интерфейс репозитория для работы с базой данных
/// </summary>
public interface ILogToDbRepository
{
    /// <summary>
    /// Запись лога
    /// </summary>
    /// <param name="requestLogging">Запрос для логирования</param>
    public void WriteLog(IRequestLogging requestLogging);

    /// <summary>
    /// Запись лога
    /// </summary>
    /// <param name="requestLogging">Запрос для логирования</param>
    public Task WriteLogAsync(LogDto log);

    public Task<IEnumerable<Log>> GetLogsAsync(int limit = 100);
}
