﻿namespace Appointments.Logging.Domain.Configurations;

public class ConnectionStrings
{
    public string MongoDbConnectionString { get; set; } = default!;

    public string MongoDbName { get; set; } = default!;

    public string RabbitConnection { get; set; } = default!;
}
