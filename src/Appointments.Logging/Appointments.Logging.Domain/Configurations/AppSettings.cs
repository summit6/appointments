﻿namespace Appointments.Logging.Domain.Configurations;

public class AppSettings
{
    public ConnectionStrings ConnectionStrings { get; set; } = default!;
}
