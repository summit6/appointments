﻿namespace Appointments.CrossSystemContracts.Workers
{
    /// <summary>
    /// DTO для ответа от Workers API
    /// </summary>
    public class WorkerFullNameAndSpecialitiesResponse
    {
        /// <summary>
        /// ФИО специалиста
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Строка со специальностями через запятую
        /// </summary>
        public string SpecialitiesRecord { get; set; }
    }
}
