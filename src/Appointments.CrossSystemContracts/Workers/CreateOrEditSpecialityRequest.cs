﻿namespace Appointments.CrossSystemContracts.Workers
{
    /// <summary>
    /// Запрос на добавление/редактирование специальности
    /// </summary>
    public class CreateOrEditSpecialityRequest
    {
        /// <summary>
        /// Название специальности
        /// </summary>
        public string Name { get; set; }
    }
}
