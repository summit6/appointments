﻿using System.ComponentModel.DataAnnotations;

namespace Appointments.CrossSystemContracts.Workers
{
    public class WorkerResponse
    {
        /// <summary>
        /// Идентификатор клиента
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// ФИО
        /// </summary>
        public string FullName { get; set; }
            
        /// <summary>
        /// Номер телефона в отформатированном виде
        /// </summary>
        public string? PhoneStr { get; set; }

        /// <summary>
        /// Список специальностей
        /// </summary>
        public List<SpecialityResponse> Specialities { get; set; }

        public string FirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string? SecondName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Телефонный номер
        /// </summary>
        public string? Phone { get; set; }
    }
}
