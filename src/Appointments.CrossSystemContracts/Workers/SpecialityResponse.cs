﻿namespace Appointments.CrossSystemContracts.Workers
{
    public class SpecialityResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
