﻿namespace Appointments.CrossSystemContracts.Workers
{
    public class WorkerBusyTimeResponse
    {
        public WorkerBusyTimeResponse()
        {
            this.DateTimes = new List<DateTime>();
        }

        public List<DateTime> DateTimes { get; set; }
    }
}
