﻿namespace Appointments.CrossSystemContracts.Workers
{
    /// <summary>
    /// Запрос на создание/удаление специалиста
    /// </summary>
    public class CreateOrEditWorkerRequest
    {
        /// <summary>
        /// Имя
        /// </summary>
        public string? FirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string? SecondName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string? LastName { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string? Phone { get; set; }

        /// <summary>
        /// Специальности
        /// </summary>
        public List<Guid> Specialities { get; set; }
    }
}
