﻿namespace Appointments.CrossSystemContracts.Workers
{
    public class WorkerContact
    {
        /// <summary>
        /// Идентификатор клиента
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// ФИО
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Номер телефона в отформатированном виде
        /// </summary>
        public string PhoneStr { get; set; }

    }
}
