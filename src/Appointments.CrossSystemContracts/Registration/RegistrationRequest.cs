﻿namespace Appointments.CrossSystemContracts.Registration
{
    /// <summary>
    /// Запрос на регистрацию клиента к специалисту
    /// </summary>
    public class RegistrationRequest
    {

        /// <summary>
        /// Id клиента
        /// </summary>
        public Guid PartnerId { get; set; }
        
        /// <summary>
        /// Id исполнителя
        /// </summary>
        public Guid WorkerId { get; set; }

        /// <summary>
        /// Дата и время, на которое производится запись
        /// </summary>
        public DateTime ScheduledOnTime { get; set; }

        /// <summary>
        /// Дополнительная информация 
        /// </summary>  
        public string? Information { get; set; }

    }
}