﻿namespace Appointments.CrossSystemContracts.Registration
{
    /// <summary>
    /// Запрос на получение регистраций клиента
    /// </summary>
    public class GetPartnerRegistrationsRequest
    {
        /// <summary>
        /// Id клиента
        /// </summary>
        public Guid PartnerId { get; set; }
    }
}
