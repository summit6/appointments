﻿namespace Appointments.CrossSystemContracts.Registration
{
    using System;

    /// <summary>
    /// Модель - проекция информации о клиенте, записанному к специалисту
    /// </summary>
    public class WorkerRegistrationModel
    {
        /// <summary>
        /// Id регистрации
        /// </summary>
        public Guid RegistrationItemId { get; set; }

        /// <summary>
        /// Id клиента
        /// </summary>
        public Guid PartnerId { get; set; }

        /// <summary>
        /// ФИО клиента
        /// </summary>
        public string? FullName { get; set; }

        /// <summary>
        /// Эл. почта
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// Номер телефона в отформатированном виде
        /// </summary>
        public string? PhoneStr { get; set; }

        /// <summary>
        /// Дата и время, на которое клиент записан
        /// </summary>
        public DateTime ScheduledOnTime { get; set; }
    }
}
