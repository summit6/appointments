﻿namespace Appointments.CrossSystemContracts.Registration;

/// <summary>
/// Модель - проекция информации о записи клиента к специалисту
/// </summary>
public class PartnerRegistrationModel
{
    /// <summary>
    /// Id регистрации
    /// </summary>
    public Guid RegistrationItemId { get; set; }

    /// <summary>
    /// Id специалиста
    /// </summary>
    public Guid WorkerId { get; set; }

    /// <summary>
    /// ФИО специалиста
    /// </summary>
    public string WorkerFullName { get; set; }

    /// <summary>
    /// Строка со специальностями через запятую
    /// </summary>
    public string SpecialitiesRecord { get; set; }

    /// <summary>
    /// Дата и время, на которое клиент записан
    /// </summary>
    public DateTime ScheduledOnTime { get; set; }
}