﻿namespace Appointments.CrossSystemContracts.Registration
{
    public class PartnerRegistrationsResponse
    {
        public Guid PartnerId { get; set; }

        public List<PartnerRegistrationModel> PartnerRegistrations { get; set; }
    }
}
