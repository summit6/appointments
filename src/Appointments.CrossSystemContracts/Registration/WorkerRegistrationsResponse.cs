﻿namespace Appointments.CrossSystemContracts.Registration
{
    using System;
    using System.Collections.Generic;

    public class WorkerRegistrationsResponse
    {
        public Guid WorkerId { get; set; }

        public List<WorkerRegistrationModel> WorkerRegistrations { get; set; }
    }
}
