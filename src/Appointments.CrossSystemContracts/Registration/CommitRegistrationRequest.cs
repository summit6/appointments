﻿namespace Appointments.CrossSystemContracts.Registration
{
    /// <summary>
    /// Запрос на закрытие регистрации
    /// </summary>
    public class CommitRegistrationRequest
    {
        /// <summary>
        /// Id регистрации
        /// </summary>
        public Guid RegistrationItemId { get; set; }

        /// <summary>
        /// Информация о результатах приёма
        /// </summary>
        public string Information { get; set; }
    }
}
