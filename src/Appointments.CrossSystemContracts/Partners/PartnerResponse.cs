﻿namespace Appointments.CrossSystemContracts.Partners
{
    public class PartnerResponse
    {
        /// <summary>
        /// Идентификатор клиента
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// ФИО
        /// </summary>
        public string? FullName { get; set; }

        /// <summary>
        ///  Имя
        /// </summary>
        public string? FirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string? SecondName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string? LastName { get; set; }

        /// <summary>
        /// Эл. почта
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// Номер телефона в отформатированном виде
        /// </summary>
        public string? PhoneStr { get; set; }

        /// <summary>
        /// Телефонный номер
        /// </summary>
        public string? Phone { get; set; }

        public int BirthDateYear { get; set; }
        public int BirthDateMonth { get; set; }
        public int BirthDateDay { get; set; }
        public DateOnly BirthDate { get; set; }

        /// <summary>
        /// Возраст
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Пол
        /// </summary>
        public string? SexStr { get; set; }
    }
}
