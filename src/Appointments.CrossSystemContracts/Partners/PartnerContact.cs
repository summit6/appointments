﻿using System.ComponentModel.DataAnnotations;

namespace Appointments.CrossSystemContracts.Partners
{
    public class PartnerContact
    {
        /// <summary>
        /// Идентификатор клиента
        /// </summary>
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string? SecondName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// ФИО
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Эл. почта
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// Номер телефона в отформатированном виде
        /// </summary>
        public string PhoneStr { get; set; }

    }
}
