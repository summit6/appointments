﻿namespace Appointments.CrossSystemContracts.Partners
{
    public class CreateOrEditPartnerRequest
    {
        public string? FirstName { get; set; }
        public string? SecondName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public int BirthDateYear { get; set; }
        public int BirthDateMonth { get; set; }
        public int BirthDateDay { get; set; }
        public DateOnly? BirthDate { get; set; }
        public string? Sex { get; set; }
    }
}
