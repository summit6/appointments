﻿namespace DocAppointment.DomainModel
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
