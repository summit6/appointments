﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace DocAppointment.DomainModel.PartnerModels
{
    public class PartnerAppointment : BaseEntity
    {
        public Guid PartnerId { get; set; }

        [ForeignKey("PartnerId")]
        public Partner Partner { get; set; }

        /// <summary>
        /// Дата и время приема
        /// </summary>
        public DateTime AppointmentDate { get; set; }

        /// <summary>
        /// Жалобы пациента, наблюдения врача
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// Назначения
        /// </summary>
        public string? Prescription { get; set; }

        /// <summary>
        /// Диагноз
        /// </summary>
        public string? Diagnosis { get; set; }
    }
}
