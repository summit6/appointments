﻿using System.ComponentModel.DataAnnotations;

namespace DocAppointment.DomainModel.PartnerModels
{
    /// <summary>
    /// Клиент
    /// </summary>
    public class Partner : BaseEntity
    {
        /// <summary>
        ///  Имя
        /// </summary>
        [MaxLength(100)]
        public string FirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        [MaxLength(100)]
        public string? SecondName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        [MaxLength(100)]
        public string LastName { get; set; }

        /// <summary>
        /// Эл. почта
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// Код страны
        /// </summary>
        public const string PhonePrefix = "+7";

        /// <summary>
        /// Телефонный номер
        /// </summary>
        [StringLength(12, MinimumLength = 7)]
        public string? Phone { get; set; }

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateOnly BirthDate { get; set; }

        /// <summary>
        /// Пол
        /// </summary>
        public SexEnum Sex { get; set; }

        public enum SexEnum
        {
            Муж,
            Жен
        }
    }
}
