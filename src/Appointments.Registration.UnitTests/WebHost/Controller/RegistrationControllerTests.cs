﻿namespace Appointments.Registration.UnitTests.WebHost.Controller
{
    using Appointments.Registration.UnitTests.Factories;
    using Appointments.Registration.Host;
    using Xunit;
    using Microsoft.AspNetCore.Mvc.Testing;
    using System.Net.Http.Json;
    using System.Net;
    using Appointments.CrossSystemContracts.Registration;
    using Appointments.Registration.Domain;
    using AutoFixture;
    using FluentAssertions;
    using Newtonsoft.Json;
    using Appointments.CrossSystemContracts.Workers;

    public class RegistrationControllerTests : IClassFixture<RegistrationWebApplicationFactory<Program>>
    {
        private readonly WebApplicationFactory<Program> factory;
        private readonly Fixture fixture;
        private readonly HttpClient client;

        public RegistrationControllerTests(RegistrationWebApplicationFactory<Program> factory)
        {
            this.factory = factory;

            this.fixture = new Fixture();

            this.client = this.factory.CreateClient();
        }

        [Fact]
        public async Task RegistrationShouldBeCreatedSuccessfully()
        {
            //Arrange 

            var registrationRequest = this.fixture.Create<RegistrationRequest>();

            var registrationResponse = await this.client.PostAsJsonAsync("/api/v1/registration/register", registrationRequest);

            var registrationItemId = JsonConvert.DeserializeObject<Guid>(await registrationResponse.Content.ReadAsStringAsync());

            //Act
            var allRegistrationsResponse = await this.client.GetAsync($"/api/v1/registration/all");

            //Assert
            allRegistrationsResponse.IsSuccessStatusCode.Should().BeTrue();
            allRegistrationsResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            var registrationItems = JsonConvert
                .DeserializeObject<List<RegistrationItem>>(
                    await allRegistrationsResponse.Content.ReadAsStringAsync());
            registrationItems.Should().NotBeNull();
            registrationItems.Should().NotBeEmpty();
            registrationItems.Should().Contain(r => r.Id == registrationItemId);
        }

        [Fact]
        public async Task CanGetWorkerBusyTime()
        {
            //Act
            var workerBusyTimeResponse = 
                await this.client.GetAsync($"/api/v1/registration/workerbusytime?id={FakeDataFactory.RegistrationItems.First().WorkerId}");
            //Assert
            workerBusyTimeResponse.IsSuccessStatusCode.Should().BeTrue();
            workerBusyTimeResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var busyTime = JsonConvert
                .DeserializeObject<WorkerBusyTimeResponse>(await workerBusyTimeResponse.Content.ReadAsStringAsync());

            busyTime.Should().NotBeNull();
            busyTime?.DateTimes.Should().NotBeNull();
            busyTime?.DateTimes.Should().NotBeEmpty();
            busyTime?.DateTimes.Should().Contain(d => d > DateTime.Now.AddDays(-1));
        } 
        
        [Fact]
        public async Task CanGetWorkerRegistrations()
        {
            //Act
            var workerRegistrationsResponse = 
                await this.client.GetAsync("/api/v1/registration/workerregistrations?id=553B5F8C-42CC-475B-AE6A-246BAB2FE767");
            //Assert
            workerRegistrationsResponse.IsSuccessStatusCode.Should().BeTrue();
            workerRegistrationsResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var workerRegistrations = JsonConvert
                .DeserializeObject<WorkerRegistrationsResponse>(await workerRegistrationsResponse.Content.ReadAsStringAsync());

            workerRegistrations.Should().NotBeNull();
            workerRegistrations?.WorkerId.Should().Be(Guid.Parse("553B5F8C-42CC-475B-AE6A-246BAB2FE767"));
            workerRegistrations?.WorkerRegistrations.Should().NotBeEmpty();
            workerRegistrations?.WorkerRegistrations.Should().HaveCount(1);
        }

        [Fact]
        public async Task CanGetPartnerRegistrations()
        {
            //Act
            var partnerRegistrationsResponse = 
                await this.client.GetAsync("/api/v1/registration/partnerregistrations?id=451533d5-d8d5-4a11-9c7b-eb9f14e1a32f");
            //Assert
            partnerRegistrationsResponse.IsSuccessStatusCode.Should().BeTrue();
            partnerRegistrationsResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var partnerRegistrations = JsonConvert
                .DeserializeObject<PartnerRegistrationsResponse>(await partnerRegistrationsResponse.Content.ReadAsStringAsync());

            partnerRegistrations.Should().NotBeNull();
            partnerRegistrations?.PartnerId.Should().Be(Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"));
            partnerRegistrations?.PartnerRegistrations.Should().NotBeEmpty();
            partnerRegistrations?.PartnerRegistrations.Should().HaveCount(2);
        }

        [Fact]
        public async Task CanGetAllRegistrations()
        {
            //Act
            var registrationItemsResponse = 
                await this.client.GetAsync("/api/v1/registration/all");
            //Assert
            registrationItemsResponse.IsSuccessStatusCode.Should().BeTrue();
            registrationItemsResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var registrationItems = (JsonConvert
                    .DeserializeObject<IEnumerable<RegistrationItem>>(
                        await registrationItemsResponse.Content.ReadAsStringAsync()) 
                                     ?? Array.Empty<RegistrationItem>()).ToList();

            registrationItems.Should().NotBeNull();
            registrationItems.Should().HaveCount(4);
        }

        [Fact]
        public async Task CanCommitRegistration()
        {
            //Arrange 
            var registrationItemsResponse =
                await this.client.GetAsync("/api/v1/registration/all");

            var registrationItems = (JsonConvert
                                         .DeserializeObject<IEnumerable<RegistrationItem>>(
                                             await registrationItemsResponse.Content.ReadAsStringAsync())
                                     ?? Array.Empty<RegistrationItem>()).ToList();

            CommitRegistrationRequest commitRegistrationRequest = this.fixture.Create<CommitRegistrationRequest>();

            commitRegistrationRequest.RegistrationItemId = registrationItems.First(r => r.IsEnabled).Id;
            
            //Act
            await this.client.PutAsJsonAsync("/api/v1/registration/commitregistration", commitRegistrationRequest);

            registrationItemsResponse =
                await this.client.GetAsync("/api/v1/registration/all");

            //Assert
            registrationItemsResponse.IsSuccessStatusCode.Should().BeTrue();
            registrationItemsResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            registrationItems = (JsonConvert
                    .DeserializeObject<IEnumerable<RegistrationItem>>(
                        await registrationItemsResponse.Content.ReadAsStringAsync()) 
                                     ?? Array.Empty<RegistrationItem>()).ToList();

            registrationItems.Should().NotBeNull();
            registrationItems.Should().HaveCount(4);
            registrationItems.Should().Contain(
                r => r.Id == commitRegistrationRequest.RegistrationItemId && r.IsEnabled == false);
        }

        
    }
}
