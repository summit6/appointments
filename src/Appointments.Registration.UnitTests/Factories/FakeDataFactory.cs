﻿namespace Appointments.Registration.UnitTests.Factories
{
    using Appointments.Registration.Domain;
    using Appointments.Workers.Domain;
    using DocAppointment.DomainModel.PartnerModels;

    public static class FakeDataFactory
    {
        public static IEnumerable<RegistrationItem> RegistrationItems => new List<RegistrationItem>
        {
            new RegistrationItem
            {
                Id = Guid.NewGuid(),
                IsEnabled = true,
                PartnerId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                WorkerId = Guid.Parse("553B5F8C-42CC-475B-AE6A-246BAB2FE767"),
                RegistrationTime = DateTime.Now,
                ScheduledOnTime = DateTime.Now.AddDays(1)
            },
            
            new RegistrationItem
            {
                Id = Guid.NewGuid(),
                IsEnabled = true,
                PartnerId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                WorkerId = Guid.Parse("8B895CC4-A247-4308-813F-4ADA1A734F2F"),
                RegistrationTime = DateTime.Now,
                ScheduledOnTime = DateTime.Now.AddDays(2)
            },
            
            new RegistrationItem
            {
                Id = Guid.NewGuid(),
                IsEnabled = false,
                PartnerId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                WorkerId = Guid.Parse("553B5F8C-42CC-475B-AE6A-246BAB2FE767"),
                RegistrationTime = DateTime.Now.AddDays(1),
                ScheduledOnTime = DateTime.Now.AddDays(1)
            },

            new RegistrationItem
            {
                Id = Guid.NewGuid(),
                IsEnabled = true,
                PartnerId = Guid.Parse("34F1BEF4-02CC-4EFC-A6EC-87CEE35467E4"),
                WorkerId = Guid.Parse("553B5F8C-42CC-475B-AE6A-246BAB2FE767"),
                RegistrationTime = DateTime.Now.AddDays(-2),
                ScheduledOnTime = DateTime.Now.AddDays(-11)
            }
        };

        public static IEnumerable<Partner> Partners => new List<Partner>()
        {
            new Partner()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                SecondName = "Иванович",
                LastName = "Сергеев",
                BirthDate = new DateOnly(1951, 1, 22),
                Sex = Partner.SexEnum.Муж,
                Phone = "9161234567"
            },
            new Partner()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                SecondName = "Ильич",
                LastName = "Андреев",
                BirthDate = new DateOnly(2001, 10, 2),
                Sex = Partner.SexEnum.Муж,
                Phone = "9253215476"
            },
            new Partner()
            {
                Id = Guid.Parse("34F1BEF4-02CC-4EFC-A6EC-87CEE35467E4"),
                Email = "vas.prekras@somemail.ru",
                FirstName = "Василиса",
                LastName = "Прекрасная",
                BirthDate = new DateOnly(2004, 2, 29),
                Sex = Partner.SexEnum.Жен,
                Phone = "9039876543"
            },
        };

        public static IEnumerable<Worker> Workers => new List<Worker>()
        {
            new Worker()
            {
                Id = Guid.Parse("CF6F54B6-A0C4-4F9A-B77D-B08712495352"),
                FirstName = "Мафусаил",
                SecondName = "Иванович",
                LastName = "Кириченко",
                Phone = "9037657676"
            },
            new Worker()
            {
                Id = Guid.Parse("553B5F8C-42CC-475B-AE6A-246BAB2FE767"),
                FirstName = "Илья",
                SecondName = "Ильич",
                LastName = "Пирогов",
                Phone = "9259292929"
            },
            new Worker()
            {
                Id = Guid.Parse("8B895CC4-A247-4308-813F-4ADA1A734F2F"),
                FirstName = "Авдотья",
                LastName = "Грибова",
                Phone = "9038476521"
            },
        };

        public static IEnumerable<Speciality> Specialities => new List<Speciality>()
        {
            new Speciality()
            {
                Id = Guid.Parse("0090531D-8F0E-4639-B88D-B6A3C590BED2"),
                Name = "терапевт"
            },
            new Speciality()
            {
                Id = Guid.Parse("A8E2A539-F405-4532-81FD-4CEDAF748374"),
                Name = "эндокринолог"
            },
            new Speciality()
            {
                Id = Guid.Parse("24E725F6-7EA9-4EBC-AD6E-724FFDFEA3B7"),
                Name = "офтальмолог"
            },
            new Speciality()
            {
                Id = Guid.Parse("BA430918-8B51-4C15-9A55-3177404BCB97"),
                Name = "хирург"
            },
            new Speciality()
            {
                Id = Guid.Parse("6077843D-8D6B-4BCA-BFB6-0992E00967B4"),
                Name = "невролог"
            },
            new Speciality()
            {
                Id = Guid.Parse("A5A22C29-CEAA-41A6-81C4-087F487BACA4"),
                Name = "нефролог"
            },
            new Speciality()
            {
                Id = Guid.Parse("62817B1F-FCCD-4C1D-B7F9-3D1480ED0AA1"),
                Name = "отоларинголог"
            },
            new Speciality()
            {
                Id = Guid.Parse("F30C3A4F-4097-4F14-A7CF-E209B53BEF1E"),
                Name = "кардиолог"
            },
            new Speciality()
            {
                Id = Guid.Parse("F37DBBEC-205F-41CF-919A-5ABA198EEAAD"),
                Name = "психотерапевт"
            },
            new Speciality()
            {
                Id = Guid.Parse("870775BC-1644-4F50-851F-862E62F4970D"),
                Name = "гастроэнтеролог"
            },
            new Speciality()
            {
                Id = Guid.Parse("EA95DBC7-10C0-4442-AFDE-2DDC069DF7BD"),
                Name = "психиатр"
            },
        };
    }
}
