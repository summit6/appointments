﻿namespace Appointments.Registration.UnitTests.Factories
{
    using Appointments.CrossSystemContracts.Partners;
    using Appointments.Registration.DataAccess;
    using Appointments.Registration.Services.Logger;
    using Appointments.Registration.Services.Partners;
    using Appointments.Registration.Services.Workers;
    using DocAppointment.DomainModel.PartnerModels;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc.Testing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Moq;

    public class RegistrationWebApplicationFactory<TProgram> :
        WebApplicationFactory<TProgram>, IDisposable where TProgram : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                this.ConfigureDomainContext(services);

                this.MoqPartnersService(services);

                this.MoqWorkersService(services);

                this.MoqLoggerService(services);
            });
        }

        private void ConfigureDomainContext(IServiceCollection services)
        {
            this.RemoveDesctiptor<DbContextOptions<DomainContext>>(services);

            services.AddDbContext<DomainContext>(
                options =>
                {
                    options.UseInMemoryDatabase("InMemoryAppointmentsRegistration", builder => { });
                },
                ServiceLifetime.Singleton);

            var serviceProvider = services.BuildServiceProvider();

            var domainContext = serviceProvider.GetService<DomainContext>();

            domainContext?.AddRange(FakeDataFactory.RegistrationItems);

            domainContext?.SaveChanges();
        }

        private void MoqLoggerService(IServiceCollection services)
        {
            this.RemoveDesctiptor<ILoggerService>(services);

            var loggerService = new Mock<ILoggerService>();

            loggerService.Setup(ls => ls.Log(It.IsAny<string>())).Callback<string>(_ => { });

            services.AddScoped(_  => loggerService.Object);
        }

        private void MoqPartnersService(IServiceCollection services)
        {
            this.RemoveDesctiptor<IPartnersService>(services);

            var partnerService = new Mock<IPartnersService>();

            partnerService.Setup(ps =>
                    ps.EnsurePartnerExistsAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            partnerService.Setup(ps =>
                    ps.GetAllPartners(It.IsAny<CancellationToken>()))
                .ReturnsAsync(FakeDataFactory.Partners.Select(p => new PartnerContact
                {
                    FirstName = p.FirstName,
                    LastName = p.LastName,
                    SecondName = p.SecondName,
                    Email = p.Email,
                    Id = p.Id,
                    FullName = $"{p.FirstName} {p.SecondName} {p.LastName}",
                    PhoneStr = $"{Partner.PhonePrefix} {p.Phone}"
                }));

            services.AddScoped(_ => partnerService.Object);
        }

        private void MoqWorkersService(IServiceCollection services)
        {
            this.RemoveDesctiptor<IWorkersService>(services);
           
            var workerService = new Mock<IWorkersService>();

            workerService.Setup(ws =>
                    ws.EnsureWorkerExistsAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //workerService.Setup(ws =>
            //        ws.g(It.IsAny<CancellationToken>()))
            //    .ReturnsAsync(FakeDataFactory.Partners.Select(p => new PartnerContact
            //    {
            //        FirstName = p.FirstName,
            //        LastName = p.LastName,
            //        SecondName = p.SecondName,
            //        Email = p.Email,
            //        Id = p.Id,
            //        FullName = $"{p.FirstName} {p.SecondName} {p.LastName}",
            //        PhoneStr = $"{Partner.PhonePrefix} {p.Phone}"
            //    }));

            services.AddScoped(_ => workerService.Object);
        }

        private void RemoveDesctiptor<T>(IServiceCollection services)
        {
            var serviceDescriptor = services.SingleOrDefault(x => x.ServiceType == typeof(T));

            if (serviceDescriptor != null) services.Remove(serviceDescriptor);
        }

        public new void Dispose()
        {
        }
    }
}
