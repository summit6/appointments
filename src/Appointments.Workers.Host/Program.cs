namespace Appointments.Workers.Host;

using Appointments.Workers.DataAccess.Abstractions;
using Appointments.Workers.DataAccess.EntityFramework;
using Appointments.Workers.Host.Mappings;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;

internal class Program
{
    private static string origin = "WorkersOrigins";

    private static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        builder.Services.AddCors(options =>
        {
            options.AddPolicy(name: origin, conf =>
            {
                conf.WithOrigins(new[] { "*" })
                    .WithHeaders(new[] { "*" })
                    .WithMethods(new[] { "*" });
            });
        });

        InstallAutomapper(builder.Services);

        builder.Services.AddServices(builder.Configuration);

        builder.Services.AddControllers();

        builder.Services.AddEndpointsApiExplorer();

        builder.Services.AddOpenApiDocument(options =>
        {
            options.Title = "Appointments Workers API Doc";
            options.Version = "1.0";
        });
        
        var app = builder.Build();

        //------------------------

        if (!app.Environment.IsDevelopment())
        {
            app.UseExceptionHandler("/Error");
            app.UseHsts();
        }

        app.UseHttpsRedirection();

        app.UseRouting();
        app.UseCors(origin);

        app.UseCors(builder =>
            builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod()
        );

        app.UseOpenApi();

        app.UseSwaggerUi3(x => { x.DocExpansion = "list"; });

        app.UseAuthorization();

        app.MapControllers();

        MigrateDatabase(app);

        InitializeDatabase(app);

        app.Run();
    }

    private static void InitializeDatabase(WebApplication app)
    {
        using var scope = app.Services.GetRequiredService<IServiceScopeFactory>().CreateScope();

        var dBInitializer = scope.ServiceProvider.GetService<IDbInitializer>();

       dBInitializer.InitializeDb();
    }

    private static void MigrateDatabase(WebApplication app)
    {
        using var scope = app.Services.GetRequiredService<IServiceScopeFactory>().CreateScope();

        var dbContext = scope.ServiceProvider.GetService<DomainContext>();

        if (dbContext.Database.ProviderName != "Microsoft.EntityFrameworkCore.InMemory")
        {
            dbContext.Database.Migrate();
        }
    }

    private static void InstallAutomapper(IServiceCollection services)
    {
        services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));
    }

    private static MapperConfiguration GetMapperConfiguration()
    {
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<WorkerMappingsProfile>();
        });
        configuration.AssertConfigurationIsValid();
        return configuration;
    }

}