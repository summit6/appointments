#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["src/Appointments.Workers.Host/Appointments.Workers.Host.csproj", "src/Appointments.Workers.Host/"]
COPY ["src/Appointments.CrossSystemContracts/Appointments.CrossSystemContracts.csproj", "src/Appointments.CrossSystemContracts/"]
COPY ["src/Appointments.Workers.DataAccess/Appointments.Workers.DataAccess.csproj", "src/Appointments.Workers.DataAccess/"]
COPY ["src/Appointments.Workers.Domain/Appointments.Workers.Domain.csproj", "src/Appointments.Workers.Domain/"]
RUN dotnet restore "src/Appointments.Workers.Host/Appointments.Workers.Host.csproj"
COPY . .
WORKDIR "/src/src/Appointments.Workers.Host"
RUN dotnet build "Appointments.Workers.Host.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Appointments.Workers.Host.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Appointments.Workers.Host.dll"]