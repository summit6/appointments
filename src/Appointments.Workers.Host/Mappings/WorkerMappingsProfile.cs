﻿namespace Appointments.Workers.Host.Mappings
{
    using Appointments.CrossSystemContracts.Workers;
    using Appointments.Workers.Domain;
    using AutoMapper;
    using AutoMapper.Internal;

    public class WorkerMappingsProfile : Profile
    {
        public WorkerMappingsProfile()
        {
            this.CreateMap<CreateOrEditWorkerRequest, Worker>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.WorkerSpecialities, map => map.Ignore())
                .ForMember(d => d.Specialities, map => map.Ignore())

                .ForMember(d => d.FirstName, map => map.MapFrom(
                    (src, dest) => !string.IsNullOrEmpty(src.FirstName)
                    ? src.FirstName
                    : dest.FirstName))
                .ForMember(d => d.SecondName, map => map.MapFrom(
                    (src, dest) => !string.IsNullOrEmpty(src.SecondName)
                        ? src.SecondName
                        : dest.SecondName))

                .ForMember(d => d.LastName, map => map.MapFrom(
                    (src, dest) => !string.IsNullOrEmpty(src.LastName)
                        ? src.LastName
                        : dest.LastName))
                .ForMember(d => d.Phone, map => map.MapFrom(src =>
                    !string.IsNullOrEmpty(src.Phone) && src.Phone.StartsWith(Worker.PhonePrefix)
                        ? src.Phone.Substring(Worker.PhonePrefix.Length)
                        : src.Phone))
                ;

            this.CreateMap<Worker, WorkerContact>()
                .ForMember(d => d.Id, map => map.MapFrom(src => src.Id))
                .ForMember(d => d.FullName, map => map.MapFrom(src => $"{src.FirstName} {src.SecondName} {src.LastName}"))
                .ForMember(d => d.PhoneStr, map => map.MapFrom(src => src.Phone.FormatPhoneNumber(Worker.PhonePrefix)))
                ;


            this.CreateMap<Worker, WorkerResponse>()
                .ForMember(d => d.Id, map => map.MapFrom(src => src.Id))
                .ForMember(d => d.FullName,
                    map => map.MapFrom(src => $"{src.FirstName} {src.SecondName} {src.LastName}"))
                .ForMember(d => d.PhoneStr, map => map.MapFrom(src => src.Phone.FormatPhoneNumber(Worker.PhonePrefix)))
                .ForMember(d => d.FirstName, map => map.MapFrom(src => src.FirstName))
                .ForMember(d => d.SecondName, map => map.MapFrom(src => src.SecondName))
                .ForMember(d => d.LastName, map => map.MapFrom(src => src.LastName))
                .ForMember(d => d.Phone, map => map.MapFrom(src => src.Phone))
                ;

            this.CreateMap<Speciality, SpecialityResponse>()
                .ForMember(d => d.Id, map => map.MapFrom(r => r.Id))
                .ForMember(d => d.Name, map => map.MapFrom(r => r.Name))
                ;

            this.CreateMap<Worker, WorkerFullNameAndSpecialitiesResponse>()
                .ForMember(r => r.FullName, map => map.MapFrom(w => $"{w.LastName} {w.FirstName} {w.SecondName}"))
                .ForMember(r => r.SpecialitiesRecord, map => map.MapFrom(w =>
                    w.Specialities.ToList().ToSimpleRecord()));
        }
    }
}
