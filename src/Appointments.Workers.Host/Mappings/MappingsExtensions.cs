﻿namespace Appointments.Workers.Host.Mappings
{
    using Appointments.CrossSystemContracts.Workers;
    using Appointments.Workers.Domain;

    public static class MappingsExtensions
    {
        public static string FormatPhoneNumber(this string? phone, string prefix)
        {
            if (string.IsNullOrEmpty(phone)) return "";

            return phone.Length == 10
                ? $"{prefix} ({phone[..3]}) {phone.Substring(3, 3)}-{phone.Substring(6, phone.Length - 6)}"
                : $"{prefix} {phone}";
        }

        public static void InitializeFromUpdateRequest(this Worker worker, CreateOrEditWorkerRequest request)
        {
            worker.FirstName = string.IsNullOrEmpty(request.FirstName) 
                ? worker.FirstName 
                : request.FirstName;
            
            worker.SecondName = string.IsNullOrEmpty(request.SecondName) 
                ? worker.SecondName
                : request.SecondName;
            
            worker.LastName = string.IsNullOrEmpty(request.LastName) 
                ? worker.LastName
                : request.LastName;
            
            worker.Phone = !string.IsNullOrEmpty(request.Phone) && request.Phone.StartsWith(Worker.PhonePrefix)
                ? request.Phone[Worker.PhonePrefix.Length..]
                : worker.Phone;
        }
    }
}
