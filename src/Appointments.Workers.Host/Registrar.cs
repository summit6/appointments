﻿namespace Appointments.Workers.Host
{
    using Appointments.Workers.DataAccess.Abstractions;
    using Appointments.Workers.DataAccess.EntityFramework;
    using Appointments.Workers.DataAccess.Repositories;
    using Appointments.Workers.DataAccess.Services;
    using Microsoft.EntityFrameworkCore;

    public static class Registrar
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddDbContext<DomainContext>(options => 
                        //options.UseNpgsql("Host=localhost;Database=workers_db;Username=postgres;Password=example;Port=5444"))
                        options.UseNpgsql(configuration.GetConnectionString("WorkersConnection")))
                    .AddScoped<IDbInitializer, DbInitializer>()
                    .InstallRepositories()
                    .InstallServices();
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IWorkerRepository, WorkerRepository>();
            serviceCollection.AddScoped<ISpecialityRepository, SpecialityRepository>();
            return serviceCollection;
        }

        private static IServiceCollection InstallServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IWorkerService, WorkerSevice>();
            return serviceCollection;
        }
    }
}
