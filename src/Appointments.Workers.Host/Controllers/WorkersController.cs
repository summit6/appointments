namespace Appointments.Workers.Host.Controllers
{
    using Appointments.CrossSystemContracts.Workers;
    using Appointments.Workers.DataAccess.Abstractions;
    using Appointments.Workers.Domain;
    using Appointments.Workers.Host.Mappings;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Route("api/v1/[controller]")]
    public class WorkersController : ControllerBase
    {
        private readonly ILogger<WorkersController> _logger;
        private readonly IWorkerService _service;
        private readonly ISpecialityRepository specialityRepository;
        private readonly IMapper _mapper;

        public WorkersController(
            ILogger<WorkersController> logger,
            IWorkerService service,
            ISpecialityRepository specialityRepository,
            IMapper mapper
            )
        {
            this._logger = logger;
            this._service = service;
            this.specialityRepository = specialityRepository;
            this._mapper = mapper;
        }

        /// <summary>
        /// �������� ������ � ��������� ���� ������
        /// </summary>
        /// <returns>workers</returns>
        [HttpGet]
        public async Task<ActionResult<WorkerResponse>> GeWorkersAsync()
        {
            this._logger.LogInformation("Got into GetWorkersAsync");
            var workers = await this._service.GetAllWithSpecialities();
            return this.Ok(this._mapper.Map<List<WorkerResponse>>(workers));
        }

        /// <summary>
        /// �������� ������ ������ ����� �� Id
        /// </summary>
        /// <returns>worker</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<WorkerResponse>> GetWorkerByIdAsync(Guid id)
        {
            var worker = await this._service.GetByIdAsync(id);

            if (worker == null)
                return this.NotFound();

            return Ok(this._mapper.Map<Worker, WorkerResponse>(worker));
        }

        /// <summary>
        /// ��������� ������������� ����������� �� Id
        /// </summary>
        /// <returns>���� / ���</returns>
        [HttpGet("exists")]
        public async Task<bool> IfExistsWorker(Guid id)
        {
            var res = await this._service.GetByIdAsync(id);

            if (res == null)
                return false;

            return true;
        }

        /// <summary>
        /// ������� ����� �� Id
        /// </summary>
        /// <returns>��������� ��������</returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteWorker(Guid id)
        {
            var res = await this._service.Delete(id);

            if (res) return this.Ok();

            return this.NotFound();
        }

        /// <summary>
        /// �������� ������ �����
        /// </summary>
        /// <returns>��������� ��������</returns>
        [HttpPost]
        public async Task<IActionResult> CreateWorkerAsync(CreateOrEditWorkerRequest request)
        {
            var specialities =
                this.specialityRepository.AsQueryable().Where(s => request.Specialities.Contains(s.Id)).ToList();

            var worker = this._mapper.Map<CreateOrEditWorkerRequest, Worker>(request);

            if (specialities != null)
                worker.Specialities = specialities;

            return this.Ok(await this._service.Create(worker));
        }

        /// <summary>
        /// �������� ������ �����
        /// </summary>
        /// <returns>��������� ����������</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateWorkerAsync(Guid id, CreateOrEditWorkerRequest request)
        {
            var worker = await this._service.GetByIdAsync(id);

            if (worker == null)
                return this.NotFound($"worker with id = {id} not exist!");

            var specialities = 
                this.specialityRepository.AsQueryable().Where(s => request.Specialities.Contains(s.Id)).ToList();

            worker.InitializeFromUpdateRequest(request);

            if (specialities.Any())
            {
                worker.Specialities?.Clear();
                worker.Specialities = specialities;
            }

            await this._service.Update();
            
            return this.Ok();
        }

        [HttpGet("partnerregistrations")]
        public async Task<ActionResult<WorkerFullNameAndSpecialitiesResponse>> GetFullNameAndSpecialities(Guid id)
        {
            var worker = await this._service.GetByIdAsync(id);

            if (worker == null) 
                return this.BadRequest("����������� � ����� id �� ����������!");
            
            return this._mapper.Map<WorkerFullNameAndSpecialitiesResponse>(worker);
        }
    }
}