﻿namespace Appointments.Workers.Host.Controllers
{
    using Appointments.CrossSystemContracts.Workers;
    using Appointments.Workers.DataAccess.Abstractions;
    using Appointments.Workers.Domain;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// API для специальностей
    /// </summary>
    public class SpecialitiesController: ControllerBase
    {
        private readonly ISpecialityRepository specialityRepository;
        private readonly IMapper mapper;

        public SpecialitiesController(ISpecialityRepository specialityRepository, IMapper mapper)
        {
            this.specialityRepository = specialityRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Получить все специальности
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        public async Task<ActionResult<SpecialityResponse>> GetAll()
        {
            var specialities = await this.specialityRepository.GetAllAsync();

            return this.Ok(this.mapper.Map<List<SpecialityResponse>>(specialities));
        }

        /// <summary>
        /// Получить специальность по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<SpecialityResponse>> GetById(Guid id)
        {
            var speciality = await this.specialityRepository.GetById(id);

            if (speciality == null) return 
                this.NotFound("Специальности с таким id не существует!");

            return this.Ok(this.mapper.Map<SpecialityResponse>(speciality));
        }

        /// <summary>
        /// Изменить специальность по id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(Guid id, CreateOrEditSpecialityRequest request)
        {
            var speciality = await this.specialityRepository.GetById(id);

            if (speciality == null) return
                this.NotFound("Специальности с таким id не существует!");

            speciality.Name = request.Name;

            await this.specialityRepository.SaveChangesAsync();
                
            return this.Ok();
        }

        /// <summary>
        /// Создать специальность
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateSpecialityAsync(CreateOrEditSpecialityRequest request)
        {
            var speciality = new Speciality
            {
                Name = request.Name
            };

            await this.specialityRepository.AddAsync(speciality);

            await this.specialityRepository.SaveChangesAsync();

            return this.Ok(speciality.Id);
        }

        /// <summary>
        /// Удалить специальность по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<bool>> Delete(Guid id)
        {
            var speciality = await this.specialityRepository.GetById(id);

            if (speciality == null) return
                this.NotFound("Специальности с таким id не существует!");

            var result = await this.specialityRepository.Delete(id);

            await this.specialityRepository.SaveChangesAsync();

            return this.Ok(result);
        }
    }
}
