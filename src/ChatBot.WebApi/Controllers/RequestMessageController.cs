﻿using ChatBot.WebApi.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;

namespace ChatBot.WebApi.Controllers;

[ApiController]
[Route("[controller]")]
public class RequestMessageController : ControllerBase
{
    private readonly IBus _bus;

    public RequestMessageController(IBus bus)
    {
        _bus = bus;
    }

    [HttpPost]
    public async Task<IActionResult> Get(RequestMessage inMessage)
    {
        await _bus.Publish(inMessage);
        return Ok();
    }
}