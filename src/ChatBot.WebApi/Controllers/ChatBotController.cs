using ChatBot.WebApi.Models;
using ChatBot.WebApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace ChatBot.WebApi.Controllers;

[ApiController]
[Route("[controller]")]
public class ChatBotController : ControllerBase
{
  
    private readonly ILogger<ChatBotController> _logger;

    private readonly ChatBotService _chatBotService;

    private readonly WorkersRepository _workersClient;
    public ChatBotController(ILogger<ChatBotController> logger, ChatBotService chatBotService, Services.WorkersRepository workersClient)
    {
        
        _logger = logger;
        _chatBotService = chatBotService;
        _workersClient = workersClient;
    }

    [HttpPost]
    public async Task<IActionResult> Post(RequestMessage inMessage)
    {
        var result =await _chatBotService.Handler(inMessage);
        return Ok(result);
    }
    [HttpGet]
    public async Task<IActionResult> Get()
    {
        var result = await _workersClient.Get();
        return Ok(result);
    }
}