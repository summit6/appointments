﻿namespace ChatBot.WebApi.Declare;

public enum State
{
    Null, 
    Start,
    Workers,
    Calendar,
    Time,
    Register
}
