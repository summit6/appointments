using ChatBot.WebApi.Consumers;
using ChatBot.WebApi.Extensions;
using ChatBot.WebApi.Services;
using MassTransit;
using Refit;
using System.Globalization;
using Telegram.Bot;

namespace ChatBot.WebApi;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        
        // Add services to the container.

        builder.Services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddSwaggerService();

        builder.Services.AddSingleton<ITelegramBotClient>(_ => new TelegramBotClient(builder.Configuration.GetConnectionString("BotToken")));
        builder.Services.AddHostedService<TelegramHost>();
        builder.Services.AddTransient<ChatBotService>();

        builder.Services.AddTransient<WorkersRepository>();
        builder.Services.AddTransient<RegistrationRepository>();
        builder.Services.AddTransient<PartnersRepository>();

        builder.Services.AddRefitClient<IRegistrationRest>()
           .ConfigureHttpClient(c => c.BaseAddress = new Uri(builder.Configuration.GetConnectionString("gatewayUrl"))).ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
           {
               ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
           }
        );
        builder.Services.AddRefitClient<IPartnersRest>()
            .ConfigureHttpClient(c => c.BaseAddress = new Uri(builder.Configuration.GetConnectionString("gatewayUrl"))).ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
            }
        );
        builder.Services.AddGrpcClient<Greet.Greeter.GreeterClient>(x =>
            x.Address = new Uri(builder.Configuration.GetConnectionString("gatewayUrl")))
            .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
            }
        );

        // distributed cache
        builder.Services.AddDistributedMemoryCache();
        builder.Services.AddStackExchangeRedisCache(options =>
        {
            options.Configuration = builder.Configuration.GetConnectionString("Redis");
        });

       
        builder.Services.AddMassTransit(x =>
        {
            x.AddConsumer<RequestMessageConsumer>();

            x.UsingRabbitMq((context, cfg) =>
            {
                cfg.Host(builder.Configuration.GetConnectionString("RabbitMq"));
                cfg.ConfigureEndpoints(context);
               
            });
        });
        var cultureInfo = CultureInfo.GetCultureInfo("ru-RU");
        CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
        CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;
        var app = builder.Build();
        // Configure the HTTP request pipeline.

        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseAuthorization();


        app.MapControllers();

        app.Run();

        
    }

}