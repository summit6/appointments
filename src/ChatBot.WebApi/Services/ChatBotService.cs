﻿using ChatBot.WebApi.Declare;
using ChatBot.WebApi.Models;
using Microsoft.Extensions.Caching.Distributed;
using System.Text;

namespace ChatBot.WebApi.Services;

public class ChatBotService
{
    private WorkersRepository _workers;
    private RegistrationRepository _registration;
    private PartnersRepository _partners;
    private readonly IDistributedCache _distributedCache;

    public ChatBotService(WorkersRepository workers, IDistributedCache distributedCache, RegistrationRepository registration, PartnersRepository partners)
    {
        _workers = workers;
        _distributedCache = distributedCache;
        _registration = registration;
        _partners = partners;
    }

    public async Task<ResponseMessage> Handler(RequestMessage message)
    {
        try
        {
            var stateString = _distributedCache.GetString(message.UserName);
            var curentState = string.IsNullOrEmpty(stateString)
                ? State.Null
                : (State)Enum.Parse(typeof(State), stateString);

            var state = GetNextState(curentState);

            var response = state switch
            {
                
                State.Start => await Start(message),
                State.Workers => await WorkersSelect(),
                State.Calendar => Calendar(),
                State.Time =>await Time(message),
                State.Register => await Register(message),
                _ => await Start(message),
            };

            _distributedCache.SetString($"{message.UserName}.{state}", message.Text);
            _distributedCache.SetString(message.UserName, $"{state}");

            return response;
        }
        catch (Exception ex)
        {
            return new ResponseMessage()
            {
                Text = $"Произошла ошибка {ex}",
                ListButtons = new List<List<string>>()
            };
        }
    }

    public State GetNextState(State curentState)
    {
        var nextState = curentState switch
        {
            State.Start => State.Workers,
            State.Workers => State.Calendar,
            State.Calendar => State.Time,
            State.Time => State.Register,
            _ => State.Start,
        };
        return nextState;
    }
    private async Task<ResponseMessage> Time(RequestMessage message)
    {
        var fullName = _distributedCache.GetString(message.UserName + "." + State.Calendar);
        var workers = await _workers.Get();
        var workersId = workers.First(x => x.FullName.Equals(fullName)).Id;
        var a = await _registration.Get(workersId);
        IEnumerable<int> time;
        if (a != null)
        {
            time = a.Where(x => x.Day == int.Parse(message.Text)).Select(x => x.Hour);
        }
        else
        {
            time = new List<int>();
        }
        var listButtons = new List<List<string>>();
        for (int i = 9; i < 18; i++)
        {
            var exist= time.Any(x=>x.Equals(i))? "(Занято)":"";
            var str = $"{i},00{exist}";
            listButtons.Add(new List<string>
            {
                str
            });
        }
        return new ResponseMessage
        {
            Text = "Выберите время для записи",
            ListButtons = listButtons
        };
    }
    private async Task<Guid> GetUserId(string userName)
    {
        var users = await _partners.Get();
        var user = users.FirstOrDefault(x => x.Key == userName);
        var id = user.Value.Equals(Guid.Empty) ? await _partners.Add(userName) : user.Value;

        return id;
    }
    private async Task<ResponseMessage> Register(RequestMessage message)
    {
        var user = await GetUserId(message.UserName);

        var fullName = _distributedCache.GetString(message.UserName + "." + State.Calendar);

        var day =int.Parse(_distributedCache.GetString(message.UserName + "." + State.Time));
        var workers = await _workers.Get();
        var workersId = workers.First(x => x.FullName.Equals(fullName)).Id;
        var time = double.Parse(message.Text);
        var hour = Convert.ToInt32(time);
        var date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, day,hour,0,0);
        await _registration.Register(user,workersId, date);
        return
        new ResponseMessage
        {
            Text = $"Вы записаны на {date}",
            ListButtons = new List<List<string>>()
        };

    }
    private async Task<ResponseMessage> Start(RequestMessage message)
    {

        var user = await GetUserId(message.UserName);
        var a = await _registration.GetForUser(user);
        string text;
        if (a != null)
        {
            var res = new StringBuilder("Выши записи:" + Environment.NewLine);
            a.ToList().ForEach(x => res.AppendLine($"{x.Item2} {x.Item1}"));
            text = res.ToString();
        }
        else
        {
            text = "У вас нет записей";
        }

        var listButtons = new List<List<string>>
        {
            new List<string>
            {
                $"Записаться"
            }
        };
        return new ResponseMessage
        {
            Text = text,
            ListButtons = listButtons
        };
    }

    private async Task<ResponseMessage> WorkersSelect()
    {


        var workers = await _workers.Get();

        var listButtons = new List<List<string>>();
        foreach (var worker in workers)
        {
            listButtons.Add(new List<string>
            {
                $"{worker.FullName}"
            });
        }
        return new ResponseMessage
        {
            Text = "Выберите сотрудника для записи",
            ListButtons = listButtons
        };
    }

    private ResponseMessage Calendar()
    {
        //Console.Write("Enter a year: ");
        int year = DateTime.Now.Year;
        Console.Write("Enter a month (1-12): ");
        int month = DateTime.Now.Month;
        DateTime date = new(year, month, 1);
        Console.WriteLine(date.ToString("MMMM yyyy"));

        Console.WriteLine(" Mo Tu We Th Fr Sa Su");

        // Calculate the number of days in the month and the day of the week for the first day
        int daysInMonth = DateTime.DaysInMonth(year, month);
        DayOfWeek firstDayOfMonth = date.DayOfWeek;

        var listButtons = new List<List<string>>();

        var row = new List<string>();
        // Print blank spaces for any days before the first day of the week
        for (int i = 0; i < (int)firstDayOfMonth; i++)
        {
            row.Add("");
        }

        // Print the days of the month, with a new line after each week
        for (int i = 1; i <= daysInMonth; i++)
        {
            row.Add(i.ToString());
            if (row.Count % 7 == 0)
            {
                listButtons.Add(row);
                row = new List<string>();
            }
        }
        if (row.Count > 0)
        {
            for (int i = 1; i <= 7; i++)
            {
                if (row.Count % 7 == 0)
                {
                    listButtons.Add(row);
                    break;
                }
                row.Add("");

            }
        }
        return new ResponseMessage
        {
            Text = "Выберите день для записи",
            ListButtons = listButtons
        };
    }
}