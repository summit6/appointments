﻿using Appointments.CrossSystemContracts.Workers;
using Google.Protobuf.WellKnownTypes;
using static Greet.Greeter;

namespace ChatBot.WebApi.Services;

public class WorkersRepository
{
    private readonly GreeterClient _workerRest;

    public WorkersRepository(GreeterClient workerRest)
    {
        
        _workerRest = workerRest;
    }

    public async Task<IEnumerable<WorkerContact>> Get()
    {
        var response = await _workerRest.GetWorkersAsync(new Empty());
        var result = Newtonsoft.Json.JsonConvert.SerializeObject(response.Data);


        return Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<WorkerContact>>(result);
    }
}