﻿using Appointments.CrossSystemContracts.Registration;
using Appointments.CrossSystemContracts.Workers;
using Refit;

namespace ChatBot.WebApi.Services;

public interface IRegistrationRest
{
    [Post("/api/v1/Registration/register")]
    public Task<IApiResponse<Guid>> Register(RegistrationRequest registration);

    [Get("/api/v1/Registration/workerbusytime")]
    public Task<IApiResponse<WorkerBusyTimeResponse>> Workerbusytime([Query]Guid id);

    [Get("/api/v1/Registration/partnerregistrations")]
    public Task<IApiResponse<PartnerRegistrationsResponse>> PartnerRegistrations([Query] Guid id);


}
public class RegistrationRepository
{

    private readonly IRegistrationRest _client;

    public RegistrationRepository(IRegistrationRest workersClient)
    {
        _client = workersClient;

    }
    public async Task<IEnumerable<(string, DateTime)>> GetForUser(Guid userId)
    {

        var response = await _client.PartnerRegistrations(userId);
        if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
        {
            return default;
        }
        if (response.StatusCode != System.Net.HttpStatusCode.OK)
        {
            throw new Exception($"{response.Error} {response.Error.Content}");
        }
        var res = response.Content.PartnerRegistrations.Select(x => (x.WorkerFullName, x.ScheduledOnTime));
        return res;
    }
    public async Task<IEnumerable<DateTime>> Get(Guid worker)
    {
        var response = await _client.Workerbusytime(worker);
        if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
        {
            return default;
        }
        if (response.StatusCode != System.Net.HttpStatusCode.OK)
        {
            throw new Exception($"{response.Error} {response.Error.Content}");
        }
        return response.Content.DateTimes;
    }
    public async Task<object> Register(Guid userId, Guid workerId, DateTime time)
    {
        var a = new RegistrationRequest
        {
            PartnerId = userId,
            WorkerId = workerId,
            ScheduledOnTime = time,
            Information = time.ToString("f")
        };
        var result = await _client.Register(a);

        if (result.StatusCode != System.Net.HttpStatusCode.OK)
        {
            throw new Exception($"{result.Error} {result.Error.Content}");
        }

        return default;
    }


}