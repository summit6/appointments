﻿using Appointments.CrossSystemContracts.Partners;
using Refit;

namespace ChatBot.WebApi.Services;
public interface IPartnersRest
{
    [Post("/api/v1/Partners")]
    public Task<IApiResponse<Guid>> Add(CreateOrEditPartnerRequest request);

    [Get("/api/v1/Partners")]
    public Task<IApiResponse<IEnumerable<PartnerContact>>> Get();
}
public class PartnersRepository
{
    private readonly IPartnersRest _workerRest;

    public PartnersRepository(IPartnersRest workerRest)
    {
        
        _workerRest = workerRest;
    }

    public async Task<IDictionary<string,Guid>> Get()
    {

        var response = await _workerRest.Get();

        if (response.StatusCode != System.Net.HttpStatusCode.OK)
        {
            throw new Exception($"{response.Error} {response.Error.Content}");
        }
        var result = response.Content.ToDictionary(x => x.FirstName,x=> x.Id);
        return result;
    }
    public async Task<Guid> Add(string username)
    {
        var request = new CreateOrEditPartnerRequest
        {
            FirstName = username,
            LastName = username,
            BirthDateDay=1,
            BirthDateMonth=1,
            BirthDateYear=1,
        };
        var result = await _workerRest.Add(request);

        if (result.StatusCode != System.Net.HttpStatusCode.OK)
        {
            throw new Exception($"{result.Error} {result.Error.Content}");
        }
        return result.Content;
    }
}