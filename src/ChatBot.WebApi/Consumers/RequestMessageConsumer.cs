﻿using ChatBot.WebApi.Models;
using ChatBot.WebApi.Services;
using MassTransit;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace ChatBot.WebApi.Consumers
{

    public class RequestMessageConsumer : IConsumer<RequestMessage>
    {
        private readonly ChatBotService _chatBotService;

        private readonly ITelegramBotClient _botClient;

        public RequestMessageConsumer(ChatBotService chatBotService, ITelegramBotClient botClient)
        {
            _chatBotService = chatBotService;
            _botClient = botClient;
        }

        public async Task Consume(ConsumeContext<RequestMessage> context)
        {

            var result = await _chatBotService.Handler(context.Message);

            List<List<InlineKeyboardButton>> buttons = new();

            foreach (var items in result.ListButtons.ToList())
            {
                var row = new List<InlineKeyboardButton>();
                foreach (var item in items)
                {
                    var str = item.ToString();
                    if (str.Length == 0)
                        str = "empty";
                    row.Add(new InlineKeyboardButton(str) { CallbackData = str });
                }
                buttons.Add(row);
            }

            var keyboard = new InlineKeyboardMarkup(buttons);

            var d = await _botClient.EditMessageTextAsync(context.Message.Chat, context.Message.ResponseId, result.Text, replyMarkup: keyboard);
        }
    }
}
