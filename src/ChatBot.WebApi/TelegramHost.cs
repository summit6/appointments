﻿using ChatBot.WebApi.Models;
using MassTransit;
using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace ChatBot.WebApi
{
    public class TelegramHost : IHostedService
    {
        private readonly ILogger _logger;

        private readonly ITelegramBotClient _botClient;

        private CancellationTokenSource _cts;

        private readonly IBus _bus;

        public TelegramHost(ILogger<TelegramHost> logger, ITelegramBotClient botClient, IBus bus)
        {
            _logger = logger;
            _botClient = botClient;
            _bus = bus;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _cts = new CancellationTokenSource();
            CancellationToken cancelToken = _cts.Token;
            var receiverOptions = new ReceiverOptions
            {
                AllowedUpdates = { }, // receive all update types
            };
            _botClient.StartReceiving(
                HandleUpdateAsync,
                HandleErrorAsync,
                receiverOptions,
                cancelToken
            );
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _cts?.Cancel();
            return Task.CompletedTask;
        }

        private async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            try
            {

                if (update.Type == UpdateType.Message || update.Type == UpdateType.CallbackQuery)
                {
                    var message = update.Message;
                    message ??= update.CallbackQuery.Message;

                    var text = update.Type == UpdateType.CallbackQuery ? update.CallbackQuery.Data : message.Text;

                    var inMessage = new RequestMessage
                    {
                        Type= update.Type.ToString(),
                        Text= text,
                        Chat = message.Chat.Id,
                        MessageId = message.MessageId,
                        UserName = message.Chat.Username
                    };

                    var response = await botClient.SendTextMessageAsync(message.Chat.Id,
                        $"Обработка🔃...");

                    if (update.Type == UpdateType.CallbackQuery)
                        await _botClient.EditMessageTextAsync(message.Chat, message.MessageId, message.Text + " "+ inMessage.Text);

                    inMessage.ResponseId = response.MessageId;
                    await _bus.Publish(inMessage, cancellationToken);

                    _logger.LogDebug("{chat} {req} {res}", message.Chat.Id, message?.Text, response.MessageId);

                }

            }
            catch (Exception e)
            {

                _logger.LogError(e, "{@update}", update);
            }

        }

        private Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            _logger.LogError(exception, "error");
            Thread.Sleep(1000);
            return Task.CompletedTask;
        }

     
    }
}
