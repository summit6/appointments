﻿using ChatBot.WebApi.Example;
using Microsoft.OpenApi.Models;
using System.Text.RegularExpressions;

namespace ChatBot.WebApi.Extensions;

public static class SwaggerExtensions
{
    public static IServiceCollection AddSwaggerService(this IServiceCollection services)
    {
        return services.AddSwaggerGen(c =>
        {
            c.CustomOperationIds(e => $"{Regex.Replace(e.RelativePath, "{|}", "")}{e.HttpMethod}");
            c.SwaggerDoc("v1", new OpenApiInfo { Title = typeof(Program).Namespace, Version = "v1" });
            var xmlFiles = Directory.GetFiles(AppContext.BaseDirectory, "*.xml", SearchOption.TopDirectoryOnly).ToList();
            xmlFiles.ForEach(xmlFile => c.IncludeXmlComments(xmlFile));
            c.SchemaFilter<ExampleSchemaFilter>();
        });
    }
}