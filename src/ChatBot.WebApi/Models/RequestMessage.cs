﻿namespace ChatBot.WebApi.Models
{
    public class RequestMessage
    {
        public string Type { get; set; }
        public string Text { get; set; }
        public string UserName { get; set; }
        public long Chat { get; set; }
        public int MessageId { get; set; }
        public int ResponseId { get; set; }

    }
}
