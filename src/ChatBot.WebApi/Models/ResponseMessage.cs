﻿namespace ChatBot.WebApi.Models
{
    public struct ResponseMessage
    {
        public string Text { get; set; }

        public IEnumerable<IEnumerable<string>> ListButtons { get; set; }

    }
}
