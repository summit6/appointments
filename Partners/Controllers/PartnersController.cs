namespace Appointments.Partners.Controllers
{
    using Appointments.CrossSystemContracts.Partners;
    using Appointments.Partners.DataAccess.Contracts;
    using Appointments.Partners.Validators;
    using AutoMapper;
    using DocAppointment.DomainModel.PartnerModels;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// �������
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PartnersController : ControllerBase
    {
        private readonly ILogger<PartnersController> _logger;
        private readonly IPartnerService _service;
        private readonly IMapper _mapper;
        private readonly IPartnersValidator _validator;

        public PartnersController(
            ILogger<PartnersController> logger,
            IPartnerService service,
            IMapper mapper,
            IPartnersValidator validator
            )
        {
            this._logger = logger;
            this._service = service;
            this._mapper = mapper;
            this._validator = validator;
        }

        /// <summary>
        /// �������� ������ � ��������� ���� ��������
        /// </summary>
        /// <returns>partners</returns>
        [HttpGet]
        public async Task<ActionResult<PartnerContact>> GetPartnersAsync()
        {
            this._logger.LogInformation("Got into GetPartnersAsync");
            var partners = await this._service.GetAll();
            return this.Ok(this._mapper.Map<List<PartnerContact>>(partners));
         }

        /// <summary>
        /// �������� ������ ������ ������� �� Id
        /// </summary>
        /// <returns>partner</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<PartnerResponse>> GetPartnerByIdAsync(Guid id)
        {
            var partner = await this._service.GetByIdAsync(id);
            if (partner == null)
                return this.NotFound();
            return this.Ok(this._mapper.Map<Partner, PartnerResponse>(partner));
        }

        /// <summary>
        /// ����� ������� �� Id
        /// </summary>
        /// <returns>���� / ���</returns>
        [HttpGet("exists")]
        public bool IfExistsPartner(Guid id)
        {
            var res = this._service.GetById(id);
            if (res == null)
                return false;
            return true;
        }

        /// <summary>
        /// ������� ������� �� Id
        /// </summary>
        /// <returns>��������� ��������</returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeletePartner(Guid id)
        {
            var res = await this._service.Delete(id);
            if (res)
                return this.Ok();
            return this.NotFound();
        }

        /// <summary>
        /// �������� ������ �������
        /// </summary>
        /// <returns>��������� ��������</returns>
        [HttpPost]
        public async Task<IActionResult> CreatePartnerAsync(CreateOrEditPartnerRequest request)
        {
            var validationResult =
                await this._validator.ValidateAsync(request, CancellationToken.None);

            if (!validationResult.IsValid)
            {
                var errorText =
                    validationResult
                        .Errors
                        .Aggregate(string.Empty, (current, error) => current + $"{error.ErrorMessage}\r\n");

                return this.BadRequest(errorText);
            }

            try
            {
                return this.Ok(await this._service.Create(this._mapper.Map<CreateOrEditPartnerRequest, Partner>(request)));
            }
            catch( Exception ex)
            {
                return this.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// �������� ������ �������
        /// </summary>
        /// <returns>��������� ����������</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatePartnerAsync(Guid id, CreateOrEditPartnerRequest request)
        {
            var res = await this._service.Update(id, this._mapper.Map<CreateOrEditPartnerRequest, Partner>(request));
            if (res)
                return this.Ok();
            return this.NotFound();
        }
    }
}