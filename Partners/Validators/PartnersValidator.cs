﻿using DocAppointment.DomainModel.PartnerModels;
using FluentAssertions;
using FluentValidation;

namespace Appointments.Partners.Validators
{
    using Appointments.CrossSystemContracts.Partners;

    public class PartnersValidator : AbstractValidator<CreateOrEditPartnerRequest>, IPartnersValidator
    {
        public PartnersValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty().WithMessage("Имя клиента не может быть пустым");
            RuleFor(x => x.LastName).NotEmpty().WithMessage("Фамилия клиента не может быть пустой");
            RuleFor(x => x.Email).EmailAddress().WithMessage("Введите корректный email адрес");
            RuleFor(x => x.Phone).Length(7, 12).WithMessage("Номер телефона должен содержать от 7 до 12 цифр");
        }
    }
}
