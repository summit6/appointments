﻿using DocAppointment.DomainModel.PartnerModels;
using FluentValidation.Results;

namespace Appointments.Partners.Validators
{
    using Appointments.CrossSystemContracts.Partners;

    public interface IPartnersValidator
    {
        public Task<ValidationResult> ValidateAsync(CreateOrEditPartnerRequest instance, CancellationToken cancellationToken);
    }
}
