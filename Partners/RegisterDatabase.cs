﻿using Appointments.Partners.DataAccess.Contracts;
using Appointments.Partners.DataAccess.EntityFramework;
using Appointments.Partners.DataAccess.Repositories;
using Appointments.Partners.DataAccess.Services;
using Microsoft.EntityFrameworkCore;

namespace Appointments.Partners
{
    public static class RegisterDatabase
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                //.AddDbContext<DatabaseContext>(options => options.UseSqlite(configuration.GetConnectionString("Sqlite")))
                .AddDbContext<DatabaseContext>(options => options.UseNpgsql(configuration.GetConnectionString("PartnersDb")))
                .InstallRepositories()
                .InstallServices()
                ;
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<IPartnerRepository, PartnerRepository>();
            return serviceCollection;
        }

        private static IServiceCollection InstallServices(this IServiceCollection serviceCollection)
        {
            serviceCollection
            .AddTransient<IPartnerService, PartnerSevice>();
            return serviceCollection;
        }
    }
}
