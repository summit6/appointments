﻿using AutoMapper;
using DocAppointment.DomainModel.PartnerModels;

namespace Appointments.Partners.Mapping
{
    using Appointments.CrossSystemContracts.Partners;

    public class PartnerMappingsProfile : Profile
    {
        public PartnerMappingsProfile()
        {
            CreateMap<CreateOrEditPartnerRequest, Partner>()
               .ForMember(d => d.Id, map => map.Ignore())
               .ForMember(d => d.FirstName, map => map.MapFrom(src => src.FirstName))
               .ForMember(d => d.SecondName, map => map.MapFrom(src => src.SecondName))
               .ForMember(d => d.LastName, map => map.MapFrom(src => src.LastName))
               .ForMember(d => d.Email, map => map.MapFrom(src => src.Email))
               .ForMember(d => d.Phone, map => map.MapFrom(src => src.Phone != null && src.Phone.StartsWith(Partner.PhonePrefix) ? src.Phone.Substring(Partner.PhonePrefix.Length) : src.Phone))
               .ForMember(d => d.BirthDate, map => map.MapFrom(src => src.BirthDate == null ? new DateOnly(src.BirthDateYear, src.BirthDateMonth, src.BirthDateDay) : src.BirthDate))
               .ForMember(d => d.Sex, map => map.MapFrom(src => src.Sex == null || src.Sex.ToLower().Contains('ж') ? Partner.SexEnum.Жен : Partner.SexEnum.Муж))
               ;

            CreateMap<Partner, PartnerContact>()
                .ForMember(d => d.Id, map => map.MapFrom(src => src.Id))
                .ForMember(d => d.FullName, map => map.MapFrom(src => $"{src.FirstName} {src.SecondName} {src.LastName}"))
                .ForMember(d => d.Email, map => map.MapFrom(src => src.Email))
                .ForMember(d => d.PhoneStr, map => map.MapFrom(src => src.Phone == null ? "" : $"{Partner.PhonePrefix} ({src.Phone.Substring(0, 3)}) {src.Phone.Substring(3, 3)}-{src.Phone.Substring(6, src.Phone.Length-6)}"))
                .ForMember(d => d.FirstName, map => map.MapFrom(src => src.FirstName))
                .ForMember(d => d.SecondName, map => map.MapFrom(src => src.SecondName))
                .ForMember(d => d.LastName, map => map.MapFrom(src => src.LastName))
                ;


            _ = CreateMap<Partner, PartnerResponse>()
                .ForMember(d => d.Id, map => map.MapFrom(src => src.Id))
                .ForMember(d => d.FirstName, map => map.MapFrom(src => src.FirstName))
                .ForMember(d => d.SecondName, map => map.MapFrom(src => src.SecondName))
                .ForMember(d => d.LastName, map => map.MapFrom(src => src.LastName))
                .ForMember(d => d.FullName, map => map.MapFrom(src => $"{src.FirstName} {src.SecondName} {src.LastName}"))
                .ForMember(d => d.Email, map => map.MapFrom(src => src.Email))
                .ForMember(d => d.Phone, map => map.MapFrom(src => src.Phone))
                .ForMember(d => d.PhoneStr, map => map.MapFrom(src => src.Phone == null ? "" : $"{Partner.PhonePrefix} ({src.Phone.Substring(0, 3)}) {src.Phone.Substring(3, 3)}-{src.Phone.Substring(6, src.Phone.Length - 6)}"))
                .ForMember(d => d.BirthDate, map => map.MapFrom(src => src.BirthDate))
                .ForMember(d => d.BirthDateYear, map => map.MapFrom(src => src.BirthDate.Year))
                .ForMember(d => d.BirthDateMonth, map => map.MapFrom(src => src.BirthDate.Month))
                .ForMember(d => d.BirthDateDay, map => map.MapFrom(src => src.BirthDate.Day))
                .ForMember(d => d.Age, map => map.MapFrom(src => DateTime.Now.DayOfYear < src.BirthDate.DayOfYear ?
                                                DateTime.Now.Year - src.BirthDate.Year - 1 :
                                                DateTime.Now.Year - src.BirthDate.Year))
                .ForMember(d => d.SexStr, map => map.MapFrom(src => src.Sex.ToString()))
               ;


        }
    }
}
