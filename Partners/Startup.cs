﻿using Appointments.Partners.Mapping;
using AutoMapper;

namespace Appointments.Partners
{
    using Appointments.Partners.DataAccess.EntityFramework;
    using Appointments.Partners.Validators;
    using Microsoft.EntityFrameworkCore;

    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            InstallAutomapper(services);

            services.AddServices(Configuration);
            services.AddControllers();
            //            services.AddSingleton(typeof(IRepository<Partner>), (x) =>
            //                new InMemoryRepository<Partner>(FakeDataFactory.Partners));

            services.AddOpenApiDocument(options =>
            {
                options.Title = "Appointments Partners API Doc";
                options.Version = "1.0";
            });
            services.AddScoped<IPartnersValidator, PartnersValidator>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DatabaseContext databaseContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(builder =>
                builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod()
            );

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            databaseContext.Database.Migrate();
        }

        private static IServiceCollection InstallAutomapper(IServiceCollection services)
        {
            services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));
            return services;
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<PartnerMappingsProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }

    }
}
