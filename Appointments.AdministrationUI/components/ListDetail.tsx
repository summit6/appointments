﻿import React, { useState } from 'react'
import { useRouter } from "next/router";
import { SubmitHandler, useForm } from 'react-hook-form';
import { UserResponse } from '../interfaces'
import styles from './ListDetail.module.css';

type ListDetailProps = {
    item: UserResponse
    apiUrl: string
}

const ListDetail = ({ item: user, apiUrl: apiUrl }: ListDetailProps) => {
    const isAdd = user.id === null
    const url = apiUrl;

    const date = new Date();
    const minYear = date.getFullYear() - 100;
    const maxYear = date.getFullYear();
    const [serverError, setServerError] = useState('');

    const {
        register,
        formState: {
            errors,
        },
        handleSubmit,
    } = useForm<UserResponse>({
        defaultValues: {
            id: user.id,
            firstName: isAdd ? "" : user.firstName,
            secondName: isAdd ? "" : user.secondName,
            lastName: isAdd ? "" : user.lastName,
            email: isAdd ? "" : user.email,
            phone: isAdd ? "" : user.phone,
            birthDateYear: isAdd ? 2000 : user.birthDateYear,
            birthDateMonth: isAdd ? 1 : user.birthDateMonth,
            birthDateDay: isAdd ? 1 : user.birthDateDay,
        },
        mode: "onBlur"
    })

    const submit: SubmitHandler<UserResponse> = data => {
        setServerError("");
        console.log(data);

        if (isAdd)
            return fetch(`${url}`, {
                method: "POST",
                headers: { "Content-Type": "application/json", Accept: 'application/json', },
                body: JSON.stringify({
                    FirstName: data.firstName,
                    SecondName: data.secondName,
                    LastName: data.lastName,
                    email: data.email,
                    phone: data.phone,
                    birthDateYear: data.birthDateYear,
                    birthDateMonth: data.birthDateMonth,
                    birthDateDay: data.birthDateDay,
                    sex: data.sexStr,
                }),
            })
                .then(handleResponse)
                .catch(error => {
                    console.error('There was an error!', error);
                });
        else {
            return fetch(`${url}/${user.id}`, {
               method: "PUT",
                headers: { "Content-Type": "application/json", Accept: 'application/json', },
                body: JSON.stringify({
                    Id: data.id,
                    FirstName: data.firstName,
                    SecondName: data.secondName,
                    LastName: data.lastName,
                    email: data.email,
                    phone: data.phone,
                    birthDateYear: data.birthDateYear,
                    birthDateMonth: data.birthDateMonth,
                    birthDateDay: data.birthDateDay,
                    sex: data.sexStr,
                }),
            })
                .then(handleResponse)
                .catch(error => {
                    console.error('There was an error!', error);
                });
        }
   }

    const isEmail = data => {
        if (data.indexOf('@') < 0)
            return false;
        if (data.substring(data.indexOf('@')).indexOf('.') < 0)
            return false;
        return true;
    }

    const router = useRouter();
    const goHome = () => {
        router.push('/clients')
    }

    const handleResponse = response => {
        return response.text().then(text => {
            console.error('handleResponse!', text);
            const data = text;

            if (!response.ok) {
                const error = `${response.statusText}`;
                setServerError(error);
                return Promise.reject(error);
            } else {
                goHome();
            }

            return data;
        });
    }

    return <form onSubmit={handleSubmit(submit)}>
        <h1>{isAdd ? 'Введите нового клиента:' : 'Редактировать:'}</h1>
        
        <div className={styles.semi}>
            <label>Имя:
                <p>
                    <input type="text" {...register('firstName', { required: "Поле обязательное к заполнению" })} aria-invalid={errors.firstName ? true : false} />
                </p>
                <div style={{ color: 'red' }}>
                    {errors?.firstName && <p>{errors?.firstName?.message || "Ошибка!"}</p> }
                </div>
            </label>
            <label>Отчество:
                <p>
                    <input type="text" {...register('secondName')} />
                </p>
            </label>
            <label>Фамилия:
                <p>
                    <input type="text" {...register('lastName', { required: "Поле обязательное к заполнению" })} aria-invalid={errors.lastName ? true : false} />
                </p>
                <div style={{ color: 'red' }}>
                    {errors?.lastName && <p>{errors?.lastName?.message || "Ошибка!"}</p>}
                </div>
            </label>
            <label>email:
                <p>
                    <input type="text" {...register('email', { validate: { isEmail } })} />
                </p>
                <div style={{ color: 'red' }}>
                    {errors?.email && <p>{errors?.email?.message || "Ошибка в написании email!"}</p>}
                </div>
            </label>
            <label>Телефон:
                <p>
                    +7<input type="number" {...register('phone', { minLength: { value: 7, message: "Минимум 7 символов" }, maxLength: { value: 12, message: "Максимум 12 символов" } })} aria-invalid={errors.phone ? true : false} />
                </p>
                <div style={{ color: 'red' }}>
                    {errors?.phone && <p>{errors?.phone?.message || "Ошибка!"}</p>}
                </div>
            </label>
            <label className={styles.birth}>Дата рождения:
                <p>
                <label>Год:</label>
                    <input type="number" {...register('birthDateYear', {
                        min: { value: minYear, message: `Минимальное значение ${minYear}` }, max: { value: maxYear, message: `Максимальное значение  ${maxYear}` } })} aria-invalid={errors.birthDateYear ? true : false} />
                
                <label>Месяц:</label>
                    <input type="number" {...register('birthDateMonth', { min: { value: 1, message: "Минимальное значение 1" }, max: { value: 12, message: "Максимальное значение 12" } })} aria-invalid={errors.birthDateMonth ? true : false} />
                
                <label>День:</label>
                    <input type="number" {...register('birthDateDay', { min: { value: 1, message: "Минимальное значение 1" }, max: { value: 31, message: "Максимальное значение 31" } })} aria-invalid={errors.birthDateDay ? true : false} />
                <div style={{ color: 'red' }}>
                        {errors?.birthDateYear && <p>Год: {errors?.birthDateYear?.message || "Ошибка!"}</p>}
                </div>
                <div style={{ color: 'red' }}>
                        {errors?.birthDateMonth && <p>Месяц: {errors?.birthDateMonth?.message || "Ошибка!"}</p>}
                </div>
                <div style={{ color: 'red' }}>
                        {errors?.birthDateDay && <p>День: {errors?.birthDateDay?.message || "Ошибка!"}</p>}
                </div>
                
                </p>
            </label>
            <button>Сохранить</button>
            <button type="button" onClick={goHome}>Отменить</button>
            <div style={{ color: 'red' }}>
                {serverError && <p>{serverError || "Ошибка!"}</p>}
            </div>

        </div>
    </form >
}

export default ListDetail
