﻿import * as React from 'react'
import Link from 'next/link'
import ListItem from './ListItem'
import { User } from '../interfaces'

type Props = {
    items: User[]
    apiUrl: string
}

const List = ({ items, apiUrl }: Props) => (
    <div>
        <Link href="/clients/add">Добавить клиента</Link>
        <ul>
            {items.map((item) => (
                <li key={item.id}>
                    <ListItem data={item} apiUrl={apiUrl} />
                </li>
            ))}
        </ul>
    </div>
)

export default List
