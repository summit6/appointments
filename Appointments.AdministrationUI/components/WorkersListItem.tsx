﻿import React from 'react'
import Link from 'next/link'
import styles from './WorkersListItem.module.css';

import { Worker } from '../interfaces'

type Props = {
    data: Worker
    apiUrl: string
}

const remove = (id, apiUrl) => {
    fetch(`${apiUrl}/${id}`, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }    })
        .then(() => {
            console.log('removed');
        })
        .catch(error => {
            console.error('There was an error while deleting!', error);
        });
}

const WorkersListItem = ({ data, apiUrl}: Props) => (
    <div className={styles.semi}>
        <Link href="/users/[id]" as={`/users/${data.id}`}>
            {data.fullName}: {data.specialities.map(item => item.name).join(', ')}
        </Link>
        <button type="button" onClick={() => remove(data.id, apiUrl)}>Удалить</button>
    </div>
)

export default WorkersListItem
