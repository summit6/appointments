﻿import * as React from 'react'
import Link from 'next/link'
import WorkersListItem from './WorkersListItem'
import { Worker } from '../interfaces'

type Props = {
    items: Worker[]
    apiUrl: string
}

const WorkersList = ({ items, apiUrl }: Props) => (
    <div>
        <Link href="/users/add">Добавить сотрудника</Link>
        <ul>
            {items.map((item) => (
                <li key={item.id}>
                    <WorkersListItem data={item} apiUrl={apiUrl} />
                </li>
            ))}
        </ul>
    </div>
)

export default WorkersList
