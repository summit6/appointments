﻿import React, { ReactNode } from 'react'
import Link from 'next/link'
import LayoutBase from './LayoutBase'

type Props = {
    children?: ReactNode
    title?: string
}

const Layout = ({ children, title = 'This is the default title' }: Props) => (
    <LayoutBase title={title}>
        <header>
            <nav>
                <Link href="/">&#9668;</Link> |{' '}
                <Link href="/users">Сотрудники</Link> |{' '}
                <Link href="/clients">Клиенты</Link> |{' '}
            </nav>
        </header>
        {children}
    </LayoutBase>
)

export default Layout
