﻿import React, { useState } from 'react'
import { useRouter } from "next/router";
import { SubmitHandler, useForm } from 'react-hook-form';
import { WorkerResponse } from '../interfaces'
import styles from './WorkersListDetail.module.css';

type ListDetailProps = {
    item: WorkerResponse
    apiUrl: string
}

const WorkersListDetail = ({ item: user, apiUrl: apiUrl }: ListDetailProps) => {
    const isAdd = user.id === null

    const url = apiUrl;

    const [serverError, setServerError] = useState('');

    const {
        register,
        formState: {
            errors,
        },
        handleSubmit,
    } = useForm<WorkerResponse>({
        defaultValues: {
            id: user.id,
            firstName: isAdd ? "" : user.firstName,
            secondName: isAdd ? "" : user.secondName,
            lastName: isAdd ? "" : user.lastName,
            phone: isAdd ? "" : user.phone,
        },
        mode: "onBlur"
    })

    const submit: SubmitHandler<WorkerResponse> = (data) => {
        setServerError("");
        console.log(data);
        console.log(url);

        if (isAdd) {
            console.log("POST");
            console.log(data.secondName);
            return fetch(`${url}`, {
                method: "POST",
                headers: { "Content-Type": "application/json", Accept: 'application/json', },
                body: JSON.stringify({
                    FirstName: data.firstName,
                    SecondName: data.secondName,
                    LastName: data.lastName,
                    phone: data.phone,
                }),
            })
                .then(handleResponse)
                .catch(error => {
                    console.error('There was an error!', error);
                });
        } else {
            console.log("PUT, address   !!!!");
            console.log(`${url}/${user.id}`);
            return fetch(`${url}/${user.id}`, {
            method: "PUT",
            headers: { "Content-Type": "application/json", Accept: 'application/json', },
            body: JSON.stringify({
                Id: data.id,
                FirstName: data.firstName,
                SecondName: data.secondName,
                LastName: data.lastName,
                phone: data.phone,
            }),
        })
            .then(handleResponse)
            .catch(error => {
                console.error('There was an error!', error);
            });
        }
    }

    const router = useRouter();
    const goHome = () => {
        router.push('/users')
    }

    const handleResponse = response => {
        return response.text().then(text => {
            console.error('handleResponse!', text);
            const data = text;

            if (!response.ok) {
                const error = `${response.statusText}`;
                setServerError(error);
                return Promise.reject(error);
            } else {
                goHome();
            }

            return data;
        });
    }

    return <form onSubmit={handleSubmit(submit)}>
        <h1>{isAdd ? 'Введите нового сотрудника:' : 'Редактировать:'}</h1>
        
        <div className={styles.semi}>
            <label>Имя:
                <p>
                    <input type="text" {...register('firstName', { required: "Поле обязательное к заполнению" })} aria-invalid={errors.firstName ? true : false} />
                </p>
                <div style={{ color: 'red' }}>
                    {errors?.firstName && <p>{errors?.firstName?.message || "Ошибка!"}</p> }
                </div>
            </label>
            <label>Отчество:
                <p>
                    <input type="text" {...register('secondName')} />
                </p>
            </label>
            <label>Фамилия:
                <p>
                    <input type="text" {...register('lastName', { required: "Поле обязательное к заполнению" })} aria-invalid={errors.lastName ? true : false} />
                </p>
                <div style={{ color: 'red' }}>
                    {errors?.lastName && <p>{errors?.lastName?.message || "Ошибка!"}</p>}
                </div>
            </label>
            <label>Телефон:
                <p>
                    +7<input type="number" {...register('phone', { minLength: { value: 7, message: "Минимум 7 символов" }, maxLength: { value: 12, message: "Максимум 12 символов" } })} aria-invalid={errors.phone ? true : false} />
                </p>
                <div style={{ color: 'red' }}>
                    {errors?.phone && <p>{errors?.phone?.message || "Ошибка!"}</p>}
                </div>
           </label>
            <button>Сохранить</button>
            <button type="button" onClick={goHome}>Отменить</button>
            <div style={{ color: 'red' }}>
                {serverError && <p>{serverError || "Ошибка!"}</p>}
            </div>

        </div>
    </form >
}

export default WorkersListDetail
