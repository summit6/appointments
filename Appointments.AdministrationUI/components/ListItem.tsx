﻿import React from 'react'
import Link from 'next/link'
import styles from './ListItem.module.css';

import { User } from '../interfaces'

type Props = {
    data: User
    apiUrl: string
}

const remove = (id, apiUrl) => {
    fetch(`${apiUrl}/${id}`, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }    })
        .then(() => {
            console.log('removed');
        })
        .catch(error => {
            console.error('There was an error while deleting!', error);
        });
}

const ListItem = ({ data, apiUrl }: Props) => (
    <div className={styles.semi}>
        <Link href="/clients/[id]" as={`/clients/${data.id}`}>
            {data.fullName}
        </Link>
        <button type="button" onClick={() => remove(data.id, apiUrl)}>Удалить</button>
    </div>
)

export default ListItem
