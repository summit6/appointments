﻿import { GetStaticProps, GetStaticPaths } from 'next'
import { WorkerResponse } from '../../interfaces'
import Layout from '../../components/Layout'
import WorkersListDetail from '../../components/WorkersListDetail'

type Props = {
    item?: WorkerResponse
    errors?: string
    apiUrl?: string
}

const StaticPropsDetail = ({ item, errors, apiUrl }: Props) => {
    if (errors) {
        return (
            <Layout title="Error | Next.js + TypeScript Example">
                <p>
                    <span style={{ color: 'red' }}>Error:</span> {errors}
                </p>
            </Layout>
        )
    }

    return (
        <Layout
            title={`${item ? item.fullName : 'Редактирование сотрудника'
                } | Сотрудник не найден`}
        >
            {item && <WorkersListDetail item={item} apiUrl={apiUrl} />}
        </Layout>
    )
}

export default StaticPropsDetail

export const getStaticPaths: GetStaticPaths = async () => {
    const paths = []

    return { paths, fallback: 'blocking' }
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
    try {
        const res = await fetch(`${process.env.API_WORKERS_URL}/${params.id}`)
        const item = await res.json()
        return {
            props: {
                item,
                apiUrl: process.env.API_WORKERS_URL
            },
            revalidate: 1
        }
    } catch (err: any) {
        return { props: { errors: err.message } }
    }
}
