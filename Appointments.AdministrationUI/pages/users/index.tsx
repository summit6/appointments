﻿import { GetStaticProps } from 'next'
import Link from 'next/link'
import { Worker } from '../../interfaces'
import Layout from '../../components/Layout'
import WorkersList from '../../components/WorkersList'

type Props = {
    items: Worker[]
    apiUrl: string
}

const WorkersRoot = ({ items, apiUrl }: Props) => (
    <Layout title="Список сотрудников | Appointments">
        <h1>Сотрудники</h1>
        <WorkersList items={items} apiUrl={apiUrl} />
        <p>
            <Link href="/">&#9668;</Link>
        </p>
    </Layout>
)

export const getStaticProps: GetStaticProps = async () => {
    console.log(process.env.API_WORKERS_URL);
    try {
        const res = await fetch(`${process.env.API_WORKERS_URL}`);
        console.log(res.json.toString());
        const items: Worker[] = await res.json()
        if (!items) {
            return { notFound: true };
        }
        return {
            props: {
                items,
                apiUrl: process.env.API_WORKERS_URL } };
    } catch {
        return { notFound: true };
    }
}

export default WorkersRoot
