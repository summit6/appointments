﻿import { WorkerResponse } from '../../../interfaces'
import Layout from '../../../components/Layout'
import WorkersListDetail from '../../../components/WorkersListDetail'

type Props = {
    apiUrl: string
}

const AddWorker = ({ apiUrl }: Props) => {
    const item: WorkerResponse = {
        id: null,
        fullName: '',
        firstName: '',
        secondName: '',
        lastName: '',
        phoneStr: '',
        phone: '',
        specialities: null
    }
    return (
        <Layout
            title={ 'Добавление нового сотрудника' }
        >
            <WorkersListDetail item={item} apiUrl={apiUrl} />
        </Layout>
    )
}

export default AddWorker