﻿import { UserResponse } from '../../../interfaces'
import Layout from '../../../components/Layout'
import ListDetail from '../../../components/ListDetail'

type Props = {
    apiUrl: string
}

const AddUser = ({ apiUrl }: Props) => {
   const item: UserResponse = {
        id: null,
        fullName: '',
        firstName: '',
        secondName: '',
        lastName: '',
        email: '',
        phoneStr: '',
        phone: '',
        birthDateYear: 0,
        birthDateMonth: 0,
        birthDateDay: 0,
        age: 0,
        sexStr: ''
    }
    return (
        <Layout
            title={ 'Добавление нового клиента' }
        >
            <ListDetail item={item} apiUrl={apiUrl} />
        </Layout>
    )
}

export default AddUser