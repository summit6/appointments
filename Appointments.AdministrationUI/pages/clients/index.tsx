﻿import { GetStaticProps } from 'next'
import Link from 'next/link'

import { User } from '../../interfaces'
import Layout from '../../components/Layout'
import List from '../../components/List'

type Props = {
    items: User[]
    apiUrl: string
}

const PartnersRoot = ({ items, apiUrl }: Props) => (
    <Layout title="Список клиентов | Appointments">
        <h1>Клиенты</h1>
        <List items={items} apiUrl={apiUrl} />
        <p>
            <Link href="/">&#9668;</Link>
        </p>
    </Layout>
)

export const getStaticProps: GetStaticProps = async () => {
    try {
        const res = await fetch(`${process.env.API_CLIENTS_URL}`);
        const items: User[] = await res.json()
        if (!items) {
            return { notFound: true };
        }
        return {
            props: {
                items,
                apiUrl: process.env.API_CLIENTS_URL } };
    } catch {
        return { notFound: true };
    }
}

export default PartnersRoot
