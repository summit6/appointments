﻿import { GetStaticProps, GetStaticPaths } from 'next'
import { UserResponse } from '../../interfaces'
import Layout from '../../components/Layout'
import ListDetail from '../../components/ListDetail'

type Props = {
    item?: UserResponse
    errors?: string
    apiUrl?: string
}

const StaticPropsDetail = ({ item, errors, apiUrl }: Props) => {
    if (errors) {
        return (
            <Layout title="Error | Next.js + TypeScript Example">
                <p>
                    <span style={{ color: 'red' }}>Error:</span> {errors}
                </p>
            </Layout>
        )
    }

    return (
        <Layout
            title={`${item ? item.fullName : 'Редактирование клиента'
                } | Клиент не найден`}
        >
            {item && <ListDetail item={item} apiUrl={apiUrl} />}
        </Layout>
    )
}

export default StaticPropsDetail

export const getStaticPaths: GetStaticPaths = async () => {
    const paths = []

    return { paths, fallback: 'blocking' }
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
    try {
        const res = await fetch(`${process.env.API_CLIENTS_URL}/${params.id}`)
        const item = await res.json()
        return {
            props: {
                item,
                apiUrl: process.env.API_CLIENTS_URL
            },
            revalidate: 1
        }
    } catch (err: any) {
        return { props: { errors: err.message } }
    }
}
