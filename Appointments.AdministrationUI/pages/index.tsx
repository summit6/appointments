import Link from 'next/link'
import LayoutBase from '../components/LayoutBase'


const IndexPage = () => (
    <LayoutBase title="Appointments. Администрирование.">
        <div style={{ backgroundImage: 'url(/blue-nurse-med.jpg)', backgroundRepeat: "no-repeat", paddingLeft: "400px", minHeight: "500px" }} >
            <h1 style={{ paddingTop: "30px", paddingBottom: "50px" }}>
                Appointments. Администрирование.
            </h1>
            <header>
                <nav>
                    <Link href="/users">Сотрудники</Link> |{' '}
                    <Link href="/clients">Клиенты</Link> |{' '}
                </nav>
            </header>
        </div>
    </LayoutBase>
)

export default IndexPage
