﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Appointments.Partners.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class InitialPostgre : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Partners",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FirstName = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    SecondName = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    LastName = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Email = table.Column<string>(type: "text", nullable: true),
                    Phone = table.Column<string>(type: "character varying(12)", maxLength: 12, nullable: true),
                    BirthDate = table.Column<DateOnly>(type: "date", nullable: false),
                    Sex = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Partners", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PartnerAppointments",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    PartnerId = table.Column<Guid>(type: "uuid", nullable: false),
                    AppointmentDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Prescription = table.Column<string>(type: "text", nullable: true),
                    Diagnosis = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartnerAppointments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PartnerAppointments_Partners_PartnerId",
                        column: x => x.PartnerId,
                        principalTable: "Partners",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Partners",
                columns: new[] { "Id", "BirthDate", "Email", "FirstName", "LastName", "Phone", "SecondName", "Sex" },
                values: new object[,]
                {
                    { new Guid("34f1bef4-02cc-4efc-a6ec-87cee35467e4"), new DateOnly(2004, 2, 29), "vas.prekras@somemail.ru", "Василиса", "Прекрасная", "9039876543", null, 1 },
                    { new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"), new DateOnly(1951, 1, 22), "owner@somemail.ru", "Иван", "Сергеев", "9161234567", "Иванович", 0 },
                    { new Guid("f766e2bf-340a-46ea-bff3-f1700b435895"), new DateOnly(2001, 10, 2), "andreev@somemail.ru", "Петр", "Андреев", "9253215476", "Ильич", 0 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_PartnerAppointments_PartnerId",
                table: "PartnerAppointments",
                column: "PartnerId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PartnerAppointments");

            migrationBuilder.DropTable(
                name: "Partners");
        }
    }
}
