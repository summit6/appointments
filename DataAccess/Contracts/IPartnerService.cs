﻿using DocAppointment.DomainModel.PartnerModels;

namespace Appointments.Partners.DataAccess.Contracts
{
    public interface IPartnerService
    {
        /// <summary>
        /// Получить список.
        /// </summary>
        /// <returns> Список клиентов. </returns>
        Task<IEnumerable<Partner>> GetAll();

        /// <summary>
        /// Получить список промокодов.
        /// </summary>
        /// <returns> Список промокодов. </returns>
        Task<IEnumerable<PartnerAppointment>> GetAllAppointments(Guid id);

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>клиент</returns>
        Task<Partner?> GetByIdAsync(Guid id);

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>клиент</returns>
        Partner? GetById(Guid id);

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="customer">клиент</para
        Task<Guid> Create(Partner customer);

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="customer">клиент</param>
        Task<bool> Update(Guid id, Partner customer);

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        Task<bool> Delete(Guid id);
    }
}
