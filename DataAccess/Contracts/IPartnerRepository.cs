﻿using DocAppointment.DomainModel.PartnerModels;

namespace Appointments.Partners.DataAccess.Contracts
{
    /// <summary>
    /// Репозиторий работы с клиентами
    /// </summary>
    public interface IPartnerRepository : IRepository<Partner>
    {
        Task<IEnumerable<PartnerAppointment>> GetAppointments(Guid id);
    }
}
