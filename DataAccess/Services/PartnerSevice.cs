﻿using Appointments.Partners.DataAccess.Contracts;
using DocAppointment.DomainModel.PartnerModels;

namespace Appointments.Partners.DataAccess.Services
{
    public class PartnerSevice : IPartnerService
    {
        private readonly IPartnerRepository _repository;

        public PartnerSevice(
            IPartnerRepository repository
            )
        {
            _repository = repository;
        }
        /// <summary>
        /// Получить весь список.
        /// </summary>
        /// <returns> Список клиентов. </returns>
        public async Task<IEnumerable<Partner>> GetAll()
        {
            return await _repository.GetAllAsync();
        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>клиент</returns>
        public async Task<Partner?> GetByIdAsync(Guid id)
        {
            return await _repository.GetByIdAsync(id);
        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>клиент</returns>
        public Partner? GetById(Guid id)
        {
            return _repository.GetById(id);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="customer">клиент</para
        public async Task<Guid> Create(Partner customer)
        {
            var res = await _repository.AddAsync(customer);
            await _repository.SaveChangesAsync();
            return res.Id;
        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="customer">клиент</param>
        public async Task<bool> Update(Guid id, Partner customer)
        {
            customer.Id = id;
            var res = await _repository.ChangeAsync(customer);
            if (res)
                await _repository.SaveChangesAsync();
            return res;
        }

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        public async Task<bool> Delete(Guid id)
        {
            var res = await _repository.Delete(id);
            if (res)
                await _repository.SaveChangesAsync();
            return res;
        }

        /// <summary>
        /// Получить список записей.
        /// </summary>
        /// <param name="id">идентификатор клиента</param>
        /// <returns> Список записей. </returns>
        public async Task<IEnumerable<PartnerAppointment>> GetAllAppointments(Guid id)
        {
            return await _repository.GetAppointments(id);
        }
    }
}
