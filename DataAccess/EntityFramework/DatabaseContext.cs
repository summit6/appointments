﻿using DocAppointment.DomainModel.PartnerModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Appointments.Partners.DataAccess.EntityFramework
{
    using Appointments.Partners.DataAccess.Data;

    /// <summary>
    /// Контекст
    /// </summary>
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
            //Database.Migrate();
        }

        /// <summary>
        /// Клиенты
        /// </summary>
        public DbSet<Partner> Partners { get; set; }

        /// <summary>
        /// Записи Клиента
        /// </summary>
        public DbSet<PartnerAppointment> PartnerAppointments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<PartnerAppointment>()
                .HasOne(c => c.Partner)
                .WithMany()
                .HasForeignKey("PartnerId")
                ;

            modelBuilder.Entity<Partner>().HasData(FakeDataFactory.Partners);
        }
    }
}
