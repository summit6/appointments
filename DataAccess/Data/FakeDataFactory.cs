﻿namespace Appointments.Partners.DataAccess.Data
{
    using DocAppointment.DomainModel.PartnerModels;

    public static class FakeDataFactory
    {
        public static IEnumerable<Partner> Partners => new List<Partner>()
        {
            new Partner()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                SecondName = "Иванович",
                LastName = "Сергеев",
                BirthDate = new DateOnly(1951, 1, 22),
                Sex = Partner.SexEnum.Муж,
                Phone = "9161234567"
            },
            new Partner()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                SecondName = "Ильич",
                LastName = "Андреев",
                BirthDate = new DateOnly(2001, 10, 2),
                Sex = Partner.SexEnum.Муж,
                Phone = "9253215476"
            },
            new Partner()
            {
                Id = Guid.Parse("34F1BEF4-02CC-4EFC-A6EC-87CEE35467E4"),
                Email = "vas.prekras@somemail.ru",
                FirstName = "Василиса",
                LastName = "Прекрасная",
                BirthDate = new DateOnly(2004, 2, 29),
                Sex = Partner.SexEnum.Жен,
                Phone = "9039876543"
            },
        };
    }
}
