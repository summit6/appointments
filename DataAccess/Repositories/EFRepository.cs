﻿using Appointments.Partners.DataAccess.Contracts;
using DocAppointment.DomainModel;
using Microsoft.EntityFrameworkCore;

namespace Appointments.Partners.DataAccess.Repositories
{
    public abstract class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly DbContext Context;
        private readonly DbSet<T> _entitySet;
        public EfRepository(DbContext context)
        {
            Context = context;
            _entitySet = Context.Set<T>();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _entitySet.ToListAsync();
        }

        public virtual async Task<T?> GetByIdAsync(Guid id)
        {
            return await _entitySet.FindAsync(id);
        }

        public virtual T? GetById(Guid id)
        {
            return _entitySet.Find(id);
        }

        public virtual Task<bool> Delete(Guid id)
        {
            return Task.Run(() =>
            {
                var obj = _entitySet.Find(id);
                if (obj == null)
                {
                    return false;
                }
                _entitySet.Remove(obj);
                return true;
            });
        }

        public abstract Task<bool> ChangeAsync(T entity);

        public virtual async Task<T> AddAsync(T entity)
        {
            return (await _entitySet.AddAsync(entity)).Entity;
        }

        #region SaveChanges

        /// <summary>
        /// Сохранить изменения
        /// </summary>
        public virtual async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
        }

        #endregion
    }
}
