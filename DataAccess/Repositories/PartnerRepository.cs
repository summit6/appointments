﻿using Appointments.Partners.DataAccess.Contracts;
using Appointments.Partners.DataAccess.EntityFramework;
using DocAppointment.DomainModel.PartnerModels;
using Microsoft.EntityFrameworkCore;

namespace Appointments.Partners.DataAccess.Repositories
{
    public class PartnerRepository : EfRepository<Partner>, IPartnerRepository
    {
        public PartnerRepository(DatabaseContext context) : base(context)
        {
        }

        public override async Task<bool> ChangeAsync(Partner entity)
        {
            if (entity == null)
            {
                return false;
            }
            var obj = await GetByIdAsync(entity.Id);
            if (obj == null)
            {
                return false;
            }
            if (!string.IsNullOrEmpty(entity.LastName))
                obj.LastName = entity.LastName;
            if (!string.IsNullOrEmpty(entity.FirstName))
                obj.FirstName = entity.FirstName;
            if (!string.IsNullOrEmpty(entity.SecondName))
                obj.SecondName = entity.SecondName;
            if (!string.IsNullOrEmpty(entity.Phone))
                obj.Phone = entity.Phone;
            if (entity.BirthDate != DateOnly.MinValue)
                obj.BirthDate = entity.BirthDate;
            if (!string.IsNullOrEmpty(entity.Email))
                obj.Email = entity.Email;
            obj.Sex = entity.Sex;
            return true;
        }


        /// <summary>
        /// Получить записи
        /// </summary>
        /// <returns>записи</returns>
        public async Task<IEnumerable<PartnerAppointment>> GetAppointments(Guid id)
        {
            var query = Context.Set<PartnerAppointment>()
                .AsQueryable()
                .Include(c => c.Partner)
                .Where(c => c.PartnerId == id);

            return await query.ToListAsync();
        }

    }
}
