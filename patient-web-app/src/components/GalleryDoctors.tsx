import { Component } from 'react';
import './GalleryDoctors.css';
import galleryFoto1 from '../images/Screenshot_1.jpg';
import galleryFoto2 from '../images/Screenshot_2.jpg';
import galleryFoto3 from '../images/Screenshot_3.jpg';
import galleryFoto4 from '../images/Screenshot_4.jpg';

export default class GalleryDoctors extends Component {
    render(){
        return (
            <div>
                
                <section className="u-clearfix u-container-align-center-md u-container-align-center-sm u-container-align-center-xs u-valign-middle-md u-valign-middle-xl u-section-2" id="carousel_d993">
                <h2 className="u-align-center u-text u-text-default-lg u-text-default-xl u-text-1" data-animation-name="customAnimationIn" data-animation-duration="1500"> Наши врачи</h2>
        <div className="u-expanded-width u-palette-1-base u-shape u-shape-rectangle u-shape-1" data-animation-name="customAnimationIn" data-animation-duration="1500" data-animation-delay="250"></div>
        <div className="u-clearfix u-gutter-26 u-layout-wrap u-layout-wrap-1">
            <div className="u-layout">
            <div className="u-layout-row">
                <div className="u-size-15 u-size-30-md">
                <div className="u-layout-col">
                    <div className="u-container-align-center-lg u-container-align-center-md u-container-align-center-xl u-container-align-center-xxl u-container-style u-layout-cell u-size-40 u-layout-cell-1" data-animation-name="customAnimationIn" data-animation-duration="1500">
                    <div className="u-container-layout u-valign-middle u-container-layout-1">
                        <img className="u-image u-image-round u-radius-25 u-image-1" src={galleryFoto1} data-image-width="450" data-image-height="723"/>
                        <h5 className="u-align-center u-text u-text-2">Ветрова Марина Валерьевна</h5>
                        <p className="u-align-center u-text u-text-3">Отоларинголог</p>
                    </div>
                    </div>
                    <div className="u-container-style u-hidden-sm u-hidden-xs u-layout-cell u-size-20 u-layout-cell-2">
                    <div className="u-container-layout u-container-layout-2"></div>
                    </div>
                </div>
                </div>
                <div className="u-size-15 u-size-30-md">
                <div className="u-layout-col">
                    <div className="u-container-style u-hidden-sm u-hidden-xs u-layout-cell u-size-20 u-layout-cell-3">
                    <div className="u-container-layout u-container-layout-3"></div>
                    </div>
                    <div className="u-container-style u-layout-cell u-size-40 u-layout-cell-4" data-animation-name="customAnimationIn" data-animation-duration="1500" data-animation-delay="500">
                    <div className="u-container-layout u-valign-middle u-container-layout-4">
                        <img className="u-image u-image-round u-radius-25 u-image-2" src={galleryFoto3} data-image-width="548" data-image-height="592"/>
                        <h5 className="u-align-center u-text u-text-4"> Морозов Василий&nbsp;<br/>Викторович
                        </h5>
                        <p className="u-align-center u-text u-text-5">Хирург</p>
                    </div>
                    </div>
                </div>
                </div>
                <div className="u-size-15 u-size-30-md">
                <div className="u-layout-col">
                    <div className="u-container-style u-layout-cell u-size-40 u-layout-cell-5" data-animation-name="customAnimationIn" data-animation-duration="1500">
                    <div className="u-container-layout u-valign-middle u-container-layout-5">
                        <img className="u-image u-image-round u-radius-25 u-image-3" src={galleryFoto4}  data-image-width="450" data-image-height="499"/>
                        <h5 className="u-align-center u-text u-text-6">Пономаренко Юлия&nbsp;<br/>Петровна
                        </h5>
                        <p className="u-align-center u-text u-text-7">Терапевт</p>
                    </div>
                    </div>
                    <div className="u-container-style u-hidden-sm u-hidden-xs u-layout-cell u-size-20 u-layout-cell-6">
                    <div className="u-container-layout u-container-layout-6"></div>
                    </div>
                </div>
                </div>
                <div className="u-size-15 u-size-30-md">
                <div className="u-layout-col">
                    <div className="u-container-style u-hidden-sm u-hidden-xs u-layout-cell u-size-20 u-layout-cell-7">
                    <div className="u-container-layout u-container-layout-7"></div>
                    </div>
                    <div className="u-container-style u-layout-cell u-size-40 u-layout-cell-8" data-animation-name="customAnimationIn" data-animation-duration="1500" data-animation-delay="500">
                    <div className="u-container-layout u-container-layout-8">
                        <img className="u-image u-image-round u-radius-25 u-image-4" src={galleryFoto2} data-image-width="445" data-image-height="597"/>
                        <h5 className="u-align-center u-text u-text-8">Буянов Георгий Николаевич</h5>
                        <p className="u-align-center u-text u-text-9">Психиатр</p>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        <p className="u-align-center u-text u-text-body-color u-text-10" data-animation-name="customAnimationIn" data-animation-duration="1500" data-animation-delay="500">Галерея клиники</p>
        </section>
            </div>
        );
    }
}