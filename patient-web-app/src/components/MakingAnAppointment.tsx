import { Component } from "react";
import backimage from '../images/dsg-min.jpg';
import MuiFormRegistration from './MuiFormRegistration';

export default class MakingAnAppointment extends Component {
    render(){
        return (
            <div>
                <section className="u-clearfix u-grey-10 u-valign-middle-xs u-section-1" id="carousel_b8d3">
                    <div className="text-block"> 
                        <h3 className="u-text u-text-1">ЗАПИСЬ НА ПРИЕМ</h3>
                        <h6>Записаться на амбулаторный приём к врачу можно несколькими способами:<br/><br/>
                        1. Оставьте заявку на сайте. Введите свои данные, выберите врача и время приема. В результате будет создано электронное направление в ЛОКБ.<br/><br/>
                        2. Переходите по ссылке ниже в ТГ бот и оставляйте заявку.<br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="http://localhost:3000/%D0%A1%D0%BE%D1%86%D1%81%D0%B5%D1%82%D0%B8.html">ПЕРЕЙТИ В БОТ</a>
                        </h6>
                    </div>

                    <div className="u-expanded-height-lg u-expanded-height-md u-expanded-height-xl u-palette-1-base u-shape u-shape-rectangle u-shape-1"> 
                    </div>
                    <div className="u-expanded-width u-shape u-shape-rectangle u-white u-shape-2"></div>
                    <img src={backimage} className="u-align-left u-image u-image-1" data-image-width="1200" data-image-height="800"/>
                    <div className="u-align-left u-container-style u-group u-white u-group-1">
                        <div className="u-container-layout u-container-layout-1">
                        <h3 className="u-text u-text-1">Заполните заявку</h3>
                        <div className="u-align-left u-form u-form-1">
                            <MuiFormRegistration/>
                        </div>
                        </div>
                    </div>
                    <div className="u-palette-1-base u-shape u-shape-circle u-shape-3"></div>
                    
                </section>
            </div>
        );
    }
}