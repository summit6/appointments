import React from "react";
import { useEffect } from "react";
import { useState } from "react";

const FormRegistration = () => {
    const [workers, setWorkers] = useState([])
    const [partners, setPartners] = useState([])

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [secondName, setSecondName] = useState('')
    const [partnerPhone, setPartnerPhone] = useState('')
    const [dateReg, setDateReg] = useState(new Date());

    const [data, setData] = useState(null)

    const fetchGetWorkers = () => {
        fetch(process.env.REACT_APP_API_WORKERS_URL)
            .then(response => response.json())
            .then(data => setWorkers(data))
            .catch(error => console.log(error))
    }

    const fetchGetPartners = async () => {
        try {
            const response = await fetch(process.env.REACT_APP_API_PARTNERS_URL, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                }
            })

            if (!response.ok) {
                return response.text().then(text => { throw new Error(text) });
            }

            setPartners(await response.json());
            console.log(partners);

        } catch (err) {
            console.log(err);
        }
    };

    const fetchPostPartner = async (newPartner) => {
        try {
            const response = await fetch(process.env.REACT_APP_API_PARTNERS_URL, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
                body: JSON.stringify(newPartner)
            })

            if (!response.ok) {
                return response.text().then(text => { throw new Error(text) });
            }

            let result = await response.json();

            console.log('partnerId ' + result);

            return result;

        } catch (err) {
            console.log(err);
        }
    };

    const fetchPostRegistration = async (regData) => {
        try {
            const response = await fetch(process.env.REACT_APP_API_REGISTRATION_URL, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
                body: JSON.stringify(regData)
            })

            if (!response.ok) {
                return response.text().then(text => { throw new Error(text) });
            }

            let result = await response.json();

            console.log('regId ' + result);

            if (response.ok) {
                alert("Заявка принята!");
            }

        } catch (err) {
            console.log(err);
        }
    };

    const partnersMock = [
        {
            id: "451533d5-d8d5-4a11-9c7b-eb9f14e1a32f",
            firstName: "Иван",
            secondName: "Иванович",
            lastName: "Сергеев",
            fullName: "Иван Иванович Сергеев",
            email: "owner@somemail.ru",
            phoneStr: "+7 (916) 123-4567"
        },
        {
            id: "f766e2bf-340a-46ea-bff3-f1700b435895",
            firstName: "Петр",
            secondName: "Ильич",
            lastName: "Андреев",
            fullName: "Петр Ильич Андреев",
            email: "andreev@somemail.ru",
            phoneStr: "+7 (925) 321-5476"
        },
        {
            id: "60de82a1-0498-4ffe-bded-cf61d45c29c6",
            firstName: "string",
            secondName: "string",
            lastName: "string",
            fullName: "string string string",
            email: "string@mail.ru",
            phoneStr: "+7 (793) 715-68956"
        }
    ];

    useEffect(() => {
        fetchGetWorkers();
        //fetchPartners();
    }, [])

    const formRegistrationButtonClick = async () => {
        console.log("formRegistrationButtonClick");

        if (firstName && lastName && secondName && partnerPhone) {
            let partner = partnersMock.find(i => (i.firstName == firstName
                && i.lastName == lastName
                && i.secondName == secondName
                && i.phoneStr == partnerPhone
            ));

            console.log("partner " + partner);

            if (partner !== undefined) {
                console.log('Регистрация с существующим клиентом');
                console.log(partner.id);
                console.log(process.env.REACT_APP_API_REGISTRATION_URL);

                let regData = {
                    "partnerId": partner.id,
                    "workerId": document.getElementById("select-worker").value,
                    "scheduledOnTime": "2023-07-10T08:38:10.463Z",
                    "information": "test"
                };

                fetchPostRegistration(regData);
            }
            else {
                console.log('Регистрация с новым клиентом');

                let newPartner = {
                    "firstName": firstName,
                    "secondName": secondName,
                    "lastName": lastName,
                    "email": "test@mail.ru",
                    "phone": "89371249188",
                    "birthDateYear": 1992,
                    "birthDateMonth": 10,
                    "birthDateDay": 8,
                    "sex": "-"
                };

                let partnerId = await fetchPostPartner(newPartner);

                console.log("newPartnerId " + partnerId);

                let regData = {
                    "partnerId": partnerId,
                    "workerId": document.getElementById("select-worker").value,
                    "scheduledOnTime": "2023-07-10T08:38:10.463Z",
                    "information": "test"
                };

                fetchPostRegistration(regData);
            }
        }
    }


    return (
        <div>
            <form id="registrationForm" className="u-clearfix u-form-spacing-28 u-form-vertical u-inner-form" style={{ padding: '10px' }} name="form">
                <div className="u-form-group u-form-name">
                    <label className="u-form-control-hidden u-label">lastName</label>
                    <input type="text" value={lastName} placeholder="Введите фамилию" id="partnerLastName" name="name" onChange={e => setLastName(e.target.value)} className="u-border-2 u-border-black u-border-no-left u-border-no-right u-border-no-top u-input u-input-rectangle u-none" required="" wfd-id="id513" />
                </div>
                <div className="u-form-group u-form-name">
                    <label className="u-form-control-hidden u-label">firstName</label>
                    <input type="text" value={firstName} placeholder="Введите имя" id="partnerFirstName" name="name" onChange={e => setFirstName(e.target.value)} className="u-border-2 u-border-black u-border-no-left u-border-no-right u-border-no-top u-input u-input-rectangle u-none" required="" wfd-id="id513" />
                </div>
                <div className="u-form-group u-form-name">
                    <label className="u-form-control-hidden u-label">secondName</label>
                    <input type="text" value={secondName} placeholder="Введите отчество" id="partnerSecondName" name="name" onChange={e => setSecondName(e.target.value)} className="u-border-2 u-border-black u-border-no-left u-border-no-right u-border-no-top u-input u-input-rectangle u-none" required="" wfd-id="id513" />
                </div>
                <div className="u-form-phone u-form-group">
                    <label className="u-form-control-hidden u-label">Phone</label>
                    <input type="phone" value={partnerPhone} placeholder="Введите телефон" id="partnerPhone" name="phone" onChange={e => setPartnerPhone(e.target.value)} className="u-border-2 u-border-black u-border-no-left u-border-no-right u-border-no-top u-input u-input-rectangle u-none" required="" wfd-id="id514" />
                </div>
                <div className="u-form-group u-form-select u-form-group-3">
                    <label className="u-label">Выберите врача</label>
                    <div className="u-form-select-wrapper">
                        <select id="select-worker" name="select" className="u-border-2 u-border-black u-border-no-left u-border-no-right u-border-no-top u-input u-input-rectangle u-none">
                            {workers.map(worker => (
                                <option key={worker.id} value={worker.id}>{worker.fullName}</option>
                            ))}
                        </select>
                        <svg className="u-caret u-caret-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" style={{ fill: 'currentColor' }} xmlSpace="preserve"><polygon className="st0" points="8,12 2,4 14,4 "></polygon></svg>
                    </div>
                </div>
               
                {/*<div class="u-align-left u-form-group u-form-submit">
                                <a class="u-active-black u-border-none u-btn u-btn-round u-btn-submit u-button-style u-hover-black u-palette-4-base u-radius-50 u-btn-1">Отправить</a>
                                <input type="button" value="submit" class="u-form-control-hidden" wfd-id="id515"/>
                                    </div>*/}

                <div className="u-form-send-message u-form-send-success"> Thank you! Your message has been sent. </div>
                <div className="u-form-send-error u-form-send-message"> Unable to send your message. Please fix errors then try again. </div>
            </form>
            <div>
                <button onClick={formRegistrationButtonClick}>Отправить заявку на прием</button>
            </div>
        </div>
    )
}

export default FormRegistration;