import { useEffect } from "react";
import { useState } from "react";
import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import dayjs from 'dayjs';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Button from '@mui/material/Button';
import SendIcon from '@mui/icons-material/Send';
import moment from "moment";
import Alert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';
import InputMask from "react-input-mask";

const MuiFormRegistration = () => {
    const [workers, setWorkers] = useState([])
    const [partners, setPartners] = useState([])

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [secondName, setSecondName] = useState('')
    const [phone, setPhone] = useState('')
    const [email, setEmail] = useState('')
    const [dateBirthd, setDateBirthd] = useState(dayjs(new Date()));
    const [gender, setGender] = useState('Муж.')
    const [selectWorker, setSelectWorker] = useState('')

    const [dateReg, setDateReg] = useState(dayjs(new Date()));
    const [information, setInformation] = useState('');

    const [data, setData] = useState(null)
    const [showAlert, setShowAlert] = useState(false);
    const [alertSeverity, setAlertSeverity] = useState('info');
    const [alertText, setAlertText] = useState('');

    const [emailError, setEmailError] = useState(false);
    const [helperTextEmail, setHelperTextEmail] = useState('');
    
    const handleSelectWorkerChange = (event) => {
        setSelectWorker(event.target.value);
      };

    const fetchGetWorkers = () => {
        fetch(process.env.REACT_APP_API_WORKERS_URL)
            .then(response => response.json())
            .then(data => setWorkers(data))
            .catch(error => console.log(error));
    }

    const fetchGetPartners = async () => {
        try {
            const response = await fetch(process.env.REACT_APP_API_PARTNERS_URL, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                }
            })

            if (!response.ok) {
                return response.text().then(text => { throw new Error(text) });
            }

            setPartners(await response.json());
            console.log(partners);

        } catch (err) {
            console.log(err);
        }
    };

    const fetchPostPartner = async (newPartner) => {
        try {
            const response = await fetch(process.env.REACT_APP_API_PARTNERS_URL, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
                body: JSON.stringify(newPartner)
            })

            if (!response.ok) {
                return response.text().then(text => { throw new Error(text) });
            }

            let result = await response.json();

            console.log('partnerId ' + result);

            return result;

        } catch (err) {
            console.log(err);
        }
    };

    const fetchPostRegistration = async (regData) => {
        try {
            console.log(regData);
            const response = await fetch(process.env.REACT_APP_API_REGISTRATION_URL, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                },
                body: JSON.stringify(regData)
            })

            if (!response.ok) {
                return response.text().then(text => { throw new Error(text) });
            }

            let result = await response.json();

            console.log('regId ' + result);

            if (response.ok) {                
                setAlertSeverity("success");
                setAlertText("Ваша заявка была успешно принята!");
                setShowAlert(true);
                //alert("Ваша заявка была успешно принята!");
            }

        } catch (err) {
            setAlertSeverity("error");
            setAlertText("Ошибка формирования заявки. " + err.message);
            setShowAlert(true);

            console.log(err);
        }
    };

    const partnersMock = [
        {
            id: "451533d5-d8d5-4a11-9c7b-eb9f14e1a32f",
            firstName: "Иван",
            secondName: "Иванович",
            lastName: "Сергеев",
            fullName: "Иван Иванович Сергеев",
            email: "owner@somemail.ru",
            phoneStr: "+7 (916) 123-4567"
        },
        {
            id: "f766e2bf-340a-46ea-bff3-f1700b435895",
            firstName: "Петр",
            secondName: "Ильич",
            lastName: "Андреев",
            fullName: "Петр Ильич Андреев",
            email: "andreev@somemail.ru",
            phoneStr: "+7 (925) 321-5476"
        },
        {
            id: "60de82a1-0498-4ffe-bded-cf61d45c29c6",
            firstName: "string",
            secondName: "string",
            lastName: "string",
            fullName: "string string string",
            email: "string@mail.ru",
            phoneStr: "+7 (793) 715-68956"
        }
    ];

    const createRegisterData = (partnerId) =>{
        console.log(dayjs(dateReg).format('YYYY-MM-DD'));

        return {
            "partnerId": partnerId,
            "workerId": selectWorker,
            "scheduledOnTime": dayjs(dateReg).format('YYYY-MM-DD'),
            "information": information
        };
    };

    useEffect(() => {
        fetchGetWorkers();
        fetchGetPartners();
    }, [])

    const formRegistrationButtonClick = async () => {       
        console.log(phone);
        console.log(removeCharactersPhone(phone));

        if (firstName && lastName && secondName && phone) {
            let partner = partners.find(i => (i.firstName == firstName
                && i.lastName == lastName
                && i.secondName == secondName
                && i.phoneStr == phone
            ));

            if (partner !== undefined) {
                console.log('Регистрация с существующим клиентом');
                console.log(partner.id);
                console.log(process.env.REACT_APP_API_REGISTRATION_URL);

                let regData= createRegisterData(partner.id);                
                fetchPostRegistration(regData);
            }
            else {
                console.log('Регистрация с новым клиентом');

                let newPartner = {
                    "firstName": firstName,
                    "secondName": secondName,
                    "lastName": lastName,
                    "email": email,
                    "phone": removeCharactersPhone(phone),
                    "birthDateYear": dayjs(dateBirthd).format('YYYY'),
                    "birthDateMonth": dayjs(dateBirthd).format('MM'),
                    "birthDateDay": dayjs(dateBirthd).format('DD'),
                    "sex": gender
                };

                let partnerId = await fetchPostPartner(newPartner);
                fetchGetPartners();

                let regData= createRegisterData(partnerId);

                fetchPostRegistration(regData);
            }
        }
    }

    const handleGenderChange = (event) => {
        setGender(event.target.value);
    };
    
    const handleCloseAlert = (event, reason) => {
    if (reason === 'clickaway') {
        return;
    }

    setShowAlert(false);
    };
    
    const removeCharactersPhone = (str) => {
        const result = str.substring(2).replace(' ', '').replace(' ', '').replace('(', '').replace(')', '').replace('-', '').trim();
        return result;
      };

    const handleEmail = (value) => {
        setEmail(value);
        validateEmail(value);
    };

    const validateEmail = (email) => {
        let text = email ? "" : "Почта является обязательной."

        if (email)
        text = /^[^@\s]+@[^@\s]+\.[^@\s]+$/.test(email)
          ? ""
          : "Почта введена не корректно."

        setHelperTextEmail(text);
        
        if(helperTextEmail !== "")
        {
            setEmailError(true);
        }
        else 
        {
            setEmailError(false);
        }
                       
        return helperTextEmail === "" && email !== "";
    };

    return (
        <div>
            <Box
                component="form"
                sx={{
                    '& > :not(style)': { m: 1, width: '50ch' },
                }}
                noValidate
                autoComplete="off">

                <TextField required id="lastName" label="Фамилия" variant="outlined" onChange={e => setLastName(e.target.value)}/>
                <TextField required id="firstName" label="Имя" variant="outlined" onChange={e => setFirstName(e.target.value)}/>
                <TextField required id="secondName" label="Отчество" variant="outlined" onChange={e => setSecondName(e.target.value)}/>
                
                <InputMask
                    required
                    label="Телефон"
                    mask="+7 (999) 999-9999"
                    value={phone}
                    disabled={false}
                    maskChar="_"
                    onChange={e => setPhone(e.target.value)}
                >
                    {() => <TextField variant="outlined" label="Телефон" />}
                </InputMask>

                <TextField required id="email" label="Почта" type={"email"} variant="outlined" error = {emailError} helperText={helperTextEmail} onChange={e => handleEmail(e.target.value)}/>

                <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DemoContainer components={['DatePicker']}>
                        <DatePicker id="dateBirthd" label="Дата рождения" width = "" variant = "outlined" disableFuture
                        value = {dateBirthd} 
                        onChange={(newValue) => setDateBirthd(newValue)}
                        renderInput = {(props) => <TextField {...props}/>} 
                        inputFormat="YYYY-MM-DD"
                        format="YYYY-MM-DD"/>
                    </DemoContainer>
                </LocalizationProvider>

                <FormLabel id="gender">Пол</FormLabel>
                <RadioGroup aria-labelledby="demo-radio-buttons-group-label" defaultValue="Муж." name="gender" value={gender} onChange={handleGenderChange}>
                    <FormControlLabel value="Муж." control={<Radio />} label="Муж." />
                    <FormControlLabel value="Жен." control={<Radio />} label="Жен." />
                </RadioGroup>

                <InputLabel id="worker">Выберите врача</InputLabel>
                <Select
                    labelId="select-worker"
                    id="select-worker"
                    value={selectWorker}
                    label="Врачи"
                    onChange={handleSelectWorkerChange}
                >
                    {workers.map(worker => (                       
                        <MenuItem key={worker.id} value={worker.id}>{worker.fullName + " - " + worker.specialities[0].name}</MenuItem>
                    ))}
                </Select>

                <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DemoContainer components={['DatePicker']}>
                        <DatePicker id ="dateReg" label="Выберите дату приема" width = "" variant = "outlined"
                        value = {dateReg} 
                        onChange={(newDateReg) => setDateReg(newDateReg)}
                        inputFormat="YYYY-MM-DD"
                        format="YYYY-MM-DD"/>
                    </DemoContainer>
                </LocalizationProvider>
                
                <TextField required id="information" label="Информация для врача (Жалобы)" variant="outlined" onChange={e => setInformation(e.target.value)}/>

                <Button variant="contained" endIcon={<SendIcon />} onClick={formRegistrationButtonClick}>
                Отправить заявку
                </Button>

                <Snackbar open={showAlert} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} autoHideDuration={6000} onClose={handleCloseAlert}>
                    <Alert onClose={handleCloseAlert} severity={alertSeverity} sx={{ width: '100%' }}>
                    {alertText}
                    </Alert>
                </Snackbar>
            </Box>
        </div>
    )
}

export default MuiFormRegistration;