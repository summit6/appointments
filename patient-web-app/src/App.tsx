import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import MakingAnAppointment from './components/MakingAnAppointment';
import GalleryDoctors from './components/GalleryDoctors';
import Footer from './components/Footer';

function App() {
  return (
    <div className="App">
      <Header/>
      <MakingAnAppointment/>
      <GalleryDoctors/>
      <Footer/>
    </div>
  );
}

export default App;
